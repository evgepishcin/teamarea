package com.teamarea.cloud.messaging.service;

import com.teamarea.common.data.api.TransferData;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CloudMessageSenderService {
    public void sendMessage(TransferData data, List<String> cloudTokens) {
        //TODO при настоящей реализации можно будет подкючить учетную запись firebase, и слать сообщение в облако
        for (String s : cloudTokens) {
            System.out.println(s + ":" + data.getPayload());
        }
    }
}
