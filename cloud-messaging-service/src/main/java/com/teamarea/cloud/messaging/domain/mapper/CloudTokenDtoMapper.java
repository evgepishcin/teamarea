package com.teamarea.cloud.messaging.domain.mapper;

import com.teamarea.cloud.messaging.domain.dto.CloudTokenDto;
import com.teamarea.cloud.messaging.domain.entity.Token;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CloudTokenDtoMapper {
    CloudTokenDto tokenToCloudTokenDto(Token source);
}
