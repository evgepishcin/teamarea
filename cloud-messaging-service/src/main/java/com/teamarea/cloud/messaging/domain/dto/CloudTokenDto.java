package com.teamarea.cloud.messaging.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CloudTokenDto {
    private UUID deviceUuid;
    private Long userId;
    private String cloudToken;
}
