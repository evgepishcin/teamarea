package com.teamarea.cloud.messaging.domain.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "cloud_token")
@Getter
@Setter
@NoArgsConstructor
public class Token {
    @Id
    private UUID deviceUuid;
    private Long userId;
    private String cloudToken;
}
