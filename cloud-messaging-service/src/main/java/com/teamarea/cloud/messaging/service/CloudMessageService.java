package com.teamarea.cloud.messaging.service;

import com.teamarea.cloud.messaging.domain.dto.CloudTokenDto;
import com.teamarea.cloud.messaging.domain.dto.NewCloudTokenDto;
import com.teamarea.cloud.messaging.domain.entity.Token;
import com.teamarea.cloud.messaging.domain.mapper.CloudTokenDtoMapper;
import com.teamarea.cloud.messaging.repository.CloudTokenRepository;
import com.teamarea.common.data.api.TransferData;
import com.teamarea.common.data.api.TransferDataWithUserIds;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class CloudMessageService {

    private final CloudTokenRepository cloudTokenRepository;
    private final CloudTokenDtoMapper cloudTokenDtoMapper;
    private final CloudMessageSenderService cloudMessageSenderService;

    @Transactional
    public CloudTokenDto registerToken(NewCloudTokenDto newCloudTokenDto, UUID deviceUuid, Long userId) {
        Token token = new Token();
        token.setDeviceUuid(deviceUuid);
        token.setUserId(userId);
        token.setCloudToken(newCloudTokenDto.getCloudToken());
        cloudTokenRepository.save(token);
        return cloudTokenDtoMapper.tokenToCloudTokenDto(token);
    }

    @Transactional
    public void removeUsers(List<Long> usersIds) {
        if (!usersIds.isEmpty()) {
            cloudTokenRepository.deleteAllByUserIdIn(usersIds);
        }
    }

    @Transactional
    public void removeDevices(List<UUID> uuids) {
        if (!uuids.isEmpty()) {
            cloudTokenRepository.deleteAllByDeviceUuidIn(uuids);
        }
    }

    public void notifyUsers(TransferDataWithUserIds data) {
        List<String> usersTokens = cloudTokenRepository.selectTokensByUserInIn(data.getUsersIds());
        TransferData transferData = new TransferData();
        transferData.setType(data.getType());
        transferData.setOperation(data.getOperation());
        transferData.setPayload(data.getPayload());
        cloudMessageSenderService.sendMessage(transferData, usersTokens);
    }
}
