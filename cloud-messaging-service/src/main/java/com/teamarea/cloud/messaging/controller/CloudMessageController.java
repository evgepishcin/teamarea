package com.teamarea.cloud.messaging.controller;


import com.teamarea.cloud.messaging.domain.dto.CloudTokenDto;
import com.teamarea.cloud.messaging.domain.dto.NewCloudTokenDto;
import com.teamarea.cloud.messaging.service.CloudMessageService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class CloudMessageController {

    private final CloudMessageService cloudMessageService;

    @PostMapping("/cloudMessaging/token")
    public CloudTokenDto registerToken(@Valid @RequestBody NewCloudTokenDto newCloudTokenDto,
                                       @RequestHeader("Device-Uuid") UUID deviceUuid,
                                       @RequestHeader("User-Id") Long userId) {
        return cloudMessageService.registerToken(newCloudTokenDto, deviceUuid, userId);
    }
}
