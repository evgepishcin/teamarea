package com.teamarea.cloud.messaging.service.messages;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.teamarea.account.api.dto.AccountMessageType;
import com.teamarea.account.api.dto.DeviceUuidList;
import com.teamarea.account.api.dto.UsersIdsList;
import com.teamarea.cloud.messaging.service.CloudMessageService;
import com.teamarea.common.data.api.Operation;
import com.teamarea.common.data.api.TransferData;
import com.teamarea.common.data.api.TransferDataWithUserIds;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BrokerMessageListener {

    private final ObjectMapper objectMapper;
    private final CloudMessageService cloudMessageService;

    @SneakyThrows
    @KafkaListener(topics = Channels.ACCOUNT)
    public void listenAccount(String message) {
        TransferData transferData = objectMapper.readValue(message, TransferData.class);
        if (AccountMessageType.USERS_ID_LIST.equals(transferData.getType())) {
            if (Operation.DELETE.equals(transferData.getOperation())) {
                UsersIdsList usersIdsList = objectMapper.readValue(transferData.getPayload(), UsersIdsList.class);
                cloudMessageService.removeUsers(usersIdsList.getUsersIds());
            }
        } else if (AccountMessageType.DEVICE_UUID_LIST.equals(transferData.getType())) {
            if (Operation.DELETE.equals(transferData.getOperation())) {
                DeviceUuidList deviceUuidList = objectMapper.readValue(transferData.getPayload(), DeviceUuidList.class);
                cloudMessageService.removeDevices(deviceUuidList.getUuid());
            }
        }
    }

    @SneakyThrows
    @KafkaListener(topics = Channels.CLOUD_MESSAGE)
    public void listenCloudMessages(String message) {
        TransferDataWithUserIds data = objectMapper.readValue(message, TransferDataWithUserIds.class);
        cloudMessageService.notifyUsers(data);
    }
}
