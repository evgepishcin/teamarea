package com.teamarea.cloud.messaging;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
@Configuration
public class CloudMessagingApp {

    public static void main(String[] args) {
        SpringApplication.run(CloudMessagingApp.class, args);
    }
}
