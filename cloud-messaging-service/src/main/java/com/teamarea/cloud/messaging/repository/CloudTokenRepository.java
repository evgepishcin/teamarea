package com.teamarea.cloud.messaging.repository;

import com.teamarea.cloud.messaging.domain.entity.Token;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface CloudTokenRepository extends JpaRepository<Token, UUID> {

    void deleteAllByUserIdIn(List<Long> usersIds);

    void deleteAllByDeviceUuidIn(List<UUID> uuids);

    @Query("select t.cloudToken from Token t where t.userId in :usersIds ")
    List<String> selectTokensByUserInIn(List<Long> usersIds);
}
