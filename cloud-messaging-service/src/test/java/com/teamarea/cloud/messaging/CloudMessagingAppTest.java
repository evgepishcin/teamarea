package com.teamarea.cloud.messaging;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
class CloudMessagingAppTest {


    @Test
    @SuppressWarnings("java:S2699")
    void contextLoads() {
    }

}