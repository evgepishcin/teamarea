package com.teamarea.account.configuration;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

@ConfigurationProperties(prefix = "teamarea")
@Getter
@Setter
public class AccountProperties {

    @NestedConfigurationProperty
    private Services services;

    @NestedConfigurationProperty
    private Jwt jwt;

    public static class Services {
        private String mailServiceUrl;
        private String gatewayServiceUrl;

        public String getMailServiceUrl() {
            return mailServiceUrl;
        }

        public void setMailServiceUrl(String mailServiceUrl) {
            this.mailServiceUrl = mailServiceUrl;
        }

        public String getGatewayServiceUrl() {
            return gatewayServiceUrl;
        }

        public void setGatewayServiceUrl(String gatewayServiceUrl) {
            this.gatewayServiceUrl = gatewayServiceUrl;
        }
    }

    public static class Jwt {
        private String secret;
        private Long validityMilliseconds;

        public String getSecret() {
            return secret;
        }

        public void setSecret(String secret) {
            this.secret = secret;
        }

        public Long getValidityMilliseconds() {
            return validityMilliseconds;
        }

        public void setValidityMilliseconds(Long validityMilliseconds) {
            this.validityMilliseconds = validityMilliseconds;
        }
    }

}
