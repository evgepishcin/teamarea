package com.teamarea.account.controller;

import com.teamarea.account.domain.dto.TokenDeviceDto;
import com.teamarea.account.domain.dto.TokenDto;
import com.teamarea.account.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
public class SecureController {

    private final AccountService accountService;

    @PostMapping("/secure/account/validate")
    public TokenDeviceDto validateToken(@Valid @RequestBody TokenDto tokenDto) {
        return accountService.validateToken(tokenDto);
    }
}
