package com.teamarea.account.controller;

import com.teamarea.account.domain.dto.*;
import com.teamarea.account.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
public class AccountController {

    private final AccountService accountService;

    @PostMapping("/account/registration")
    public TokenDto registration(@Valid @RequestBody RegistrationDto registrationDto) {
        return accountService.registration(registrationDto);
    }

    @PostMapping("/account/authorization")
    public TokenDto authorization(@Valid @RequestBody AuthorizationDto authorizationDto) {
        return accountService.authorization(authorizationDto);
    }

    @PostMapping("/account/deviceConfirmation")
    public void confirmDevice(@RequestParam("code") String code,
                              @RequestHeader("Authorization") String token) {
        accountService.confirmDevice(code, token);
    }

    @PostMapping("/account/logout")
    public void logout(@RequestHeader("Authorization") String token) {
        accountService.logout(token);
    }

    @PostMapping("/account/logoutAll")
    public void logout(Authentication authentication) {
        accountService.logoutAll(authentication);
    }

    @PostMapping("/account/changePassword")
    public void changePassword(Authentication authentication, @Valid @RequestBody ChangePasswordDto changePasswordDto) {
        accountService.changePassword(changePasswordDto, authentication);
    }

    @PostMapping("/account/passwordRecovery")
    public void passwordRecovery(@Valid @RequestBody LoginDto loginDto) {
        accountService.passwordRecovery(loginDto);
    }

    @DeleteMapping("/account/delete")
    public void deleteAccount(Authentication authentication) {
        accountService.deleteAccount(authentication);
    }
}
