package com.teamarea.account.dao;

import com.teamarea.account.domain.entity.User;
import com.teamarea.account.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Component
@RequiredArgsConstructor
public class UserDao {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Transactional
    public User createUser(String login, String password) {
        User user = new User();
        user.setLogin(login);
        user.setPassword(passwordEncoder.encode(password));
        user.setConfirm(false);
        user.setCreateDate(new Date());
        user.setEnabled(true);
        return userRepository.saveAndFlush(user);
    }
}
