package com.teamarea.account.dao;

import com.teamarea.account.domain.entity.DeviceCode;
import com.teamarea.account.repository.DeviceCodeRepository;
import com.teamarea.account.utils.AppUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class DeviceCodeDao {

    private final DeviceCodeRepository deviceCodeRepository;
    private final AppUtils appUtils;

    public DeviceCode createDeviceCode(UUID uuid) {
        DeviceCode deviceCode = new DeviceCode();
        deviceCode.setUuid(uuid);
        deviceCode.setCode(appUtils.generateCode());
        deviceCode.setCreateDate(new Date());
        deviceCode.setTryCount(0);
        return deviceCodeRepository.saveAndFlush(deviceCode);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public DeviceCode saveAndCommit(DeviceCode deviceCode) {
        return deviceCodeRepository.saveAndFlush(deviceCode);
    }
}
