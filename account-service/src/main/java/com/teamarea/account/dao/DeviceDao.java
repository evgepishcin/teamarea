package com.teamarea.account.dao;

import com.teamarea.account.domain.entity.Device;
import com.teamarea.account.repository.DeviceCodeRepository;
import com.teamarea.account.repository.DeviceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class DeviceDao {

    private final DeviceRepository deviceRepository;
    private final DeviceCodeRepository deviceCodeRepository;

    @Transactional
    public Device createDevice(Long userId, String about) {
        Device device = new Device();
        device.setUserId(userId);
        device.setConfirm(false);
        device.setCreateDate(new Date());
        device.setAbout(about);
        return deviceRepository.saveAndFlush(device);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void deleteAndCommit(UUID uuid) {
        deviceCodeRepository.deleteById(uuid);
        deviceRepository.deleteById(uuid);
    }
}
