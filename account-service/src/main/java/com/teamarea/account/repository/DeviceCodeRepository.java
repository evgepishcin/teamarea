package com.teamarea.account.repository;

import com.teamarea.account.domain.entity.DeviceCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public interface DeviceCodeRepository extends JpaRepository<DeviceCode, UUID> {

    @Query("delete from DeviceCode dc where dc.uuid in (select d.uuid from Device d where d.userId = :userId) " +
            "and dc.uuid <> :uuid")
    @Modifying(flushAutomatically = true, clearAutomatically = true)
    void deleteAllByUserIdAndUuidNot(Long userId, UUID uuid);

    @Query("delete from DeviceCode dc where dc.uuid in (select d.uuid from Device d where d.userId = :userId)")
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    void deleteAllByUserId(Long userId);

    @Query("delete from DeviceCode dc where dc.uuid in (select d.uuid from Device d where d.userId in :usersIds)")
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    void deleteAllByUserIdIn(List<Long> usersIds);

    void deleteAllByCreateDateLessThan(Date date);
}
