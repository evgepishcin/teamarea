package com.teamarea.account.repository;

import com.teamarea.account.domain.entity.Device;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface DeviceRepository extends JpaRepository<Device, UUID> {

    void deleteAllByUserIdAndUuidNot(Long userId, UUID uuid);

    @Query("select d.uuid from Device d where d.userId = :userId and d.uuid <> :uuid")
    List<UUID> selectUuidByUserIdAndUuidNot(Long userId, UUID uuid);

    void deleteAllByUserId(Long userId);

    void deleteAllByUserIdIn(List<Long> usersIds);
}
