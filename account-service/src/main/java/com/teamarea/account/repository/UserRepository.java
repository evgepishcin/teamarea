package com.teamarea.account.repository;

import com.teamarea.account.domain.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByLogin(String login);

    List<User> findAllByConfirmIsFalseAndCreateDateLessThan(Date date);

    void deleteAllByIdIn(List<Long> ids);
}
