package com.teamarea.account.service;

import com.teamarea.account.api.dto.AccountMessageType;
import com.teamarea.account.api.dto.DeviceUuidList;
import com.teamarea.account.api.dto.UsersIdsList;
import com.teamarea.account.configuration.AccountProperties;
import com.teamarea.account.dao.DeviceCodeDao;
import com.teamarea.account.dao.DeviceDao;
import com.teamarea.account.dao.UserDao;
import com.teamarea.account.domain.exception.*;
import com.teamarea.account.domain.model.TokenInfo;
import com.teamarea.account.domain.dto.*;
import com.teamarea.account.domain.entity.Device;
import com.teamarea.account.domain.entity.DeviceCode;
import com.teamarea.account.domain.entity.User;
import com.teamarea.account.repository.DeviceCodeRepository;
import com.teamarea.account.repository.DeviceRepository;
import com.teamarea.account.repository.UserRepository;
import com.teamarea.account.security.JwtTokenProvider;
import com.teamarea.account.service.messages.Channels;
import com.teamarea.account.service.messages.MessagesService;
import com.teamarea.account.utils.AppUtils;
import com.teamarea.common.data.api.Operation;
import com.teamarea.mail.api.dto.DeviceConfirmationDto;
import com.teamarea.mail.api.dto.MailMessageType;
import com.teamarea.mail.api.dto.PasswordRecoveryDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class AccountService {

    private static final int MAX_CODE_TRY_COUNT = 3;

    private final UserRepository userRepository;
    private final DeviceRepository deviceRepository;
    private final DeviceCodeRepository deviceCodeRepository;
    private final UserDao userDao;
    private final DeviceDao deviceDao;
    private final DeviceCodeDao deviceCodeDao;
    private final RestTemplate restTemplate;
    private final AccountProperties accountProperties;
    private final JwtTokenProvider jwtTokenProvider;
    private final MessagesService messagesService;
    private final AppUtils appUtils;
    private final PasswordEncoder passwordEncoder;

    @Transactional
    public TokenDto registration(RegistrationDto registrationDto) {
        if (userRepository.findByLogin(registrationDto.getLogin()).isPresent()) {
            throw new UserAlreadyExistException();
        }
        User user = userDao.createUser(registrationDto.getLogin(), registrationDto.getPassword());
        Device device = deviceDao.createDevice(user.getId(), registrationDto.getAbout());
        DeviceCode deviceCode = deviceCodeDao.createDeviceCode(device.getUuid());
        RegisterEmailDto emailDto = new RegisterEmailDto(user.getId(), registrationDto.getEmail());
        ResponseEntity<?> response = restTemplate
                .postForEntity(accountProperties.getServices().getMailServiceUrl()
                                .concat("secure/mail/user/"), emailDto,
                        Void.class);
        if (!response.getStatusCode().equals(HttpStatus.OK) && !response.getStatusCode().equals(HttpStatus.CREATED)) {
            if (response.getStatusCode().equals(HttpStatus.CONFLICT)) {
                throw new UserAlreadyExistException();
            }
            throw new AccountAppException();
        }
        TokenDto tokenDto = new TokenDto(jwtTokenProvider.createToken(user.getLogin(), user.getId(), device.getUuid()));
        DeviceConfirmationDto confirmationDto = new DeviceConfirmationDto();
        confirmationDto.setUserId(user.getId());
        confirmationDto.setDeviceInfo(device.getAbout());
        confirmationDto.setCode(deviceCode.getCode());
        messagesService.sendMessage(Channels.MAIL, MailMessageType.DEVICE_CONFIRMATION,
                Operation.CREATE, confirmationDto);
        return tokenDto;
    }

    @Transactional
    public TokenDto authorization(AuthorizationDto authorizationDto) {
        User user = userRepository.findByLogin(authorizationDto.getLogin()).orElseThrow(UserNotFoundException::new);
        if (Boolean.FALSE.equals(user.getEnabled())) {
            throw new UserNotFoundException();
        }
        if (!passwordEncoder.matches(authorizationDto.getPassword(), user.getPassword())) {
            throw new WrongPasswordException();
        }
        Device device = deviceDao.createDevice(user.getId(), authorizationDto.getAbout());
        DeviceCode deviceCode = deviceCodeDao.createDeviceCode(device.getUuid());
        TokenDto tokenDto = new TokenDto(jwtTokenProvider.createToken(user.getLogin(), user.getId(), device.getUuid()));
        DeviceConfirmationDto confirmationDto = new DeviceConfirmationDto();
        confirmationDto.setUserId(user.getId());
        confirmationDto.setDeviceInfo(device.getAbout());
        confirmationDto.setCode(deviceCode.getCode());
        messagesService.sendMessage(Channels.MAIL, MailMessageType.DEVICE_CONFIRMATION,
                Operation.CREATE, confirmationDto);
        return tokenDto;
    }

    public TokenDeviceDto validateToken(TokenDto tokenDto) {
        TokenInfo tokenInfo = jwtTokenProvider.validateToken(tokenDto.getToken());
        appUtils.checkDeviceAndReturn(tokenInfo.getDeviceUuid(), true);
        return new TokenDeviceDto(tokenInfo.getUserId(), tokenInfo.getDeviceUuid());
    }

    @Transactional
    public void confirmDevice(String code, String token) {
        TokenInfo tokenInfo = jwtTokenProvider.validateToken(token);
        DeviceCode deviceCode = deviceCodeRepository.findById(tokenInfo.getDeviceUuid())
                .orElseThrow(AccountAppException::new);
        if (!code.equals(deviceCode.getCode())) {
            deviceCode.setTryCount(deviceCode.getTryCount() + 1);
            if (deviceCode.getTryCount() >= MAX_CODE_TRY_COUNT) {
                deviceDao.deleteAndCommit(deviceCode.getUuid());
                throw new MaxTryCountException();
            }
            deviceCodeDao.saveAndCommit(deviceCode);
            throw new WrongCodeException();
        }
        Device device = deviceRepository.findById(deviceCode.getUuid()).orElseThrow(AccountAppException::new);
        User user = userRepository.findByLogin(tokenInfo.getUsername()).orElseThrow(AccountAppException::new);
        deviceCodeRepository.deleteById(deviceCode.getUuid());
        device.setConfirm(true);
        deviceRepository.save(device);
        if (Boolean.FALSE.equals(user.getConfirm())) {
            user.setConfirm(true);
            userRepository.save(user);
        }
    }

    @Transactional
    public void logout(String token) {
        TokenInfo tokenInfo = jwtTokenProvider.validateToken(token);
        Device device = appUtils.checkDeviceAndReturn(tokenInfo.getDeviceUuid(), false);
        if (deviceCodeRepository.existsById(device.getUuid())) {
            deviceCodeRepository.deleteById(device.getUuid());
        }
        deviceRepository.deleteById(device.getUuid());
        TokenGatewayDto tokenGatewayDto = new TokenGatewayDto(token, device.getUserId(), device.getUuid());
        restTemplate
                .postForEntity(accountProperties.getServices().getGatewayServiceUrl()
                        .concat("secure/gateway/jwt"), tokenGatewayDto, Void.class);
        List<UUID> uuids = List.of(device.getUuid());
        DeviceUuidList deviceUuidList = new DeviceUuidList(uuids);
        messagesService.sendMessage(Channels.ACCOUNT, AccountMessageType.DEVICE_UUID_LIST,
                Operation.DELETE, deviceUuidList);
    }

    @Transactional
    public void logoutAll(Authentication authentication) {
        Device device = appUtils.checkDeviceAndReturn(UUID.fromString(authentication.getCredentials().toString()),
                true);
        List<UUID> deleteUuids = deviceRepository.selectUuidByUserIdAndUuidNot(device.getUserId(), device.getUuid());
        deviceCodeRepository.deleteAllByUserIdAndUuidNot(device.getUserId(), device.getUuid());
        deviceRepository.deleteAllByUserIdAndUuidNot(device.getUserId(), device.getUuid());
        restTemplate.delete(accountProperties.getServices().getGatewayServiceUrl()
                .concat("/secure/gateway/user?userId=").concat(String.valueOf(device.getUserId())));
        DeviceUuidList deviceUuidList = new DeviceUuidList(deleteUuids);
        messagesService.sendMessage(Channels.ACCOUNT, AccountMessageType.DEVICE_UUID_LIST,
                Operation.DELETE, deviceUuidList);
    }

    @Transactional
    public void changePassword(ChangePasswordDto changePasswordDto, Authentication authentication) {
        Device device = appUtils.checkDeviceAndReturn(UUID.fromString(authentication.getCredentials().toString()),
                true);
        User user = userRepository.findById(device.getUserId()).orElseThrow(AccountAppException::new);
        if (!passwordEncoder.matches(changePasswordDto.getOldPassword(), user.getPassword())) {
            throw new WrongPasswordException();
        }
        user.setPassword(passwordEncoder.encode(changePasswordDto.getNewPassword()));
    }

    @Transactional
    public void passwordRecovery(LoginDto loginDto) {
        User user = userRepository.findByLogin(loginDto.getLogin()).orElseThrow(UserNotFoundException::new);
        if (Boolean.FALSE.equals(user.getEnabled())) {
            throw new UserNotFoundException();
        }
        String newPassword = appUtils.generateCode();
        user.setPassword(passwordEncoder.encode(newPassword));
        userRepository.save(user);
        PasswordRecoveryDto passwordRecoveryDto = new PasswordRecoveryDto();
        passwordRecoveryDto.setUserId(user.getId());
        passwordRecoveryDto.setNewPassword(newPassword);
        messagesService.sendMessage(Channels.MAIL, MailMessageType.PASSWORD_RECOVERY,
                Operation.CREATE, passwordRecoveryDto);
    }

    @Transactional
    public void deleteAccount(Authentication authentication) {
        Device device = appUtils.checkDeviceAndReturn(UUID.fromString(authentication.getCredentials().toString()),
                true);
        deviceCodeRepository.deleteAllByUserId(device.getUserId());
        deviceRepository.deleteAllByUserId(device.getUserId());
        User user = userRepository.findById(device.getUserId()).orElseThrow(AccountAppException::new);
        user.setEnabled(false);
        userRepository.save(user);
        restTemplate.delete(accountProperties.getServices().getGatewayServiceUrl()
                .concat("/secure/gateway/user?userId=").concat(String.valueOf(device.getUserId())));
        List<Long> deleteId = List.of(user.getId());
        UsersIdsList usersIdsList = new UsersIdsList(deleteId);
        messagesService.sendMessage(Channels.ACCOUNT, AccountMessageType.USERS_ID_LIST,
                Operation.DELETE, usersIdsList);
    }

    @Transactional
    public void deleteUnconfirmedUsers() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.HOUR_OF_DAY, -1);
        Date deleteTime = calendar.getTime();
        List<User> deleteUsers = userRepository.findAllByConfirmIsFalseAndCreateDateLessThan(deleteTime);
        List<Long> deleteUsersIds = deleteUsers.stream().map(User::getId).toList();
        deviceCodeRepository.deleteAllByUserIdIn(deleteUsersIds);
        deviceRepository.deleteAllByUserIdIn(deleteUsersIds);
        userRepository.deleteAllByIdIn(deleteUsersIds);
        UsersIdsList usersIdsList = new UsersIdsList(deleteUsersIds);
        messagesService.sendMessage(Channels.ACCOUNT, AccountMessageType.USERS_ID_LIST,
                Operation.DELETE, usersIdsList);
    }

    @Transactional
    public void deleteOldDeviceCodes() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.HOUR_OF_DAY, -1);
        Date deleteTime = calendar.getTime();
        deviceCodeRepository.deleteAllByCreateDateLessThan(deleteTime);
    }
}
