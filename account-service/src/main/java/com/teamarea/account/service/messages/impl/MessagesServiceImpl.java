package com.teamarea.account.service.messages.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.teamarea.account.service.messages.MessagesService;
import com.teamarea.common.data.api.TransferData;
import com.teamarea.common.data.api.TransferDataWithUserIds;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class MessagesServiceImpl implements MessagesService {

    private final KafkaTemplate<String, String> kafkaTemplate;
    private final ObjectMapper objectMapper;

    @SneakyThrows
    @Override
    public void sendMessage(String channel, String messageType, String operation, Object payload) {
        String strPayload = objectMapper.writeValueAsString(payload);
        TransferData transferData = new TransferData(messageType, operation, strPayload);
        String strTransfer = objectMapper.writeValueAsString(transferData);
        kafkaTemplate.send(channel, strTransfer);
    }

    @SneakyThrows
    @Override
    public void sendMessage(String channel, String messageType, String operation, Object payload, List<Long> usersIds) {
        String strPayload = objectMapper.writeValueAsString(payload);
        TransferDataWithUserIds transferDataWithUserIds =
                new TransferDataWithUserIds(messageType, usersIds, operation, strPayload);
        String strTransfer = objectMapper.writeValueAsString(transferDataWithUserIds);
        kafkaTemplate.send(channel, strTransfer);
    }
}
