package com.teamarea.account.service.messages;

import lombok.experimental.UtilityClass;

/**
 * Канал для отправки сообщений в брокер сообщений
 */
@UtilityClass
public class Channels {
    public final String MAIL = "mail";
    public final String ACCOUNT = "account";
}
