package com.teamarea.account.service;

import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ScheduledService {

    private final AccountService accountService;

    @Scheduled(fixedDelay = 300000)
    public void deleteUnconfirmedUsers() {
        accountService.deleteUnconfirmedUsers();
    }

    @Scheduled(fixedDelay = 300000)
    public void deleteOldDeviceCodes() {
        accountService.deleteOldDeviceCodes();
    }
}
