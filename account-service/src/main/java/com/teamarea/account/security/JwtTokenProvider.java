package com.teamarea.account.security;

import com.teamarea.account.configuration.AccountProperties;
import com.teamarea.account.domain.exception.InvalidTokenException;
import com.teamarea.account.domain.model.TokenInfo;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class JwtTokenProvider {

    private static final String USER_ID = "userId";
    private static final String DEVICE_UUID = "deviceUuid";

    private final AccountProperties accountProperties;
    private final UserDetailsService userDetailsService;

    public String createToken(String login, Long userId, UUID deviceUuid) {
        Claims claims = Jwts.claims()
                .setSubject(login);
        claims.put(USER_ID, userId);
        claims.put(DEVICE_UUID, deviceUuid);
        Date now = new Date();
        Date validity = new Date(now.getTime() + accountProperties.getJwt().getValidityMilliseconds());
        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(validity)
                .signWith(SignatureAlgorithm.HS256, accountProperties.getJwt().getSecret())
                .compact();
    }

    public TokenInfo validateToken(String token) throws InvalidTokenException {
        String substrToken = token.substring(7);
        try {
            Jws<Claims> claimsJws = Jwts.parser().setSigningKey(accountProperties
                    .getJwt().getSecret()).parseClaimsJws(substrToken);
            if (claimsJws.getBody().getExpiration().before(new Date())) {
                throw new InvalidTokenException();
            }
            UUID uuid = UUID.fromString(Jwts.parser().setSigningKey(accountProperties.getJwt().getSecret())
                    .parseClaimsJws(substrToken).getBody().get(DEVICE_UUID, String.class));
            String username = Jwts.parser().setSigningKey(accountProperties.getJwt().getSecret())
                    .parseClaimsJws(substrToken).getBody().getSubject();
            Long userId = Jwts.parser().setSigningKey(accountProperties.getJwt().getSecret())
                    .parseClaimsJws(substrToken).getBody().get(USER_ID, Long.class);
            return new TokenInfo(username, userId, uuid);
        } catch (Exception e) {
            throw new InvalidTokenException();
        }
    }

    public Authentication getAuthentication(String token) {
        String substrToken = token.substring(7);
        UserDetails userDetails = userDetailsService.loadUserByUsername(getUsername(substrToken));
        UUID uuid = UUID.fromString(Jwts.parser().setSigningKey(accountProperties.getJwt().getSecret())
                .parseClaimsJws(substrToken).getBody().get(DEVICE_UUID, String.class));
        return new UsernamePasswordAuthenticationToken(userDetails, uuid, userDetails.getAuthorities());
    }

    public String getUsername(String token) {
        return Jwts.parser().setSigningKey(accountProperties.getJwt().getSecret())
                .parseClaimsJws(token).getBody().getSubject();
    }

}
