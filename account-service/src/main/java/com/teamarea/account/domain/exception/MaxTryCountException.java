package com.teamarea.account.domain.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class MaxTryCountException extends RuntimeException {
    public MaxTryCountException() {
        super("You have reached the limit for the number of attempts!");
    }
}
