package com.teamarea.account.domain.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "teamarea_user")
@Getter
@Setter
@NoArgsConstructor
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "teamarea_user_id_gen")
    @SequenceGenerator(name = "teamarea_user_id_gen", sequenceName = "user_id_seq", allocationSize = 1)
    private Long id;
    private String login;
    private String password;
    private Boolean confirm;
    private Date createDate;
    private Boolean enabled;
}
