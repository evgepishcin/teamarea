package com.teamarea.account.domain.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
import java.util.UUID;

@Entity
@Table
@Getter
@Setter
@NoArgsConstructor
public class DeviceCode {

    @Id
    private UUID uuid;
    private String code;
    private Integer tryCount;
    private Date createDate;
}
