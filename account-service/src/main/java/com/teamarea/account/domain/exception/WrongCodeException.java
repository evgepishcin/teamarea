package com.teamarea.account.domain.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class WrongCodeException extends RuntimeException {
    public WrongCodeException() {
        super("Wrong code!");
    }
}
