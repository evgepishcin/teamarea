package com.teamarea.account.domain.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class MailAlreadyExistException extends RuntimeException {
    public MailAlreadyExistException() {
        super("Another user is already using this mail!");
    }
}
