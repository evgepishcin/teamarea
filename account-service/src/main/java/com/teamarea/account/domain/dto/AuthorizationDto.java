package com.teamarea.account.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AuthorizationDto {
    @NotBlank
    private String login;

    @NotBlank
    private String password;

    @NotBlank
    private String about;
}
