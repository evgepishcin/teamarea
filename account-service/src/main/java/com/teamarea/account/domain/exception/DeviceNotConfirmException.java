package com.teamarea.account.domain.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class DeviceNotConfirmException extends RuntimeException {
    public DeviceNotConfirmException() {
        super("Device not confirm!");
    }
}
