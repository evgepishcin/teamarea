package com.teamarea.account.utils;

import com.teamarea.account.domain.exception.DeviceNotConfirmException;
import com.teamarea.account.domain.entity.Device;
import com.teamarea.account.repository.DeviceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Random;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class AppUtils {

    private static final int MIN_CODE_LENGTH = 10;
    private static final int MAX_CODE_LENGTH = 20;
    private final Random random = new Random();
    private final DeviceRepository deviceRepository;

    public String generateCode() {
        int len = random.nextInt(MAX_CODE_LENGTH - MIN_CODE_LENGTH) + MIN_CODE_LENGTH;
        RandomString randomString = new RandomString(len);
        return randomString.nextString();
    }

    public Device checkDeviceAndReturn(UUID deviceUuid, boolean checkConfirm) {
        Device device = deviceRepository.findById(deviceUuid).orElseThrow(DeviceNotConfirmException::new);
        if (checkConfirm && Boolean.FALSE.equals(device.getConfirm())) {
            throw new DeviceNotConfirmException();
        }
        return device;
    }
}
