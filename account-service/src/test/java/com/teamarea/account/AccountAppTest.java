package com.teamarea.account;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;

@SpringBootTest
@TestConfiguration
class AccountAppTest {

    @Test
    @SuppressWarnings("java:S2699")
    void contextLoads() {
    }
}
