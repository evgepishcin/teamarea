package com.teamarea.group.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Участник группы
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ParticipantDtoApi {

    /**
     * Идентификатор участника группы
     */
    private Long id;

    /**
     * Идентификатор группы
     */
    private Long groupId;

    /**
     * Идентификатор пользователя системы
     */
    private Long userId;

    /**
     * Статус пользователя (активный или удаленный)
     */
    private Boolean active;

    /**
     * Статус модератора
     */
    private Boolean moderator;

    /**
     * Идентификатор роли участника
     */
    private Long roleId;
}
