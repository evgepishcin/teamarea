package com.teamarea.group.api.dto;

import lombok.experimental.UtilityClass;

@UtilityClass
public class GroupMessageTypeApi {
    public final String PARTICIPANT = "PARTICIPANT";
    public final String ROLE = "ROLE";
    public final String GROUP = "GROUP";
    public final String EVENT = "EVENT";
}
