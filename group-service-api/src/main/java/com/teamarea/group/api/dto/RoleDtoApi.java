package com.teamarea.group.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Роль для участников группы
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RoleDtoApi {

    /**
     * Идентификатор роли
     */
    private Long id;

    /**
     * Идентификатор группы
     */
    private Long groupId;
}
