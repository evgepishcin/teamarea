package com.teamarea.group.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Группа
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GroupDtoApi {

    /**
     * Идентификатор группы
     */
    private Long id;

    /**
     * Имя группы
     */
    private String name;

    /**
     * Информация о группе
     */
    private String about;
}
