package com.teamarea.group.repository;

import com.teamarea.group.domain.entity.TimetableParticipant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface TimetableParticipantRepository extends JpaRepository<TimetableParticipant, Long> {

    @Query("DELETE FROM TimetableParticipant tp WHERE tp.timetableId = :timetableId")
    @Modifying(flushAutomatically = true, clearAutomatically = true)
    void deleteAllByTimetableId(Long timetableId);

    @Query("SELECT tp FROM TimetableParticipant tp where tp.participantId = :participantId AND " +
            "tp.timetableId = :timetableId")
    Optional<TimetableParticipant> findByParticipantIdAndTimetableId(Long participantId, Long timetableId);

    @Query("SELECT tp.participantId FROM TimetableParticipant tp WHERE tp.timetableId = :timetableId")
    List<Long> selectAllParticipantsIdsByTimetableId(Long timetableId);

    @Query("DELETE FROM TimetableParticipant tp where tp.participantId = :participantId")
    @Modifying(flushAutomatically = true, clearAutomatically = true)
    void deleteAllByParticipantId(Long participantId);

    @Query("DELETE FROM TimetableParticipant tp where tp.timetableId in " +
            "(SELECT t.id FROM Timetable t where t.groupId = :groupId)")
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    void deleteAllByGroupId(Long groupId);
}
