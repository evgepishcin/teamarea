package com.teamarea.group.repository;

import com.teamarea.group.domain.entity.EventNotificationMail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface EventNotificationMailRepository extends JpaRepository<EventNotificationMail, Long> {
    @Query("SELECT p.userId FROM Participant p join EventNotificationMail enm on p.id = enm.participantId" +
            " WHERE enm.participantId in :ids and p.userId is not null ")
    List<Long> getUsersIdsByParticipantIdsIn(List<Long> ids);

    @Query("DELETE FROM EventNotificationMail enm WHERE enm.participantId = :participantId")
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    void deleteAllByParticipantId(Long participantId);


    @Query("SELECT enm FROM EventNotificationMail enm WHERE enm.participantId = :participantId")
    Optional<EventNotificationMail> findByParticipantId(Long participantId);

    @Query("DELETE FROM EventNotificationMail ent WHERE ent.participantId in " +
            "(SELECT p.id FROM Participant p WHERE p.groupId = :groupId)")
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    void deleteAllByGroupId(Long groupId);
}
