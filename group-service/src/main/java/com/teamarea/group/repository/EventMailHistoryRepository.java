package com.teamarea.group.repository;

import com.teamarea.group.domain.entity.EventMailHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface EventMailHistoryRepository extends JpaRepository<EventMailHistory, Long> {
    @Query("DELETE FROM EventMailHistory emh where emh.eventId = :eventId")
    @Modifying(flushAutomatically = true, clearAutomatically = true)
    void deleteAllByEventId(Long eventId);

    @Query("DELETE FROM EventMailHistory emh where emh.eventId in" +
            " (SELECT e.id FROM Event e where e.groupId = :groupId)")
    @Modifying(flushAutomatically = true, clearAutomatically = true)
    void deleteAllByGroupId(Long groupId);
}
