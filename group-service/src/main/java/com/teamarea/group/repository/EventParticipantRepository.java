package com.teamarea.group.repository;

import com.teamarea.group.domain.entity.EventParticipant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface EventParticipantRepository extends JpaRepository<EventParticipant, Long> {
    @Query("DELETE FROM EventParticipant ep WHERE ep.eventId = :eventId")
    @Modifying(flushAutomatically = true, clearAutomatically = true)
    void deleteAllByEventId(Long eventId);

    @Query("SELECT p.userId FROM Participant p join EventParticipant ep on p.id = ep.participantId" +
            " where p.userId is not null and ep.eventId = :eventId")
    List<Long> selectUserIdsByEventId(Long eventId);

    @Query("SELECT ep FROM EventParticipant ep WHERE ep.participantId = :participantId and ep.eventId = :eventId")
    Optional<EventParticipant> findByParticipantIdAndEventId(Long participantId, Long eventId);

    @Query("SELECT ep.participantId FROM EventParticipant ep WHERE ep.eventId = :eventId")
    List<Long> selectAllParticipantsIdsByEventId(Long eventId);

    @Query("DELETE FROM EventParticipant ep where ep.participantId = :participantId")
    @Modifying(flushAutomatically = true, clearAutomatically = true)
    void deleteAllByParticipantId(Long participantId);

    @Query("DELETE FROM EventParticipant ep where ep.eventId in " +
            "(SELECT e.id FROM Event e where e.groupId = :groupId)")
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    void deleteAllByGroupId(Long groupId);
}
