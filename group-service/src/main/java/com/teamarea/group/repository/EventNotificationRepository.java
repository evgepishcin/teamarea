package com.teamarea.group.repository;

import com.teamarea.group.domain.entity.EventNotification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface EventNotificationRepository extends JpaRepository<EventNotification, Long> {


    @Query("SELECT p.userId FROM Participant p join EventNotification en on p.id = en.participantId" +
            " WHERE en.participantId in :ids and p.userId is not null ")
    List<Long> getUsersIdsByParticipantIdsIn(List<Long> ids);

    @Query("DELETE FROM EventNotification en WHERE en.participantId = :participantId")
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    void deleteAllByParticipantId(Long participantId);

    @Query("SELECT en FROM EventNotification en WHERE en.participantId = :participantId")
    Optional<EventNotification> findByParticipantId(Long participantId);

    @Query("DELETE FROM EventNotification en WHERE en.participantId in " +
            "(SELECT p.id FROM Participant p WHERE p.groupId = :groupId)")
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    void deleteAllByGroupId(Long groupId);
}
