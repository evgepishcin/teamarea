package com.teamarea.group.repository;

import com.teamarea.group.domain.entity.Timetable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TimetableRepository extends JpaRepository<Timetable, Long> {

    @Query("SELECT t FROM Timetable t where t.groupId = :groupId")
    List<Timetable> findAllByGroupId(Long groupId);

    @Query("SELECT t FROM Timetable t join TimetableRole tr on t.id = tr.timetableId where tr.roleId = :roleId")
    List<Timetable> findAllByRoleId(Long roleId);

    @Query("SELECT t FROM Timetable t join TimetableParticipant tp on t.id = tp.timetableId" +
            " where tp.participantId = :participantId")
    List<Timetable> findAllByParticipantId(Long participantId);

    @Query("DELETE FROM Timetable t where t.groupId = :groupId")
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    void deleteAllByGroupId(Long groupId);
}
