package com.teamarea.group.repository;

import com.teamarea.group.domain.entity.TimetableRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface TimetableRoleRepository extends JpaRepository<TimetableRole, Long> {

    @Query("DELETE FROM TimetableRole tr WHERE tr.timetableId = :timetableId")
    @Modifying(flushAutomatically = true, clearAutomatically = true)
    void deleteAllByTimetableId(Long timetableId);

    @Query("DELETE FROM TimetableRole  tr where tr.roleId = :roleId")
    @Modifying(flushAutomatically = true, clearAutomatically = true)
    void deleteAllByRoleId(Long roleId);

    @Query("SELECT tr FROM TimetableRole tr WHERE tr.roleId = :roleId AND tr.timetableId = :timetableId")
    Optional<TimetableRole> findByRoleIdAndTimetableId(Long roleId, Long timetableId);

    @Query("SELECT tr.roleId FROM TimetableRole tr where tr.timetableId = :timetableId")
    List<Long> selectAllRolesIdsByTimetableId(Long timetableId);

    @Query("DELETE FROM TimetableRole tr where tr.timetableId in " +
            "(SELECT t.id FROM Timetable t where t.groupId = :groupId)")
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    void deleteAllByGroupId(Long groupId);
}
