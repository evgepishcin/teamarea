package com.teamarea.group.repository;

import com.teamarea.group.domain.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RoleRepository extends JpaRepository<Role, Long> {

    @Query("SELECT r FROM Role r where r.groupId = :groupId")
    List<Role> findAllByGroupId(Long groupId);

    @Query("SELECT r FROM Role r where r.id in :ids and r.groupId = :groupId")
    List<Role> findAllByIdInAndGroupIdEquals(List<Long> ids, Long groupId);

    @Query("DELETE FROM Role r where r.groupId = :groupId")
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    void deleteAllByGroupId(Long groupId);
}
