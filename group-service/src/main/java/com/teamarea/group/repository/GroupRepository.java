package com.teamarea.group.repository;

import com.teamarea.group.domain.entity.Group;
import com.teamarea.group.domain.pojo.GroupInfoPojo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface GroupRepository extends JpaRepository<Group, Long> {

    @Query("SELECT new com.teamarea.group.domain.pojo.GroupInfoPojo(g.id, g.name," +
            " p.moderator, p.roleId)" +
            "from Group g join Participant p on g.id = p.groupId where p.userId = :userId")
    List<GroupInfoPojo> getUserGroups(Long userId);

    @Query("DELETE FROM Group g where g.id = :groupId")
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    void deleteAllByGroupId(Long groupId);
}
