package com.teamarea.group.repository;

import com.teamarea.group.domain.entity.Participant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ParticipantRepository extends JpaRepository<Participant, Long> {

    @Query("SELECT p FROM Participant p WHERE p.userId = :userId and p.groupId = :groupId")
    Optional<Participant> findByUserIdAndGroupId(Long userId, Long groupId);

    @Query("SELECT p FROM Participant p WHERE p.groupId = :groupId and p.moderator = true and p.active = true")
    List<Participant> findAllByGroupIdAndModeratorIsTrueAndActiveIsTrue(Long groupId);

    @Query("SELECT p FROM Participant p WHERE p.groupId = :groupId and p.active = true")
    List<Participant> findAllByGroupIdAndActiveIsTrue(Long groupId);

    @Query("SELECT p FROM Participant p WHERE p.groupId = :groupId and p.roleId = :roleId and p.active = true")
    List<Participant> findAllByGroupIdAndRoleIdAndActiveIsTrue(Long groupId, Long roleId);

    @Query("SELECT p FROM Participant p WHERE p.id IN :ids AND p.groupId = :groupId and p.active = true ")
    List<Participant> findAllByIdInAndGroupIdEqualsAndActive(List<Long> ids, Long groupId);

    @Query("SELECT p FROM Participant p WHERE p.invitationCode = :code")
    Optional<Participant> findByInvitationCode(String code);

    @Query("UPDATE Participant p set p.roleId = null where p.roleId = :roleId")
    @Modifying(flushAutomatically = true, clearAutomatically = true)
    void removeRole(Long roleId);

    @Query("DELETE FROM Participant p where p.groupId = :groupId")
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    void deleteAllByGroupId(Long groupId);
}
