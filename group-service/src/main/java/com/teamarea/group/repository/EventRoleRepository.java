package com.teamarea.group.repository;

import com.teamarea.group.domain.entity.EventRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface EventRoleRepository extends JpaRepository<EventRole, Long> {
    @Query("DELETE FROM EventRole ev where ev.eventId = :eventId")
    @Modifying(flushAutomatically = true, clearAutomatically = true)
    void deleteAllByEventId(Long eventId);

    @Query("DELETE FROM EventRole er where er.roleId = :roleId")
    @Modifying(flushAutomatically = true, clearAutomatically = true)
    void deleteAllByRoleId(Long roleId);

    @Query("SELECT p.userId FROM Participant p join Role r on p.roleId = r.id" +
            " join EventRole ev on ev.roleId = r.id where p.userId is not null and ev.eventId = :eventId")
    List<Long> selectUserIdsByEventId(Long eventId);

    @Query("SELECT ev FROM EventRole ev WHERE ev.roleId = :roleId AND ev.eventId = :eventId")
    Optional<EventRole> findByRoleIdAndEventId(Long roleId, Long eventId);

    @Query("SELECT er.roleId FROM EventRole er WHERE er.eventId = :eventId")
    List<Long> selectAllRolesIdsByEventId(Long eventId);

    @Query("DELETE FROM EventRole er WHERE er.eventId IN " +
            "(SELECT e.id FROM Event e where e.groupId = :groupId)")
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    void deleteAllByGroupId(Long groupId);
}
