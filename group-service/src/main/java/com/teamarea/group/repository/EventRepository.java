package com.teamarea.group.repository;

import com.teamarea.group.domain.entity.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface EventRepository extends JpaRepository<Event, Long> {
    @Query("SELECT e FROM Event e WHERE e.groupId = :groupId")
    List<Event> findAllByGroupId(Long groupId);

    @Query("SELECT e FROM Event e join EventRole er on e.id = er.eventId where er.roleId = :roleId")
    List<Event> findAllByRoleId(Long roleId);

    @Query("SELECT e FROM Event e join EventParticipant ep on e.id = ep.eventId where ep.participantId = :participantId")
    List<Event> findAllByParticipantId(Long participantId);

    @Query("SELECT e FROM Event e left join EventMailHistory emh on e.id = emh.eventId where e.date < :date " +
            "and emh.eventId is null")
    List<Event> findAllByForEmailReminder(Date date);

    @Query("DELETE FROM Event e where e.groupId = :groupId")
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    void deleteAllByGroupId(Long groupId);
}
