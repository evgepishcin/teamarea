package com.teamarea.group.domain.mapper;

import com.teamarea.group.domain.dto.NewTimetableDto;
import com.teamarea.group.domain.entity.Timetable;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface NewTimetableDtoMapper {
    Timetable newTimeTableDtoToTimetable(NewTimetableDto source);
}
