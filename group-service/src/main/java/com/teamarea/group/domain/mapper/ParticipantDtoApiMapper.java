package com.teamarea.group.domain.mapper;

import com.teamarea.group.api.dto.ParticipantDtoApi;
import com.teamarea.group.domain.entity.Participant;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ParticipantDtoApiMapper {
    ParticipantDtoApi participantToParticipantDtoApi(Participant source);
}
