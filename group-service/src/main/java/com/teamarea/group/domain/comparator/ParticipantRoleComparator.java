package com.teamarea.group.domain.comparator;

import com.teamarea.group.domain.entity.Participant;

import java.util.Comparator;

public class ParticipantRoleComparator implements Comparator<Participant> {
    @Override
    public int compare(Participant o1, Participant o2) {
        if (o1.getRoleId() == null && o2.getRoleId() == null) {
            return 0;
        } else if (o1.getRoleId() != null && o2.getRoleId() == null) {
            return -1;
        } else if (o1.getRoleId() == null && o2.getRoleId() != null) {
            return 1;
        } else {
            return o1.getRoleId().compareTo(o2.getRoleId());
        }
    }
}
