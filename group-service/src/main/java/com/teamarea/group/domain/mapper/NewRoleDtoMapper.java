package com.teamarea.group.domain.mapper;

import com.teamarea.group.domain.dto.NewRoleDto;
import com.teamarea.group.domain.entity.Role;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface NewRoleDtoMapper {
    Role newRoleDtoToRole(NewRoleDto source);
}
