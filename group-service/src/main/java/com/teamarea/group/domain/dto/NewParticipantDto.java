package com.teamarea.group.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class NewParticipantDto {
    private Boolean moderator;
    private String surname;
    private String name;
    private String patronymic;
    private Long roleId;
}
