package com.teamarea.group.domain.mapper;

import com.teamarea.group.api.dto.RoleDtoApi;
import com.teamarea.group.domain.entity.Role;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface RoleDtoApiMapper {
    RoleDtoApi roleToRoleDtoApi(Role source);
}
