package com.teamarea.group.domain.mapper;

import com.teamarea.group.domain.entity.Event;
import com.teamarea.mail.api.dto.EventDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface MailEventDtoMapper {
    EventDto eventToMailEventDto(Event source);
}
