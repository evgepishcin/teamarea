package com.teamarea.group.domain.mapper;

import com.teamarea.group.domain.dto.ParticipantDto;
import com.teamarea.group.domain.entity.Participant;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper(componentModel = "spring")
public interface ParticipantDtoMapper {
    Participant participantDtoToParticipant(ParticipantDto source);

    @Mapping(source = "userId", target = "accountConnected", qualifiedByName = "accountConnected")
    ParticipantDto participantToParticipantDto(Participant source);

    @Named("accountConnected")
    default Boolean accountConnectedMapping(Long userId) {
        return userId != null;
    }
}
