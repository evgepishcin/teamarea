package com.teamarea.group.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TimetableDto {
    private Long id;
    private Long groupId;
    private String name;
    private String about;
    private Integer dayOfWeek;
    private Integer hours;
    private Integer minutes;
    private Integer weekParity;
    private Integer monthWeekNum;
    private Integer weekParityDay;
    private Date weekParityDaySet;
    private Integer dayParity;
    private Date dayParitySet;
    private Boolean includeWeekends;
}
