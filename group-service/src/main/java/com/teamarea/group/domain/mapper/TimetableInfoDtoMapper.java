package com.teamarea.group.domain.mapper;

import com.teamarea.group.domain.dto.TimetableInfoDto;
import com.teamarea.group.domain.entity.Timetable;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface TimetableInfoDtoMapper {
    TimetableInfoDto timetableToTimetableInfoDto(Timetable source);
}
