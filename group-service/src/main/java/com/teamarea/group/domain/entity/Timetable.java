package com.teamarea.group.domain.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "timetable")
@Getter
@Setter
@NoArgsConstructor
public class Timetable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "timetable_id_gen")
    @SequenceGenerator(name = "timetable_id_gen", sequenceName = "timetable_id_seq", allocationSize = 1)
    private Long id;
    private Long groupId;
    private String name;
    private String about;
    private Integer dayOfWeek;
    private Integer hours;
    private Integer minutes;
    private Integer weekParity;
    private Integer monthWeekNum;
    private Integer weekParityDay;
    private Date weekParityDaySet;
    private Integer dayParity;
    private Date dayParitySet;
    private Boolean includeWeekends;
}
