package com.teamarea.group.domain.comparator;

import com.teamarea.group.domain.entity.Participant;

import java.util.Comparator;

public class ParticipantAlphabetComparator implements Comparator<Participant> {
    @Override
    public int compare(Participant o1, Participant o2) {
        int result = compareString(o1.getSurname(), o2.getSurname());
        if (result == 0) {
            result = compareString(o1.getName(), o2.getName());
            if (result == 0) {
                result = compareString(o1.getPatronymic(), o2.getPatronymic());
            }
        }
        return result;
    }

    private int compareString(String o1, String o2) {
        if (o1 == null && o2 == null) {
            return 0;
        } else if (o1 != null && o2 == null) {
            return 1;
        } else if (o1 == null) {
            return -1;
        } else {
            return o1.compareTo(o2);
        }
    }
}
