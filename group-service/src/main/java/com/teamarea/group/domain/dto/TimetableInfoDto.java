package com.teamarea.group.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TimetableInfoDto {
    private Long id;
    private Long groupId;
    private String name;
    private String about;
    private Integer dayOfWeek;
    private Integer hours;
    private Integer minutes;
    private Integer weekParity;
    private Integer monthWeekNum;
    private Integer weekParityDay;
    private Date weekParityDaySet;
    private Integer dayParity;
    private Date dayParitySet;
    private Boolean includeWeekends;
    private List<Long> roles;
    private List<Long> participants;
}
