package com.teamarea.group.domain.mapper;

import com.teamarea.group.domain.dto.EventDto;
import com.teamarea.group.domain.entity.Event;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface EventDtoMapper {
    EventDto eventToEventDto(Event source);
}
