package com.teamarea.group.domain.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "event_mail_history")
@Getter
@Setter
@NoArgsConstructor
public class EventMailHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "event_mail_history_id_gen")
    @SequenceGenerator(name = "event_mail_history_id_gen", sequenceName = "event_mail_history_id_seq", allocationSize = 1)
    private Long id;
    private Long eventId;

    public EventMailHistory(Long eventId) {
        this.eventId = eventId;
    }
}
