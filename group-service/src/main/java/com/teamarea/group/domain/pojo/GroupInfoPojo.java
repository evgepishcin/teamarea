package com.teamarea.group.domain.pojo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GroupInfoPojo {
    private Long id;
    private String name;
    private Boolean moderator;
    private Long roleId;
}
