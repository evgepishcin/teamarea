package com.teamarea.group.domain.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "event_role")
@Getter
@Setter
@NoArgsConstructor
public class EventRole {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "event_role_id_gen")
    @SequenceGenerator(name = "event_role_id_gen", sequenceName = "event_role_id_seq", allocationSize = 1)
    private Long id;
    private Long eventId;
    private Long roleId;

    public EventRole(Long eventId, Long roleId) {
        this.eventId = eventId;
        this.roleId = roleId;
    }
}
