package com.teamarea.group.domain.mapper;

import com.teamarea.group.domain.dto.NewParticipantDto;
import com.teamarea.group.domain.entity.Participant;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface NewParticipantDtoMapper {
    Participant newParticipantDtoToParticipant(NewParticipantDto source);
}
