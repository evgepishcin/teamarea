package com.teamarea.group.domain.mapper;

import com.teamarea.group.domain.dto.EventInfoDto;
import com.teamarea.group.domain.entity.Event;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface EventInfoDtoMapper {
    EventInfoDto eventToEventInfoDto(Event source);
}
