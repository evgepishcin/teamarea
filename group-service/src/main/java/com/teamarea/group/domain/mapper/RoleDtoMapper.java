package com.teamarea.group.domain.mapper;

import com.teamarea.group.domain.dto.RoleDto;
import com.teamarea.group.domain.entity.Role;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface RoleDtoMapper {
    RoleDto roleToRoleDto(Role source);
}
