package com.teamarea.group.domain.mapper;

import com.teamarea.group.api.dto.GroupDtoApi;
import com.teamarea.group.domain.entity.Group;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface GroupDtoApiMapper {
    GroupDtoApi groupToGroupDtoApi(Group source);
}
