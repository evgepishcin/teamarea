package com.teamarea.group.domain.mapper;

import com.teamarea.group.domain.dto.NewGroupDto;
import com.teamarea.group.domain.entity.Group;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface NewGroupDtoMapper {
    Group newGroupDtoToGroup(NewGroupDto source);
}
