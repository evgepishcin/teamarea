package com.teamarea.group.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EventInfoDto {
    private Long id;
    private Long groupId;
    private String name;
    private Date date;
    private String about;
    private List<Long> roles;
    private List<Long> participants;
}
