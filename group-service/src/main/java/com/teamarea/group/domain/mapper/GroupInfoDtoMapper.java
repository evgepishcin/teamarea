package com.teamarea.group.domain.mapper;

import com.teamarea.group.domain.dto.GroupInfoDto;
import com.teamarea.group.domain.pojo.GroupInfoPojo;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface GroupInfoDtoMapper {
    GroupInfoDto groupInfoPojoToGroupInfoDto(GroupInfoPojo source);
}
