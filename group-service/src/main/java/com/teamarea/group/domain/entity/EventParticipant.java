package com.teamarea.group.domain.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "event_participant")
@Getter
@Setter
@NoArgsConstructor
public class EventParticipant {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "event_participant_id_gen")
    @SequenceGenerator(name = "event_participant_id_gen", sequenceName = "event_participant_id_seq", allocationSize = 1)
    private Long id;
    private Long eventId;
    private Long participantId;

    public EventParticipant(Long eventId, Long participantId) {
        this.eventId = eventId;
        this.participantId = participantId;
    }
}
