package com.teamarea.group.domain.mapper;

import com.teamarea.group.domain.dto.TimetableDto;
import com.teamarea.group.domain.entity.Timetable;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface TimetableDtoMapper {
    TimetableDto timetableToTimetableDto(Timetable source);
}
