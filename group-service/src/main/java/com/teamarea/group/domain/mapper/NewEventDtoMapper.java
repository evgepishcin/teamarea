package com.teamarea.group.domain.mapper;

import com.teamarea.group.domain.dto.NewEventDto;
import com.teamarea.group.domain.entity.Event;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface NewEventDtoMapper {
    Event newEventDtoToEvent(NewEventDto source);
}
