package com.teamarea.group.domain.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "participant")
@Getter
@Setter
@NoArgsConstructor
public class Participant {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "participant_id_gen")
    @SequenceGenerator(name = "participant_id_gen", sequenceName = "participant_id_seq", allocationSize = 1)
    private Long id;
    private Long groupId;
    private Long userId;
    private Boolean active;
    private Boolean moderator;
    private String surname;
    private String name;
    private String patronymic;
    private Long roleId;
    private String invitationCode;
}
