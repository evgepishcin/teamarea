package com.teamarea.group.domain.mapper;

import com.teamarea.group.domain.dto.GroupDto;
import com.teamarea.group.domain.entity.Group;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface GroupDtoMapper {
    GroupDto groupToGroupDto(Group source);
}
