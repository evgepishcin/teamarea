package com.teamarea.group.domain.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "timetable_participant")
@Getter
@Setter
@NoArgsConstructor
public class TimetableParticipant {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "timetable_participant_id_gen")
    @SequenceGenerator(name = "timetable_participant_id_gen", sequenceName = "timetable_participant_id_seq",
            allocationSize = 1)
    private Long id;
    private Long timetableId;
    private Long participantId;

    public TimetableParticipant(Long timetableId, Long participantId) {
        this.timetableId = timetableId;
        this.participantId = participantId;
    }
}
