package com.teamarea.group.domain.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "timetable_role")
@Getter
@Setter
@NoArgsConstructor
public class TimetableRole {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "timetable_role_id_gen")
    @SequenceGenerator(name = "timetable_role_id_gen", sequenceName = "timetable_role_id_seq", allocationSize = 1)
    private Long id;
    private Long timetableId;
    private Long roleId;

    public TimetableRole(Long timetableId, Long roleId) {
        this.timetableId = timetableId;
        this.roleId = roleId;
    }
}
