package com.teamarea.group.domain.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "event_notification_mail")
@Getter
@Setter
@NoArgsConstructor
public class EventNotificationMail {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "event_notification_mail_id_gen")
    @SequenceGenerator(name = "event_notification_mail_id_gen",
            sequenceName = "event_notification_mail_id_seq", allocationSize = 1)
    private Long id;
    private Long participantId;
}
