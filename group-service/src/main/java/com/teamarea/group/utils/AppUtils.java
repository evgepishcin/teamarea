package com.teamarea.group.utils;

import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class AppUtils {

    private static final int MIN_INVITE_CODE_LENGTH = 8;
    private static final int MAX_INVITE_CODE_LENGTH = 13;
    private final Random random = new Random();

    public String generateInviteCode() {
        int len = random.nextInt(MAX_INVITE_CODE_LENGTH - MIN_INVITE_CODE_LENGTH) + MIN_INVITE_CODE_LENGTH;
        RandomString randomString = new RandomString(len);
        return randomString.nextString();
    }
}
