package com.teamarea.group.dao;

import com.teamarea.group.domain.entity.Timetable;
import com.teamarea.group.domain.entity.TimetableParticipant;
import com.teamarea.group.domain.entity.TimetableRole;
import com.teamarea.group.repository.TimetableParticipantRepository;
import com.teamarea.group.repository.TimetableRepository;
import com.teamarea.group.repository.TimetableRoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
@RequiredArgsConstructor
public class TimetableDao {

    private final TimetableRepository timetableRepository;
    private final TimetableRoleRepository timetableRoleRepository;
    private final TimetableParticipantRepository timetableParticipantRepository;

    @Transactional
    public Timetable saveTimetable(Timetable timetable, List<Long> roles, List<Long> participants) {
        Timetable returnedTimetable = timetableRepository.saveAndFlush(timetable);
        timetableRoleRepository.deleteAllByTimetableId(timetable.getId());
        timetableParticipantRepository.deleteAllByTimetableId(timetable.getId());
        if (!roles.isEmpty()) {
            List<TimetableRole> timetableRoles = roles.stream().map(role ->
                    new TimetableRole(returnedTimetable.getId(), role)).toList();
            timetableRoleRepository.saveAll(timetableRoles);
        }
        if (!participants.isEmpty()) {
            List<TimetableParticipant> timetableParticipants = participants.stream().map(participant ->
                    new TimetableParticipant(returnedTimetable.getId(), participant)).toList();
            timetableParticipantRepository.saveAll(timetableParticipants);
        }
        return returnedTimetable;
    }

    @Transactional
    public void deleteTimetable(Long timetableId) {
        timetableRoleRepository.deleteAllByTimetableId(timetableId);
        timetableParticipantRepository.deleteAllByTimetableId(timetableId);
        timetableRepository.deleteById(timetableId);
    }
}
