package com.teamarea.group.dao;

import com.teamarea.group.repository.EventRoleRepository;
import com.teamarea.group.repository.ParticipantRepository;
import com.teamarea.group.repository.RoleRepository;
import com.teamarea.group.repository.TimetableRoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
public class RoleDao {

    private final TimetableRoleRepository timetableRoleRepository;
    private final EventRoleRepository eventRoleRepository;
    private final ParticipantRepository participantRepository;
    private final RoleRepository roleRepository;

    @Transactional
    public void deleteRole(Long roleId) {
        timetableRoleRepository.deleteAllByRoleId(roleId);
        eventRoleRepository.deleteAllByRoleId(roleId);
        participantRepository.removeRole(roleId);
        roleRepository.deleteById(roleId);
    }
}
