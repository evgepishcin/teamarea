package com.teamarea.group.dao;

import com.teamarea.group.domain.entity.Event;
import com.teamarea.group.domain.entity.EventParticipant;
import com.teamarea.group.domain.entity.EventRole;
import com.teamarea.group.repository.EventMailHistoryRepository;
import com.teamarea.group.repository.EventParticipantRepository;
import com.teamarea.group.repository.EventRepository;
import com.teamarea.group.repository.EventRoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
@RequiredArgsConstructor
public class EventDao {

    private final EventRepository eventRepository;
    private final EventRoleRepository eventRoleRepository;
    private final EventParticipantRepository eventParticipantRepository;
    private final EventMailHistoryRepository eventMailHistoryRepository;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Event saveEvent(Event event, List<Long> roles, List<Long> participants) {
        Event returnedEvent = eventRepository.saveAndFlush(event);
        eventRoleRepository.deleteAllByEventId(event.getId());
        eventParticipantRepository.deleteAllByEventId(event.getId());
        if (!roles.isEmpty()) {
            List<EventRole> eventRoles = roles.stream().map(role -> new EventRole(event.getId(), role)).toList();
            eventRoleRepository.saveAll(eventRoles);
        }
        if (!participants.isEmpty()) {
            List<EventParticipant> eventParticipants = participants.stream()
                    .map(participant -> new EventParticipant(event.getId(), participant)).toList();
            eventParticipantRepository.saveAll(eventParticipants);
        }
        return returnedEvent;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void deleteEvent(Long id) {
        eventRoleRepository.deleteAllByEventId(id);
        eventParticipantRepository.deleteAllByEventId(id);
        eventMailHistoryRepository.deleteAllByEventId(id);
        eventRepository.deleteById(id);
    }
}
