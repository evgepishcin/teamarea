package com.teamarea.group.dao;

import com.teamarea.group.domain.entity.Participant;
import com.teamarea.group.repository.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
public class ParticipantDao {

    private final ParticipantRepository participantRepository;
    private final EventNotificationRepository eventNotificationRepository;
    private final EventNotificationMailRepository eventNotificationMailRepository;
    private final EventParticipantRepository eventParticipantRepository;
    private final TimetableParticipantRepository timetableParticipantRepository;

    @Transactional
    public void disableParticipant(Long participantId) {
        Participant participant = participantRepository.findById(participantId).orElse(null);
        if (participant == null) {
            return;
        }
        participant.setUserId(null);
        participant.setInvitationCode(null);
        participant.setActive(false);
        participantRepository.saveAndFlush(participant);
        eventNotificationRepository.deleteAllByParticipantId(participantId);
        eventNotificationMailRepository.deleteAllByParticipantId(participantId);
        eventParticipantRepository.deleteAllByParticipantId(participantId);
        timetableParticipantRepository.deleteAllByParticipantId(participantId);
    }
}
