package com.teamarea.group.dao;

import com.teamarea.group.repository.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
public class GroupDao {
    private final EventMailHistoryRepository eventMailHistoryRepository;
    private final EventNotificationMailRepository eventNotificationMailRepository;
    private final EventNotificationRepository eventNotificationRepository;
    private final EventParticipantRepository eventParticipantRepository;
    private final EventRoleRepository eventRoleRepository;
    private final EventRepository eventRepository;
    private final TimetableParticipantRepository timetableParticipantRepository;
    private final TimetableRoleRepository timetableRoleRepository;
    private final TimetableRepository timetableRepository;
    private final RoleRepository roleRepository;
    private final ParticipantRepository participantRepository;
    private final GroupRepository groupRepository;

    @Transactional
    public void deleteGroup(Long groupId) {
        eventMailHistoryRepository.deleteAllByGroupId(groupId);
        eventNotificationMailRepository.deleteAllByGroupId(groupId);
        eventNotificationRepository.deleteAllByGroupId(groupId);
        eventParticipantRepository.deleteAllByGroupId(groupId);
        eventRoleRepository.deleteAllByGroupId(groupId);
        eventRepository.deleteAllByGroupId(groupId);
        timetableParticipantRepository.deleteAllByGroupId(groupId);
        timetableRoleRepository.deleteAllByGroupId(groupId);
        timetableRepository.deleteAllByGroupId(groupId);
        participantRepository.deleteAllByGroupId(groupId);
        roleRepository.deleteAllByGroupId(groupId);
        groupRepository.deleteAllByGroupId(groupId);
    }
}
