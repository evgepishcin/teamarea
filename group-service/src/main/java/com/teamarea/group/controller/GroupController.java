package com.teamarea.group.controller;

import com.teamarea.group.domain.dto.GroupDto;
import com.teamarea.group.domain.dto.GroupInfoDto;
import com.teamarea.group.domain.dto.NewGroupDto;
import com.teamarea.group.service.GroupService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class GroupController {

    private final GroupService groupService;

    @PostMapping("/group")
    public GroupDto createGroup(@RequestBody NewGroupDto newGroupDto, @RequestHeader(name = "User-Id") Long userId) {
        return groupService.createGroup(newGroupDto, userId);
    }

    @PostMapping("/group/{groupId}/leave")
    public void leaveGroup(@PathVariable("groupId") Long groupId,
                           @RequestHeader("User-Id") Long userId) {
        groupService.leaveGroup(groupId, userId);
    }

    @DeleteMapping("/group/{groupId}")
    public void deleteGroup(@PathVariable("groupId") Long groupId,
                            @RequestHeader("User-Id") Long userId) {
        groupService.deleteGroup(groupId, userId);
    }

    @GetMapping("/group/{groupId}")
    public GroupDto getGroup(@PathVariable("groupId") Long groupId,
                             @RequestHeader("User-Id") Long userId) {
        return groupService.getGroup(groupId, userId);
    }

    @GetMapping("/group/myGroups")
    public List<GroupInfoDto> getUserGroups(@RequestHeader("User-Id") Long userId) {
        return groupService.getUserGroups(userId);
    }

    @PostMapping("group/invite")
    public GroupDto acceptInvite(@RequestParam("code") String code,
                                 @RequestHeader("User-Id") Long userId) {
        return groupService.acceptInvite(userId, code);
    }
}
