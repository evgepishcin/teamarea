package com.teamarea.group.controller;

import com.teamarea.group.domain.dto.NewParticipantDto;
import com.teamarea.group.domain.dto.ParticipantDto;
import com.teamarea.group.service.ParticipantService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class ParticipantController {

    private final ParticipantService participantService;

    @PostMapping("/group/{groupId}/participant")
    public ParticipantDto createParticipant(@PathVariable Long groupId,
                                            @RequestBody NewParticipantDto newParticipantDto,
                                            @RequestHeader("User-Id") Long userId) {
        return participantService.createParticipant(newParticipantDto, groupId, userId);
    }

    @GetMapping("/group/{groupId}/participant/{participantId}")
    public ParticipantDto getParticipant(@PathVariable Long groupId,
                                         @PathVariable Long participantId,
                                         @RequestHeader("User-Id") Long userId) {
        return participantService.getParticipant(participantId, groupId, userId);
    }

    @GetMapping("group/{groupId}/participant/all")
    public List<ParticipantDto> getGroupParticipants(@PathVariable Long groupId,
                                                     @RequestParam(name = "roleId", required = false) Long roleId,
                                                     @RequestParam(name = "sortType", defaultValue = "alphabet")
                                                             String sortType,
                                                     @RequestHeader("User-Id") Long userId) {
        return participantService.getGroupParticipants(groupId, userId, roleId, sortType);
    }

    @PutMapping("/group/{groupId}/participant/{participantId}")
    public ParticipantDto updateParticipant(@PathVariable Long groupId,
                                            @PathVariable Long participantId,
                                            @RequestBody NewParticipantDto newParticipantDto,
                                            @RequestHeader("User-Id") Long userId) {
        return participantService.updateParticipant(newParticipantDto, participantId, groupId, userId);
    }

    @DeleteMapping("/group/{groupId}/participant/{participantId}")
    public ParticipantDto deleteParticipant(@PathVariable Long groupId,
                                            @PathVariable Long participantId,
                                            @RequestHeader("User-Id") Long userId) {
        return participantService.deleteParticipant(participantId, groupId, userId);
    }

    @PostMapping("group/{groupId}/participant/{participantId}/invite")
    public ParticipantDto createInviteCode(@PathVariable Long groupId,
                                           @PathVariable Long participantId,
                                           @RequestHeader("User-Id") Long userId) {
        return participantService.createInviteCode(participantId, groupId, userId);
    }

    @DeleteMapping("group/{groupId}/participant/{participantId}/invite")
    public ParticipantDto deleteInviteCode(@PathVariable Long groupId,
                                           @PathVariable Long participantId,
                                           @RequestHeader("User-Id") Long userId) {
        return participantService.deleteInviteCode(participantId, groupId, userId);
    }
}
