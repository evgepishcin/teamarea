package com.teamarea.group.controller;

import com.teamarea.group.domain.dto.NewTimetableDto;
import com.teamarea.group.domain.dto.TimetableDto;
import com.teamarea.group.domain.dto.TimetableInfoDto;
import com.teamarea.group.service.TimetableService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class TimetableController {

    private final TimetableService timetableService;

    @PostMapping("group/{groupId}/timetable")
    public TimetableDto createTimetable(@PathVariable("groupId") Long groupId,
                                        @RequestBody NewTimetableDto newTimetableDto,
                                        @RequestHeader("User-Id") Long userId) {
        return timetableService.createTimetable(newTimetableDto, groupId, userId);
    }

    @GetMapping("group/{groupId}/timetable/{timetableId}")
    public TimetableInfoDto updateTimetable(@PathVariable("groupId") Long groupId,
                                            @PathVariable("timetableId") Long timetableId,
                                            @RequestHeader("User-Id") Long userId) {
        return timetableService.getTimetable(timetableId, groupId, userId);
    }

    @PutMapping("group/{groupId}/timetable/{timetableId}")
    public TimetableDto updateTimetable(@PathVariable("groupId") Long groupId,
                                        @PathVariable("timetableId") Long timetableId,
                                        @RequestBody NewTimetableDto newTimetableDto,
                                        @RequestHeader("User-Id") Long userId) {
        return timetableService.updateTimetable(newTimetableDto, timetableId, groupId, userId);
    }

    @GetMapping("group/{groupId}/timetable/all")
    public List<TimetableDto> getGroupTimetables(@PathVariable("groupId") Long groupId,
                                                 @RequestHeader("User-Id") Long userId) {
        return timetableService.getGroupTimetables(groupId, userId);
    }

    @GetMapping("group/{groupId}/timetable/my")
    public List<TimetableDto> getParticipantTimetables(@PathVariable("groupId") Long groupId,
                                                       @RequestHeader("User-Id") Long userId) {
        return timetableService.getParticipantTimetables(groupId, userId);
    }

    @DeleteMapping("group/{groupId}/timetable/{timetableId}")
    public void deleteTimetable(@PathVariable("groupId") Long groupId,
                                @PathVariable("timetableId") Long timetableId,
                                @RequestHeader("User-Id") Long userId) {
        timetableService.deleteTimetable(timetableId, groupId, userId);
    }
}
