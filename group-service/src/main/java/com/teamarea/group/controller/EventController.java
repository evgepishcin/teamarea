package com.teamarea.group.controller;

import com.teamarea.group.domain.dto.EventDto;
import com.teamarea.group.domain.dto.EventInfoDto;
import com.teamarea.group.domain.dto.NewEventDto;
import com.teamarea.group.domain.dto.NotificationStatusDto;
import com.teamarea.group.service.EventService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class EventController {

    private final EventService eventService;

    @PostMapping("group/{groupId}/event")
    public EventDto createEvent(@PathVariable("groupId") Long groupId,
                                @RequestBody NewEventDto newEventDto,
                                @RequestHeader("User-Id") Long userId) {
        return eventService.createEvent(newEventDto, groupId, userId);
    }

    @GetMapping("group/{groupId}/event/{eventId}")
    public EventInfoDto getEvent(@PathVariable("groupId") Long groupId,
                                 @PathVariable("eventId") Long eventId,
                                 @RequestHeader("User-Id") Long userId) {
        return eventService.getEvent(eventId, groupId, userId);
    }

    @GetMapping("group/{groupId}/event/all")
    public List<EventDto> getGroupEvents(@PathVariable("groupId") Long groupId,
                                         @RequestHeader("User-Id") Long userId) {
        return eventService.getGroupEvents(groupId, userId);
    }

    @GetMapping("group/{groupId}/event/my")
    public List<EventDto> getParticipantEvents(@PathVariable("groupId") Long groupId,
                                               @RequestHeader("User-Id") Long userId) {
        return eventService.getParticipantEvents(groupId, userId);
    }

    @PutMapping("group/{groupId}/event/{eventId}")
    public EventDto updateEvent(@PathVariable("groupId") Long groupId,
                                @PathVariable("eventId") Long eventId,
                                @RequestBody NewEventDto newEventDto,
                                @RequestHeader("User-Id") Long userId) {
        return eventService.updateEvent(newEventDto, eventId, groupId, userId);
    }

    @DeleteMapping("group/{groupId}/event/{eventId}")
    public void deleteEvent(@PathVariable("groupId") Long groupId,
                            @PathVariable("eventId") Long eventId,
                            @RequestHeader("User-Id") Long userId) {
        eventService.deleteEvent(eventId, groupId, userId);
    }

    @PostMapping("group/{groupId}/event/notification")
    public NotificationStatusDto notificationSwitch(@RequestParam("enabled") Boolean enabled,
                                                    @PathVariable("groupId") Long groupId,
                                                    @RequestHeader("User-Id") Long userId) {
        return eventService.notificationSwitch(groupId, userId, enabled);
    }

    @PostMapping("group/{groupId}/event/notification-mail")
    public NotificationStatusDto notificationMailSwitch(@RequestParam("enabled") Boolean enabled,
                                                        @PathVariable("groupId") Long groupId,
                                                        @RequestHeader("User-Id") Long userId) {
        return eventService.notificationMailSwitch(groupId, userId, enabled);
    }

    @GetMapping("group/{groupId}/event/notification")
    public NotificationStatusDto getNotificationStatus(@PathVariable("groupId") Long groupId,
                                                       @RequestHeader("User-Id") Long userId) {
        return eventService.getNotificationStatus(groupId, userId);
    }

    @GetMapping("group/{groupId}/event/notification-mail")
    public NotificationStatusDto getNotificationMailStatus(@PathVariable("groupId") Long groupId,
                                                           @RequestHeader("User-Id") Long userId) {
        return eventService.getNotificationMailStatus(groupId, userId);
    }
}
