package com.teamarea.group.controller;

import com.teamarea.group.domain.dto.NewRoleDto;
import com.teamarea.group.domain.dto.RoleDto;
import com.teamarea.group.service.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class RoleController {

    private final RoleService roleService;

    @PostMapping("/group/{groupId}/role")
    public RoleDto createRole(@PathVariable Long groupId,
                              @RequestBody NewRoleDto newRoleDto,
                              @RequestHeader("User-Id") Long userId) {
        return roleService.createRole(newRoleDto, groupId, userId);
    }

    @PutMapping("/group/{groupId}/role/{roleId}")
    public RoleDto updateRole(@PathVariable Long groupId,
                              @PathVariable Long roleId,
                              @RequestBody NewRoleDto newRoleDto,
                              @RequestHeader("User-Id") Long userId) {
        return roleService.updateRole(newRoleDto, roleId, groupId, userId);
    }

    @GetMapping("/group/{groupId}/role/{roleId}")
    public RoleDto getRole(@PathVariable Long groupId,
                           @PathVariable Long roleId,
                           @RequestHeader("User-Id") Long userId) {
        return roleService.getRole(roleId, groupId, userId);
    }

    @DeleteMapping("/group/{groupId}/role/{roleId}")
    public void deleteRole(@PathVariable Long groupId,
                           @PathVariable Long roleId,
                           @RequestHeader("User-Id") Long userId) {
        roleService.deleteRole(roleId, groupId, userId);
    }

    @GetMapping("/group/{groupId}/role/all")
    public List<RoleDto> getAllGroupRoles(@PathVariable Long groupId,
                                          @RequestHeader("User-Id") Long userId) {
        return roleService.getAllGroupRoles(groupId, userId);
    }
}
