package com.teamarea.group.service.datacheck.impl;

import com.teamarea.group.domain.dto.NewEventDto;
import com.teamarea.group.service.datacheck.DataCheckException;
import com.teamarea.group.service.datacheck.DataChecker;
import org.springframework.stereotype.Component;

@Component
public class NewEventDtoDataChecker implements DataChecker<NewEventDto> {

    @Override
    public void check(NewEventDto newEventDto) throws DataCheckException {
        if (newEventDto.getName() == null || newEventDto.getName().length() == 0) {
            throw new DataCheckException("Event name can't be empty");
        }
        if (newEventDto.getDate() == null) {
            throw new DataCheckException("Event date can't be empty");
        }
        if (newEventDto.getRoles() == null) {
            throw new DataCheckException("roles can be empty, but not null");
        } else {
            newEventDto.setRoles(newEventDto.getRoles().stream().distinct().toList());
        }
        if (newEventDto.getParticipants() == null) {
            throw new DataCheckException("participants can be empty, but not null");
        } else {
            newEventDto.setParticipants(newEventDto.getParticipants().stream().distinct().toList());
        }
    }

    @Override
    public Class<?> getDataClass() {
        return NewEventDto.class;
    }
}
