package com.teamarea.group.service;

import com.teamarea.group.domain.entity.*;
import com.teamarea.group.repository.*;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Утилитный сервис для проверок бизнеслогики и прочего
 */
@Service
@RequiredArgsConstructor
public class GroupUtilsService {

    private final ParticipantRepository participantRepository;
    private final RoleRepository roleRepository;
    private final EventRepository eventRepository;
    private final EventRoleRepository eventRoleRepository;
    private final EventParticipantRepository eventParticipantRepository;
    private final TimetableRepository timetableRepository;
    private final TimetableRoleRepository timetableRoleRepository;
    private final TimetableParticipantRepository timetableParticipantRepository;

    /**
     * Получение участника группы по номеру группы и идентификатору пользователя
     * с проверкой на права модетаора и статус активного профиля
     *
     * @param userId         идентификатор пользователя
     * @param groupId        идентификатор группы
     * @param checkModerator проверить права модератора
     * @param checkActive    проверить статус активного участника группы
     * @return усастник группы, найденный по полям userId и groupId
     * @throws GroupUtilsException данное исключение выбрасывается, если не прошла хоть одна проверка
     */
    public Participant checkAndReturnParticipant(long userId, long groupId,
                                                 boolean checkModerator, boolean checkActive)
            throws GroupUtilsException {
        Optional<Participant> optionalParticipant = participantRepository.findByUserIdAndGroupId(userId, groupId);
        Participant participant = optionalParticipant.orElseThrow(() ->
                new GroupUtilsException("This user is not " +
                        "a member of the specified group!", HttpStatus.FORBIDDEN));
        checkModeratorAndActive(participant, checkModerator, checkActive);
        return participant;
    }

    /**
     * Проверка принадлежности участника группы указанной группе и получение данного участника
     *
     * @param groupId        идентификатор пользователя
     * @param participantId  идентификатор участника группы
     * @param checkModerator проверить права модератора
     * @param checkActive    проверить статус активного участника группы
     * @return участник группы
     * @throws GroupUtilsException данное исключение выбрасывается, если участник группы не состоит в данной группе
     */
    public Participant checkParticipantInGroupAndReturn(long groupId, long participantId,
                                                        boolean checkModerator, boolean checkActive)
            throws GroupUtilsException {
        Optional<Participant> participantOptional = participantRepository.findById(participantId);
        Participant participant = participantOptional.orElseThrow(() ->
                new GroupUtilsException("Participant does not belong to this group!"));
        if (!participant.getGroupId().equals(groupId)) {
            throw new GroupUtilsException("Participant does not belong to this group!");
        }
        checkModeratorAndActive(participant, checkModerator, checkActive);
        return participant;
    }

    /**
     * Проверка принадлежности роли указанной группе и получение данной роли
     *
     * @param groupId идентификатор пользователя
     * @param roleId  идентификатор роли
     * @return роль
     * @throws GroupUtilsException данное исключение выбрасывается, если роль не принадлежит указанной группе
     */
    public Role checkRoleInGroupAndReturn(long groupId, long roleId) throws GroupUtilsException {
        Optional<Role> roleOptional = roleRepository.findById(roleId);
        Role role = roleOptional.orElseThrow(() -> new GroupUtilsException("Role does not belong to this group!"));
        if (!role.getGroupId().equals(groupId)) {
            throw new GroupUtilsException("Role does not belong to this group!");
        }
        return role;
    }

    /**
     * Проверка возможности модератору покинуть группы
     * <p>
     * Если пользователь хочет покинуть группу, или снять с себя прова модератора,
     * то необходимо сначала убедиться, что он не был последним модератором группы,
     * иначе будет невозможно любое дальнейшее редактирование группы и ее участников
     *
     * @param groupId идентификатор группы
     * @throws GroupUtilsException данное исключение будет выброшено, если в группе остался только один модератор
     */
    public void checkModeratorLeave(long groupId) throws GroupUtilsException {
        List<Participant> participants = participantRepository.findAllByGroupIdAndModeratorIsTrueAndActiveIsTrue(groupId);
        if (participants.size() <= 1) {
            throw new GroupUtilsException("Can't change, because group stay without any moderator inside");
        }
    }

    public List<Role> checkRolesInGroupAndReturn(List<Long> ids, Long groupId) throws GroupUtilsException {
        List<Role> roles = roleRepository.findAllByIdInAndGroupIdEquals(ids, groupId);
        if (ids.size() != roles.size()) {
            throw new GroupUtilsException("Some roles do not belong to the specified group");
        }
        return roles;
    }

    public List<Participant> checkParticipantsInGroupAndReturn(List<Long> ids, Long groupId) throws GroupUtilsException {
        List<Participant> participants = participantRepository.findAllByIdInAndGroupIdEqualsAndActive(ids, groupId);
        if (ids.size() != participants.size()) {
            throw new GroupUtilsException("Some participants do not belong to the specified group");
        }
        return participants;
    }

    public Event checkEventInGroupAndReturn(Long eventId, Long groupId) throws GroupUtilsException {
        Event event = eventRepository.findById(eventId).orElseThrow(() ->
                new GroupUtilsException("Event does not belong to this group"));
        if (!Objects.equals(event.getGroupId(), groupId)) {
            throw new GroupUtilsException("Event does not belong to this group");
        }
        return event;
    }

    public Timetable checkTimetableInGroupAndReturn(Long timetableId, Long groupId) throws GroupUtilsException {
        Timetable timetable = timetableRepository.findById(timetableId).orElseThrow(() ->
                new GroupUtilsException("Timetable does not belong to this group"));
        if (!Objects.equals(timetable.getGroupId(), groupId)) {
            throw new GroupUtilsException("Timetable does not belong to this group");
        }
        return timetable;
    }

    public void checkParticipantAssociatedWithEvent(Long participantId, Long eventId) {
        Participant participant = participantRepository.findById(participantId).orElseThrow(() ->
                new GroupUtilsException("Participant with this id does not exist"));
        if (participant.getRoleId() != null) {
            Optional<EventRole> eventRole = eventRoleRepository.findByRoleIdAndEventId(participant.getRoleId(), eventId);
            if (eventRole.isPresent()) {
                return;
            }
        }
        Optional<EventParticipant> eventParticipant =
                eventParticipantRepository.findByParticipantIdAndEventId(participantId, eventId);
        if (eventParticipant.isEmpty()) {
            throw new GroupUtilsException("The participant is not associated with this event");
        }
    }

    public void checkParticipantAssociatedWithTimetable(Long participantId, Long timetableId) {
        Participant participant = participantRepository.findById(participantId).orElseThrow(() ->
                new GroupUtilsException("Participant with this id does not exist"));
        if (participant.getRoleId() != null) {
            Optional<TimetableRole> timetableRole = timetableRoleRepository
                    .findByRoleIdAndTimetableId(participant.getRoleId(), timetableId);
            if (timetableRole.isPresent()) {
                return;
            }
        }
        Optional<TimetableParticipant> timetableParticipant =
                timetableParticipantRepository.findByParticipantIdAndTimetableId(participantId, timetableId);
        if (timetableParticipant.isEmpty()) {
            throw new GroupUtilsException("The participant is not associated with this timetable");
        }
    }

    private void checkModeratorAndActive(Participant participant, boolean checkModerator, boolean checkActive) {
        if (checkModerator && Boolean.FALSE.equals(participant.getModerator())) {
            throw new GroupUtilsException("This participant does not have " +
                    "moderator rights for this action", HttpStatus.FORBIDDEN);
        }
        if (checkActive && Boolean.FALSE.equals(participant.getActive())) {
            throw new GroupUtilsException("This participant was disabled!", HttpStatus.FORBIDDEN);
        }
    }
}
