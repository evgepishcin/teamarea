package com.teamarea.group.service.datacheck.impl;

import com.teamarea.group.domain.dto.NewTimetableDto;
import com.teamarea.group.service.datacheck.DataCheckException;
import com.teamarea.group.service.datacheck.DataChecker;
import org.springframework.stereotype.Component;

@Component
public class NewTimetableDtoDataChecker implements DataChecker<NewTimetableDto> {

    private static final String ERROR_TEXT = "Incorrect data filling";

    @Override
    public void check(NewTimetableDto newTimetableDto) throws DataCheckException {
        if (newTimetableDto.getName() == null || newTimetableDto.getName().length() == 0) {
            throw new DataCheckException("Event name can't be empty");
        }
        if (newTimetableDto.getDayOfWeek() != null &&
                (newTimetableDto.getDayOfWeek() < 1 || newTimetableDto.getDayOfWeek() > 7)) {
            throw new DataCheckException("day of week must be in range 1-7");

        }
        if (newTimetableDto.getMinutes() < 0 || newTimetableDto.getMinutes() > 59) {
            throw new DataCheckException("minutes must be in range 0-59");
        }
        if (newTimetableDto.getHours() < 0 || newTimetableDto.getHours() > 23) {
            throw new DataCheckException("hours must be in range 0-23");
        }
        if (newTimetableDto.getWeekParity() != null &&
                (newTimetableDto.getWeekParity() < 1 || newTimetableDto.getWeekParity() > 52)) {
            throw new DataCheckException("week parity must be in range 1-52");
        }
        if (newTimetableDto.getMonthWeekNum() != null &&
                (newTimetableDto.getMonthWeekNum() < 1 || newTimetableDto.getMonthWeekNum() > 5)) {
            throw new DataCheckException("month week num must be in range 1-5");
        }
        if (newTimetableDto.getWeekParityDay() != null &&
                (newTimetableDto.getWeekParityDay() < 1 || newTimetableDto.getWeekParityDay() > 52)) {
            throw new DataCheckException("week parity day must be in range 1-52");
        }
        if (newTimetableDto.getDayParity() != null &&
                (newTimetableDto.getDayParity() < 1 || newTimetableDto.getDayParity() > 182)) {
            throw new DataCheckException("day parity must be in range 1-182");
        }
        if (newTimetableDto.getRoles() == null) {
            throw new DataCheckException("roles can be empty, but not null");
        } else {
            newTimetableDto.setRoles(newTimetableDto.getRoles().stream().distinct().toList());
        }
        if (newTimetableDto.getParticipants() == null) {
            throw new DataCheckException("participants can be empty, but not null");
        } else {
            newTimetableDto.setParticipants(newTimetableDto.getParticipants().stream().distinct().toList());
        }
        secondCheck(newTimetableDto);
    }

    private void secondCheck(NewTimetableDto newTimetableDto) {
        if (newTimetableDto.getDayOfWeek() == null) {
            if (newTimetableDto.getDayParity() == null || newTimetableDto.getDayParitySet() == null
                    || newTimetableDto.getIncludeWeekends() == null || newTimetableDto.getMonthWeekNum() != null
                    || newTimetableDto.getWeekParityDay() != null || newTimetableDto.getWeekParity() != null
                    || newTimetableDto.getWeekParityDaySet() != null) {
                throw new DataCheckException(ERROR_TEXT);
            }
        } else {
            if (newTimetableDto.getWeekParity() != null) {
                if (newTimetableDto.getMonthWeekNum() != null || newTimetableDto.getWeekParityDay() != null ||
                        newTimetableDto.getWeekParityDaySet() != null || newTimetableDto.getDayParity() != null ||
                        newTimetableDto.getDayParitySet() != null || newTimetableDto.getIncludeWeekends() != null) {
                    throw new DataCheckException(ERROR_TEXT);
                }
            } else if (newTimetableDto.getMonthWeekNum() != null) {
                if (newTimetableDto.getWeekParity() != null || newTimetableDto.getWeekParityDay() != null ||
                        newTimetableDto.getWeekParityDaySet() != null || newTimetableDto.getDayParity() != null ||
                        newTimetableDto.getDayParitySet() != null || newTimetableDto.getIncludeWeekends() != null) {
                    throw new DataCheckException(ERROR_TEXT);
                }
            } else if (newTimetableDto.getWeekParityDay() != null) {
                if (newTimetableDto.getWeekParityDaySet() == null || newTimetableDto.getWeekParity() != null ||
                        newTimetableDto.getMonthWeekNum() != null || newTimetableDto.getDayParity() != null ||
                        newTimetableDto.getDayParitySet() != null || newTimetableDto.getIncludeWeekends() != null) {
                    throw new DataCheckException(ERROR_TEXT);
                }
            } else {
                if (newTimetableDto.getWeekParity() != null || newTimetableDto.getMonthWeekNum() != null ||
                        newTimetableDto.getWeekParityDay() != null || newTimetableDto.getWeekParityDaySet() != null ||
                        newTimetableDto.getDayParity() != null || newTimetableDto.getDayParitySet() != null ||
                        newTimetableDto.getIncludeWeekends() != null) {
                    throw new DataCheckException(ERROR_TEXT);
                }
            }
        }
    }

    @Override
    public Class<?> getDataClass() {
        return NewTimetableDto.class;
    }
}
