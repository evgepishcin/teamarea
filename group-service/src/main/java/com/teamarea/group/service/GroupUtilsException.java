package com.teamarea.group.service;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class GroupUtilsException extends RuntimeException {
    private final HttpStatus httpStatus;

    public GroupUtilsException(String message) {
        super(message);
        httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
    }

    public GroupUtilsException(String message, HttpStatus httpStatus) {
        super(message);
        this.httpStatus = httpStatus;
    }
}
