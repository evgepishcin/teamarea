package com.teamarea.group.service;

import com.teamarea.group.dao.TimetableDao;
import com.teamarea.group.domain.dto.NewTimetableDto;
import com.teamarea.group.domain.dto.TimetableDto;
import com.teamarea.group.domain.dto.TimetableInfoDto;
import com.teamarea.group.domain.entity.Participant;
import com.teamarea.group.domain.entity.Timetable;
import com.teamarea.group.domain.mapper.NewTimetableDtoMapper;
import com.teamarea.group.domain.mapper.TimetableDtoMapper;
import com.teamarea.group.domain.mapper.TimetableInfoDtoMapper;
import com.teamarea.group.repository.TimetableParticipantRepository;
import com.teamarea.group.repository.TimetableRepository;
import com.teamarea.group.repository.TimetableRoleRepository;
import com.teamarea.group.service.datacheck.DataCheckerService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TimetableService {

    private final GroupUtilsService groupUtilsService;
    private final DataCheckerService dataCheckerService;
    private final NewTimetableDtoMapper newTimetableDtoMapper;
    private final TimetableDao timetableDao;
    private final TimetableDtoMapper timetableDtoMapper;
    private final TimetableInfoDtoMapper timetableInfoDtoMapper;
    private final TimetableRepository timetableRepository;
    private final TimetableRoleRepository timetableRoleRepository;
    private final TimetableParticipantRepository timetableParticipantRepository;

    @Transactional
    public TimetableDto createTimetable(NewTimetableDto newTimetableDto, Long groupId, Long userId) {
        dataCheckerService.check(newTimetableDto);
        groupUtilsService.checkAndReturnParticipant(userId, groupId, true, true);
        groupUtilsService.checkRolesInGroupAndReturn(newTimetableDto.getRoles(), groupId);
        groupUtilsService.checkParticipantsInGroupAndReturn(newTimetableDto.getParticipants(), groupId);
        Timetable timetable = newTimetableDtoMapper.newTimeTableDtoToTimetable(newTimetableDto);
        timetable.setGroupId(groupId);
        timetable = timetableDao.saveTimetable(timetable, newTimetableDto.getRoles(), newTimetableDto.getParticipants());
        return timetableDtoMapper.timetableToTimetableDto(timetable);
    }

    @Transactional
    public TimetableDto updateTimetable(NewTimetableDto newTimetableDto, Long timetableId, Long groupId, Long userId) {
        dataCheckerService.check(newTimetableDto);
        groupUtilsService.checkAndReturnParticipant(userId, groupId, true, true);
        groupUtilsService.checkRolesInGroupAndReturn(newTimetableDto.getRoles(), groupId);
        groupUtilsService.checkParticipantsInGroupAndReturn(newTimetableDto.getParticipants(), groupId);
        groupUtilsService.checkTimetableInGroupAndReturn(timetableId, groupId);
        Timetable timetable = newTimetableDtoMapper.newTimeTableDtoToTimetable(newTimetableDto);
        timetable.setId(timetableId);
        timetable.setGroupId(groupId);
        timetable = timetableDao.saveTimetable(timetable, newTimetableDto.getRoles(), newTimetableDto.getParticipants());
        return timetableDtoMapper.timetableToTimetableDto(timetable);
    }

    @Transactional
    public void deleteTimetable(Long timetableId, Long groupId, Long userId) {
        groupUtilsService.checkAndReturnParticipant(userId, groupId, true, true);
        groupUtilsService.checkTimetableInGroupAndReturn(timetableId, groupId);
        timetableDao.deleteTimetable(timetableId);
    }

    @Transactional(readOnly = true)
    public TimetableInfoDto getTimetable(Long timetableId, Long groupId, Long userId) {
        Participant participant = groupUtilsService.checkParticipantInGroupAndReturn(groupId, userId, false, true);
        Timetable timetable = groupUtilsService.checkTimetableInGroupAndReturn(timetableId, groupId);
        if (Boolean.FALSE.equals(participant.getModerator())) {
            groupUtilsService.checkParticipantAssociatedWithTimetable(participant.getId(), timetableId);
        }
        TimetableInfoDto timetableInfoDto = timetableInfoDtoMapper.timetableToTimetableInfoDto(timetable);
        timetableInfoDto.setRoles(timetableRoleRepository.selectAllRolesIdsByTimetableId(timetableId));
        timetableInfoDto.setParticipants(timetableParticipantRepository.selectAllParticipantsIdsByTimetableId(timetableId));
        return timetableInfoDto;
    }

    @Transactional(readOnly = true)
    public List<TimetableDto> getGroupTimetables(Long groupId, Long userId) {
        groupUtilsService.checkAndReturnParticipant(userId, groupId, true, true);
        List<Timetable> timetables = timetableRepository.findAllByGroupId(groupId);
        return timetables.stream().map(timetableDtoMapper::timetableToTimetableDto).toList();
    }

    public List<TimetableDto> getParticipantTimetables(Long groupId, Long userId) {
        Participant participant = groupUtilsService.checkAndReturnParticipant(userId, groupId, false, true);
        List<Timetable> timetables = new ArrayList<>();
        if (participant.getRoleId() != null) {
            timetables.addAll(timetableRepository.findAllByRoleId(participant.getRoleId()));
        }
        timetables.addAll(timetableRepository.findAllByParticipantId(participant.getId()));
        return timetables.stream().map(timetableDtoMapper::timetableToTimetableDto).toList();
    }
}
