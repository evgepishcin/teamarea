package com.teamarea.group.service.messages;

import java.util.List;

/**
 * Сервис отправки сообщеник в брокер сообщений
 */
public interface MessagesService {

    /**
     * Отправка сообщения в брокер сообщений
     *
     * @param channel     канал
     * @param messageType тип сообщения
     * @param operation   тип операции (create, read, update, delete)
     * @param payload     отправляемое сообщение
     */
    void sendMessage(String channel, String messageType, String operation, Object payload);

    /**
     * Отправка сообщения в брокер сообщений для указаных пользователей системы
     *
     * @param channel     канал
     * @param messageType тип сообщения
     * @param operation   тип операции (create, read, update, delete)
     * @param payload     отправляемое сообщение
     * @param usersIds    получатели сообщения
     */
    void sendMessage(String channel, String messageType, String operation, Object payload, List<Long> usersIds);
}
