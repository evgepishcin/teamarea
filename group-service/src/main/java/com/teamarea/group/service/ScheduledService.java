package com.teamarea.group.service;

import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class ScheduledService {

    private final EventService eventService;

    @Scheduled(fixedDelay = 300000)
    public void sendEventReminder() {
        eventService.remindAboutEvent();
    }
}
