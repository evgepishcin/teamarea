package com.teamarea.group.service.datacheck.impl;

import com.teamarea.group.domain.dto.NewGroupDto;
import com.teamarea.group.service.datacheck.DataCheckException;
import com.teamarea.group.service.datacheck.DataChecker;
import org.springframework.stereotype.Component;

@Component
public class NewGroupDtoDataChecker implements DataChecker<NewGroupDto> {

    @Override
    public void check(NewGroupDto newGroupDto) throws DataCheckException {
        if (newGroupDto.getName() == null || newGroupDto.getName().length() == 0) {
            throw new DataCheckException("Group name can't be empty!");
        }
    }

    @Override
    public Class<?> getDataClass() {
        return NewGroupDto.class;
    }
}
