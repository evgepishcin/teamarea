package com.teamarea.group.service;

import com.teamarea.common.data.api.Operation;
import com.teamarea.group.api.dto.GroupMessageTypeApi;
import com.teamarea.group.dao.EventDao;
import com.teamarea.group.domain.dto.EventDto;
import com.teamarea.group.domain.dto.EventInfoDto;
import com.teamarea.group.domain.dto.NewEventDto;
import com.teamarea.group.domain.dto.NotificationStatusDto;
import com.teamarea.group.domain.entity.*;
import com.teamarea.group.domain.mapper.EventDtoMapper;
import com.teamarea.group.domain.mapper.EventInfoDtoMapper;
import com.teamarea.group.domain.mapper.MailEventDtoMapper;
import com.teamarea.group.domain.mapper.NewEventDtoMapper;
import com.teamarea.group.repository.*;
import com.teamarea.group.service.datacheck.DataCheckerService;
import com.teamarea.group.service.messages.Channels;
import com.teamarea.group.service.messages.MessagesService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class EventService {

    private final DataCheckerService dataCheckerService;
    private final GroupUtilsService groupUtilsService;
    private final NewEventDtoMapper newEventDtoMapper;
    private final GroupRepository groupRepository;
    private final EventRepository eventRepository;
    private final EventRoleRepository eventRoleRepository;
    private final EventMailHistoryRepository eventMailHistoryRepository;
    private final EventParticipantRepository eventParticipantRepository;
    private final MessagesService messagesService;
    private final EventDtoMapper eventDtoMapper;
    private final EventNotificationRepository eventNotificationRepository;
    private final EventNotificationMailRepository eventNotificationMailRepository;
    private final EventDao eventDao;
    private final MailEventDtoMapper mailEventDtoMapper;
    private final EventInfoDtoMapper eventInfoDtoMapper;

    @Transactional
    public EventDto createEvent(NewEventDto newEventDto, Long groupId, Long userId) {
        dataCheckerService.check(newEventDto);
        groupUtilsService.checkAndReturnParticipant(userId, groupId, false, true);
        groupUtilsService.checkRolesInGroupAndReturn(newEventDto.getRoles(), groupId);
        groupUtilsService.checkParticipantsInGroupAndReturn(newEventDto.getParticipants(), groupId);
        Event event = newEventDtoMapper.newEventDtoToEvent(newEventDto);
        event.setGroupId(groupId);
        event = eventDao.saveEvent(event, newEventDto.getRoles(), newEventDto.getParticipants());
        EventDto eventDto = eventDtoMapper.eventToEventDto(event);
        notifyUsersAboutEvent(event, userId, Operation.CREATE, groupId);
        return eventDto;
    }

    @Transactional(readOnly = true)
    public EventInfoDto getEvent(Long eventId, Long groupId, Long userId) {
        Participant participant = groupUtilsService.checkAndReturnParticipant(userId, groupId, false, true);
        Event event = groupUtilsService.checkEventInGroupAndReturn(eventId, groupId);
        if (Boolean.FALSE.equals(participant.getModerator())) {
            groupUtilsService.checkParticipantAssociatedWithEvent(participant.getId(), event.getId());
        }
        EventInfoDto eventInfoDto = eventInfoDtoMapper.eventToEventInfoDto(event);
        eventInfoDto.setRoles(eventRoleRepository.selectAllRolesIdsByEventId(eventId));
        eventInfoDto.setParticipants(eventParticipantRepository.selectAllParticipantsIdsByEventId(eventId));
        return eventInfoDto;
    }

    @Transactional(readOnly = true)
    public List<EventDto> getGroupEvents(Long groupId, Long userId) {
        groupUtilsService.checkAndReturnParticipant(userId, groupId, true, false);
        List<Event> events = eventRepository.findAllByGroupId(groupId);
        return events.stream().map(eventDtoMapper::eventToEventDto).toList();
    }

    @Transactional(readOnly = true)
    public List<EventDto> getParticipantEvents(Long groupId, Long userId) {
        Participant participant = groupUtilsService.checkAndReturnParticipant(userId, groupId, false, true);
        List<Event> participantEvents = new ArrayList<>();
        if (participant.getRoleId() != null) {
            participantEvents.addAll(eventRepository.findAllByRoleId(participant.getRoleId()));
        }
        participantEvents.addAll(eventRepository.findAllByParticipantId(participant.getId()));
        return participantEvents.stream().map(eventDtoMapper::eventToEventDto).toList();
    }

    @Transactional
    public EventDto updateEvent(NewEventDto newEventDto, Long eventId, Long groupId, Long userId) {
        dataCheckerService.check(newEventDto);
        groupUtilsService.checkAndReturnParticipant(userId, groupId, false, true);
        groupUtilsService.checkRolesInGroupAndReturn(newEventDto.getRoles(), groupId);
        groupUtilsService.checkParticipantsInGroupAndReturn(newEventDto.getParticipants(), groupId);
        groupUtilsService.checkEventInGroupAndReturn(eventId, groupId);
        Event event = newEventDtoMapper.newEventDtoToEvent(newEventDto);
        event.setId(eventId);
        event.setGroupId(groupId);
        event = eventDao.saveEvent(event, newEventDto.getRoles(), newEventDto.getParticipants());
        EventDto eventDto = eventDtoMapper.eventToEventDto(event);
        notifyUsersAboutEvent(event, userId, Operation.UPDATE, groupId);
        return eventDto;
    }

    @Transactional
    public void deleteEvent(Long eventId, Long groupId, Long userId) {
        groupUtilsService.checkAndReturnParticipant(userId, groupId, false, true);
        Event event = groupUtilsService.checkEventInGroupAndReturn(eventId, groupId);
        notifyUsersAboutEvent(event, userId, Operation.DELETE, groupId);
        eventDao.deleteEvent(eventId);
    }

    @Transactional
    public NotificationStatusDto notificationSwitch(Long groupId, Long userId, Boolean enabled) {
        Participant participant = groupUtilsService.checkAndReturnParticipant(userId, groupId, false, true);
        eventNotificationRepository.deleteAllByParticipantId(participant.getId());
        if (Boolean.FALSE.equals(enabled)) {
            EventNotification eventNotification = new EventNotification();
            eventNotification.setParticipantId(participant.getId());
            eventNotificationRepository.save(eventNotification);
            return new NotificationStatusDto(false);
        }
        return new NotificationStatusDto(true);
    }

    @Transactional
    public NotificationStatusDto notificationMailSwitch(Long groupId, Long userId, Boolean enabled) {
        Participant participant = groupUtilsService.checkAndReturnParticipant(userId, groupId, false, true);
        eventNotificationMailRepository.deleteAllByParticipantId(participant.getId());
        if (Boolean.FALSE.equals(enabled)) {
            EventNotificationMail eventNotificationMail = new EventNotificationMail();
            eventNotificationMail.setParticipantId(participant.getId());
            eventNotificationMailRepository.save(eventNotificationMail);
            return new NotificationStatusDto(false);
        }
        return new NotificationStatusDto(true);
    }

    @Transactional(readOnly = true)
    public NotificationStatusDto getNotificationStatus(Long groupId, Long userId) {
        Participant participant = groupUtilsService.checkAndReturnParticipant(userId, groupId, false, true);
        if (eventNotificationRepository.findByParticipantId(participant.getId()).isPresent()) {
            return new NotificationStatusDto(false);
        }
        return new NotificationStatusDto(true);
    }

    @Transactional(readOnly = true)
    public NotificationStatusDto getNotificationMailStatus(Long groupId, Long userId) {
        Participant participant = groupUtilsService.checkAndReturnParticipant(userId, groupId, false, true);
        if (eventNotificationMailRepository.findByParticipantId(participant.getId()).isPresent()) {
            return new NotificationStatusDto(false);
        }
        return new NotificationStatusDto(true);
    }

    @Transactional
    public void remindAboutEvent() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.HOUR_OF_DAY, 1);
        Date deleteTime = calendar.getTime();
        List<Event> events = eventRepository.findAllByForEmailReminder(deleteTime);
        List<EventMailHistory> eventMailHistories =
                events.stream().map(event -> new EventMailHistory(event.getId())).toList();
        eventMailHistoryRepository.saveAll(eventMailHistories);
        events.forEach(event -> mailNotifyUsersAboutEvent(event, -1L, Operation.READ, event.getGroupId()));
    }

    private void notifyUsersAboutEvent(Event event, Long userId, String operation, Long groupId) {
        notifyOnlineUsersAboutEvent(event, operation);
        cloudNotifyUsersAboutEvent(event, userId, operation);
        mailNotifyUsersAboutEvent(event, userId, operation, groupId);
    }

    private void notifyOnlineUsersAboutEvent(Event event, String operation) {
        List<Long> eventUsers = getEventsUsers(event.getId());
        EventDto eventDto = eventDtoMapper.eventToEventDto(event);
        messagesService.sendMessage(Channels.NOTIFY_ONLINE, GroupMessageTypeApi.EVENT, operation, eventDto, eventUsers);
    }

    private void cloudNotifyUsersAboutEvent(Event event, Long senderUserId, String operation) {
        EventDto eventDto = eventDtoMapper.eventToEventDto(event);
        List<Long> eventUsers = getEventsUsers(event.getId());
        List<Long> usersIdsNotification = new ArrayList<>(eventUsers);
        usersIdsNotification.remove(senderUserId);
        usersIdsNotification.removeAll(eventNotificationRepository.getUsersIdsByParticipantIdsIn(eventUsers));
        messagesService.sendMessage(Channels.CLOUD_MESSAGE, GroupMessageTypeApi.EVENT, operation,
                eventDto, usersIdsNotification);
    }

    private void mailNotifyUsersAboutEvent(Event event, Long senderUserId, String operation, Long groupId) {
        List<Long> eventUsers = getEventsUsers(event.getId());
        List<Long> usersIdsMailNotification = new ArrayList<>(eventUsers);
        usersIdsMailNotification.remove(senderUserId);
        usersIdsMailNotification.removeAll(eventNotificationMailRepository.getUsersIdsByParticipantIdsIn(eventUsers));
        com.teamarea.mail.api.dto.EventDto mailEventDto = mailEventDtoMapper.eventToMailEventDto(event);
        String groupName = groupRepository.findById(groupId).map(Group::getName).orElse("");
        mailEventDto.setGroupName(groupName);
        mailEventDto.setUsersIds(usersIdsMailNotification);
        messagesService.sendMessage(Channels.MAIL, GroupMessageTypeApi.EVENT, operation, mailEventDto);
    }

    private List<Long> getEventsUsers(Long eventId) {
        List<Long> eventUsers = eventParticipantRepository.selectUserIdsByEventId(eventId);
        eventUsers.addAll(eventRoleRepository.selectUserIdsByEventId(eventId));
        eventUsers = eventUsers.stream().distinct().toList();
        return eventUsers;
    }
}
