package com.teamarea.group.service;

import com.teamarea.group.service.datacheck.DataCheckException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class TeamareaExceptionsHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(GroupServiceException.class)
    public ResponseEntity<String> handleGroupServiceException(GroupServiceException exception) {
        return new ResponseEntity<>(exception.getMessage(), exception.getHttpStatus());
    }

    @ExceptionHandler(ParticipantServiceException.class)
    public ResponseEntity<String> handleParticipantServiceException(ParticipantServiceException exception) {
        return new ResponseEntity<>(exception.getMessage(), exception.getHttpStatus());
    }

    @ExceptionHandler(GroupUtilsException.class)
    public ResponseEntity<String> handleGroupUtilsException(GroupUtilsException exception) {
        return new ResponseEntity<>(exception.getMessage(), exception.getHttpStatus());
    }

    @ExceptionHandler(RoleServiceException.class)
    public ResponseEntity<String> handleRoleServiceException(RoleServiceException exception) {
        return new ResponseEntity<>(exception.getMessage(), exception.getHttpStatus());
    }

    @ExceptionHandler(DataCheckException.class)
    public ResponseEntity<String> handleDataCheckException(DataCheckException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<Object> handleConflict(Throwable e, WebRequest webRequest) {
        e.printStackTrace();
        RuntimeException exception = new RuntimeException("Internal server error");
        String bodyOfResponse = "Internal server error";
        return handleExceptionInternal(exception, bodyOfResponse, new HttpHeaders(),
                HttpStatus.INTERNAL_SERVER_ERROR, webRequest);

    }

}
