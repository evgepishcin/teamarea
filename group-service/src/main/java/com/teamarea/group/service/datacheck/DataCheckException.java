package com.teamarea.group.service.datacheck;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class DataCheckException extends RuntimeException {
    public DataCheckException(String message) {
        super(message);
    }
}
