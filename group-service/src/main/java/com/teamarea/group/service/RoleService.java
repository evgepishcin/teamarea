package com.teamarea.group.service;

import com.teamarea.common.data.api.Operation;
import com.teamarea.group.api.dto.GroupMessageTypeApi;
import com.teamarea.group.api.dto.RoleDtoApi;
import com.teamarea.group.dao.RoleDao;
import com.teamarea.group.domain.dto.NewRoleDto;
import com.teamarea.group.domain.dto.RoleDto;
import com.teamarea.group.domain.entity.Participant;
import com.teamarea.group.domain.entity.Role;
import com.teamarea.group.domain.mapper.NewRoleDtoMapper;
import com.teamarea.group.domain.mapper.RoleDtoApiMapper;
import com.teamarea.group.domain.mapper.RoleDtoMapper;
import com.teamarea.group.repository.RoleRepository;
import com.teamarea.group.service.datacheck.DataCheckerService;
import com.teamarea.group.service.messages.Channels;
import com.teamarea.group.service.messages.MessagesService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class RoleService {

    private final DataCheckerService dataCheckerService;
    private final NewRoleDtoMapper newRoleDtoMapper;
    private final RoleRepository roleRepository;
    private final RoleDtoApiMapper roleDtoApiMapper;
    private final MessagesService messagesService;
    private final RoleDtoMapper roleDtoMapper;
    private final GroupUtilsService groupUtilsService;
    private final RoleDao roleDao;

    @Transactional
    public RoleDto createRole(NewRoleDto newRoleDto, Long groupId, Long userId) {
        dataCheckerService.check(newRoleDto);
        Participant participant = groupUtilsService.checkAndReturnParticipant(userId, groupId, true, true);
        Role role = newRoleDtoMapper.newRoleDtoToRole(newRoleDto);
        role.setGroupId(participant.getGroupId());
        role = roleRepository.saveAndFlush(role);
        RoleDtoApi roleDtoApi = roleDtoApiMapper.roleToRoleDtoApi(role);
        messagesService.sendMessage(Channels.GROUP, GroupMessageTypeApi.ROLE, Operation.CREATE, roleDtoApi);
        return roleDtoMapper.roleToRoleDto(role);
    }

    @Transactional
    public RoleDto updateRole(NewRoleDto newRoleDto, Long roleId, Long groupId, Long userId) {
        dataCheckerService.check(newRoleDto);
        groupUtilsService.checkAndReturnParticipant(userId, groupId, true, true);
        Role role = groupUtilsService.checkRoleInGroupAndReturn(groupId, roleId);
        Role updatedRole = newRoleDtoMapper.newRoleDtoToRole(newRoleDto);
        updatedRole.setId(role.getId());
        updatedRole.setGroupId(role.getGroupId());
        updatedRole = roleRepository.saveAndFlush(updatedRole);
        RoleDtoApi roleDtoApi = roleDtoApiMapper.roleToRoleDtoApi(updatedRole);
        messagesService.sendMessage(Channels.GROUP, GroupMessageTypeApi.ROLE, Operation.UPDATE, roleDtoApi);
        return roleDtoMapper.roleToRoleDto(updatedRole);
    }

    @Transactional(readOnly = true)
    public RoleDto getRole(Long roleId, Long groupId, Long userId) {
        groupUtilsService.checkAndReturnParticipant(userId, groupId, false, true);
        Role role = groupUtilsService.checkRoleInGroupAndReturn(groupId, roleId);
        return roleDtoMapper.roleToRoleDto(role);
    }

    @Transactional(readOnly = true)
    public List<RoleDto> getAllGroupRoles(Long groupId, Long userId) {
        groupUtilsService.checkAndReturnParticipant(userId, groupId, false, true);
        List<Role> roles = roleRepository.findAllByGroupId(groupId);
        return roles.stream().map(roleDtoMapper::roleToRoleDto).toList();
    }

    @Transactional
    public void deleteRole(Long roleId, Long groupId, Long userId) {
        groupUtilsService.checkAndReturnParticipant(userId, groupId, true, true);
        Role role = groupUtilsService.checkRoleInGroupAndReturn(groupId, roleId);
        roleDao.deleteRole(roleId);
        RoleDtoApi roleDtoApi = roleDtoApiMapper.roleToRoleDtoApi(role);
        messagesService.sendMessage(Channels.GROUP, GroupMessageTypeApi.ROLE, Operation.DELETE, roleDtoApi);
    }
}
