package com.teamarea.group.service;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class GroupServiceException extends RuntimeException {

    private final HttpStatus httpStatus;

    public GroupServiceException(String message) {
        super(message);
        httpStatus = HttpStatus.FORBIDDEN;
    }

    public GroupServiceException(String message, HttpStatus httpStatus) {
        super(message);
        this.httpStatus = httpStatus;
    }
}
