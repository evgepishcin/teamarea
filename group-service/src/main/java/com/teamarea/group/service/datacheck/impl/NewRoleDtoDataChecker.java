package com.teamarea.group.service.datacheck.impl;

import com.teamarea.group.domain.dto.NewRoleDto;
import com.teamarea.group.service.datacheck.DataCheckException;
import com.teamarea.group.service.datacheck.DataChecker;
import org.springframework.stereotype.Component;

@Component
public class NewRoleDtoDataChecker implements DataChecker<NewRoleDto> {
    @Override
    public void check(NewRoleDto newRoleDto) throws DataCheckException {
        if (newRoleDto.getName() == null || newRoleDto.getName().length() == 0) {
            throw new DataCheckException("Role name can't be empty!");
        }
    }

    @Override
    public Class<?> getDataClass() {
        return NewRoleDto.class;
    }
}
