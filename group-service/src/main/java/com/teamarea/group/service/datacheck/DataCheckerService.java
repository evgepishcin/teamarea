package com.teamarea.group.service.datacheck;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@SuppressWarnings({"unchecked", "rawtypes"})
public class DataCheckerService {

    private final Map<Class, DataChecker> dataCheckerMap;

    public DataCheckerService(List<DataChecker> dataCheckers) {
        dataCheckerMap = new HashMap<>();
        dataCheckers.forEach(dataChecker -> dataCheckerMap.put(dataChecker.getDataClass(), dataChecker));
    }

    public void check(Object value) throws DataCheckException {
        DataChecker dataChecker = dataCheckerMap.get(value.getClass());
        if (dataChecker == null) {
            throw new DataCheckException("Implementation of dataChecker with " +
                    "this class type doesn't exist");
        }
        dataChecker.check(value);
    }
}
