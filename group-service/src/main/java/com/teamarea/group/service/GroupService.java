package com.teamarea.group.service;

import com.teamarea.common.data.api.Operation;
import com.teamarea.group.api.dto.GroupDtoApi;
import com.teamarea.group.api.dto.GroupMessageTypeApi;
import com.teamarea.group.api.dto.ParticipantDtoApi;
import com.teamarea.group.dao.GroupDao;
import com.teamarea.group.domain.dto.GroupDto;
import com.teamarea.group.domain.dto.GroupInfoDto;
import com.teamarea.group.domain.dto.NewGroupDto;
import com.teamarea.group.domain.entity.Group;
import com.teamarea.group.domain.entity.Participant;
import com.teamarea.group.domain.mapper.*;
import com.teamarea.group.domain.pojo.GroupInfoPojo;
import com.teamarea.group.repository.GroupRepository;
import com.teamarea.group.repository.ParticipantRepository;
import com.teamarea.group.service.datacheck.DataCheckerService;
import com.teamarea.group.service.messages.Channels;
import com.teamarea.group.service.messages.MessagesService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class GroupService {

    private final NewGroupDtoMapper newGroupDtoMapper;
    private final GroupDtoMapper groupDtoMapper;
    private final ParticipantDtoApiMapper participantDtoApiMapper;
    private final GroupDtoApiMapper groupDtoApiMapper;
    private final GroupRepository groupRepository;
    private final DataCheckerService dataCheckerService;
    private final ParticipantRepository participantRepository;
    private final MessagesService messagesService;
    private final GroupUtilsService groupUtilsService;
    private final GroupInfoDtoMapper groupInfoDtoMapper;
    private final GroupDao groupDao;

    /**
     * Создание новой группы
     *
     * @param newGroupDto информация для создания новой группы
     * @param userId      идентификатор пользователя
     * @return новая созданная группа
     */
    @Transactional
    public GroupDto createGroup(NewGroupDto newGroupDto, Long userId) {
        dataCheckerService.check(newGroupDto);
        Group group = newGroupDtoMapper.newGroupDtoToGroup(newGroupDto);
        group = groupRepository.saveAndFlush(group);
        Participant participant = new Participant();
        participant.setGroupId(group.getId());
        participant.setUserId(userId);
        participant.setModerator(true);
        participant.setActive(true);
        participant = participantRepository.save(participant);
        ParticipantDtoApi participantDtoApi = participantDtoApiMapper.participantToParticipantDtoApi(participant);
        messagesService.sendMessage(Channels.GROUP, GroupMessageTypeApi.PARTICIPANT,
                Operation.CREATE, participantDtoApi);
        return groupDtoMapper.groupToGroupDto(group);
    }

    @Transactional
    public void leaveGroup(Long groupId, Long userId) {
        Participant participant = groupUtilsService.checkAndReturnParticipant(userId, groupId, false, true);
        if (Boolean.TRUE.equals(participant.getModerator())) {
            groupUtilsService.checkModeratorLeave(groupId);
        }
        participant.setUserId(null);
        participantRepository.save(participant);
        ParticipantDtoApi participantDtoApi = participantDtoApiMapper.participantToParticipantDtoApi(participant);
        messagesService.sendMessage(Channels.GROUP, GroupMessageTypeApi.PARTICIPANT, Operation.UPDATE, participantDtoApi);
    }

    @Transactional(readOnly = true)
    public List<GroupInfoDto> getUserGroups(Long userId) {
        List<GroupInfoPojo> groupInfoPojos = groupRepository.getUserGroups(userId);
        return groupInfoPojos.stream()
                .map(groupInfoDtoMapper::groupInfoPojoToGroupInfoDto)
                .toList();
    }

    @Transactional(readOnly = true)
    public GroupDto getGroup(Long groupId, Long userId) {
        Participant participant = groupUtilsService
                .checkAndReturnParticipant(userId, groupId, false, true);
        Group group = groupRepository.findById(participant.getGroupId()).orElse(null);
        return groupDtoMapper.groupToGroupDto(group);
    }

    @Transactional
    public GroupDto acceptInvite(Long userId, String inviteCode) {
        Optional<Participant> participantOptional = participantRepository.findByInvitationCode(inviteCode);
        if (participantOptional.isEmpty()) {
            throw new GroupServiceException("This invite code does not exist");
        }
        Participant participant = participantOptional.get();
        participant.setInvitationCode(null);
        participant.setUserId(userId);
        participant = participantRepository.saveAndFlush(participant);
        Group group = groupRepository.findById(participant.getGroupId()).orElse(null);
        GroupDto groupDto = groupDtoMapper.groupToGroupDto(group);
        ParticipantDtoApi participantDtoApi = participantDtoApiMapper.participantToParticipantDtoApi(participant);
        messagesService.sendMessage(Channels.GROUP, GroupMessageTypeApi.PARTICIPANT,
                Operation.UPDATE, participantDtoApi);
        return groupDto;
    }

    @Transactional
    public void deleteGroup(Long groupId, Long userId) {
        groupUtilsService.checkAndReturnParticipant(userId, groupId, true, true);
        Group group = groupRepository.findById(groupId).orElseThrow(() -> new GroupServiceException("Can't find group"));
        GroupDtoApi groupDtoApi = groupDtoApiMapper.groupToGroupDtoApi(group);
        groupDao.deleteGroup(groupId);
        messagesService.sendMessage(Channels.GROUP, GroupMessageTypeApi.GROUP, Operation.DELETE, groupDtoApi);
    }
}
