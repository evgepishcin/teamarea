package com.teamarea.group.service.datacheck.impl;

import com.teamarea.group.domain.dto.NewParticipantDto;
import com.teamarea.group.service.datacheck.DataCheckException;
import com.teamarea.group.service.datacheck.DataChecker;
import org.springframework.stereotype.Component;

@Component
public class NewParticipantDtoDataChecker implements DataChecker<NewParticipantDto> {
    @Override
    public void check(NewParticipantDto newParticipantDto) throws DataCheckException {
        if (newParticipantDto.getSurname() != null && newParticipantDto.getSurname().length() > 50) {
            throw new DataCheckException("field \"surname\" is very long");
        }
        if (newParticipantDto.getName() != null && newParticipantDto.getName().length() > 50) {
            throw new DataCheckException("field \"name\" is very long");
        }
        if (newParticipantDto.getPatronymic() != null && newParticipantDto.getPatronymic().length() > 50) {
            throw new DataCheckException("field \"patronymic\" is very long");
        }
    }

    @Override
    public Class<?> getDataClass() {
        return NewParticipantDto.class;
    }
}
