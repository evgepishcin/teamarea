package com.teamarea.group.service;

import com.teamarea.common.data.api.Operation;
import com.teamarea.group.api.dto.GroupMessageTypeApi;
import com.teamarea.group.api.dto.ParticipantDtoApi;
import com.teamarea.group.dao.ParticipantDao;
import com.teamarea.group.domain.comparator.ParticipantAlphabetComparator;
import com.teamarea.group.domain.comparator.ParticipantRoleComparator;
import com.teamarea.group.domain.dto.NewParticipantDto;
import com.teamarea.group.domain.dto.ParticipantDto;
import com.teamarea.group.domain.entity.Participant;
import com.teamarea.group.domain.mapper.NewParticipantDtoMapper;
import com.teamarea.group.domain.mapper.ParticipantDtoApiMapper;
import com.teamarea.group.domain.mapper.ParticipantDtoMapper;
import com.teamarea.group.repository.ParticipantRepository;
import com.teamarea.group.service.datacheck.DataCheckerService;
import com.teamarea.group.service.messages.Channels;
import com.teamarea.group.service.messages.MessagesService;
import com.teamarea.group.utils.AppUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ParticipantService {

    private final DataCheckerService dataCheckerService;
    private final GroupUtilsService groupUtilsService;
    private final NewParticipantDtoMapper newParticipantDtoMapper;
    private final ParticipantRepository participantRepository;
    private final MessagesService messagesService;
    private final ParticipantDtoApiMapper participantDtoApiMapper;
    private final ParticipantDtoMapper participantDtoMapper;
    private final AppUtils appUtils;
    private final ParticipantDao participantDao;

    @Transactional
    public ParticipantDto createParticipant(NewParticipantDto newParticipantDto, Long groupId, Long userId) {
        dataCheckerService.check(newParticipantDto);
        groupUtilsService.checkAndReturnParticipant(userId, groupId, true, true);
        if (newParticipantDto.getRoleId() != null) {
            groupUtilsService.checkRoleInGroupAndReturn(groupId, newParticipantDto.getRoleId());
        }
        Participant participant = newParticipantDtoMapper.newParticipantDtoToParticipant(newParticipantDto);
        participant.setGroupId(groupId);
        participant.setActive(true);
        participant = participantRepository.saveAndFlush(participant);
        ParticipantDtoApi participantDtoApi = participantDtoApiMapper.participantToParticipantDtoApi(participant);
        ParticipantDto participantDto = participantDtoMapper.participantToParticipantDto(participant);
        messagesService.sendMessage(Channels.GROUP, GroupMessageTypeApi.PARTICIPANT,
                Operation.CREATE, participantDtoApi);
        return participantDto;
    }

    @Transactional
    public ParticipantDto updateParticipant(NewParticipantDto newParticipantDto, Long participantId,
                                            Long groupId, Long userId) {
        dataCheckerService.check(newParticipantDto);
        groupUtilsService.checkAndReturnParticipant(userId, groupId, true, true);
        if (newParticipantDto.getRoleId() != null) {
            groupUtilsService.checkRoleInGroupAndReturn(groupId, newParticipantDto.getRoleId());
        }
        Participant participant = groupUtilsService.checkParticipantInGroupAndReturn(groupId, participantId, false, true);
        if (Boolean.TRUE.equals(participant.getModerator()) && Boolean.FALSE.equals(newParticipantDto.getModerator())) {
            groupUtilsService.checkModeratorLeave(groupId);
        }
        Participant updatedParticipant = newParticipantDtoMapper.newParticipantDtoToParticipant(newParticipantDto);
        updatedParticipant.setId(participant.getId());
        updatedParticipant.setGroupId(participant.getGroupId());
        updatedParticipant.setUserId(participant.getUserId());
        updatedParticipant.setActive(participant.getActive());
        updatedParticipant.setInvitationCode(participant.getInvitationCode());
        updatedParticipant = participantRepository.saveAndFlush(updatedParticipant);
        ParticipantDtoApi participantDtoApi =
                participantDtoApiMapper.participantToParticipantDtoApi(updatedParticipant);
        ParticipantDto participantDto = participantDtoMapper.participantToParticipantDto(updatedParticipant);
        messagesService.sendMessage(Channels.GROUP, GroupMessageTypeApi.PARTICIPANT,
                Operation.UPDATE, participantDtoApi);
        return participantDto;
    }

    @Transactional(readOnly = true)
    public ParticipantDto getParticipant(Long participantId, Long groupId, Long userId) {
        Participant participantUser = groupUtilsService.checkAndReturnParticipant(userId, groupId, false, true);
        Participant participant = groupUtilsService.checkParticipantInGroupAndReturn(groupId, participantId, false, true);
        ParticipantDto participantDto = participantDtoMapper.participantToParticipantDto(participant);
        if (Boolean.FALSE.equals(participantUser.getModerator())) {
            participantDto.setInvitationCode(null);
        }
        return participantDto;
    }

    @Transactional(readOnly = true)
    public List<ParticipantDto> getGroupParticipants(Long groupId, Long userId, Long roleId, String sortType) {
        Participant participant = groupUtilsService.checkAndReturnParticipant(userId, groupId, false, true);
        List<Participant> participants;
        if (roleId == null) {
            participants = participantRepository.findAllByGroupIdAndActiveIsTrue(groupId);
        } else {
            participants = participantRepository.findAllByGroupIdAndRoleIdAndActiveIsTrue(groupId, roleId);
        }
        if ("role".equals(sortType)) {
            participants.sort(new ParticipantRoleComparator());
        } else {
            participants.sort(new ParticipantAlphabetComparator());
        }
        List<ParticipantDto> participantDtos = participants.stream()
                .map(participantDtoMapper::participantToParticipantDto)
                .toList();
        if (Boolean.FALSE.equals(participant.getModerator())) {
            participantDtos.forEach(p -> p.setInvitationCode(null));
        }
        return participantDtos;
    }

    @Transactional
    public ParticipantDto deleteParticipant(Long participantId, Long groupId, Long userId) {
        groupUtilsService.checkAndReturnParticipant(userId, groupId, true, true);
        Participant participant = groupUtilsService.checkParticipantInGroupAndReturn(groupId, participantId, false, true);
        if (Boolean.TRUE.equals(participant.getModerator())) {
            groupUtilsService.checkModeratorLeave(groupId);
        }
        participant.setUserId(null);
        participant.setActive(false);
        participantDao.disableParticipant(participantId);
        ParticipantDtoApi participantDtoApi = participantDtoApiMapper.participantToParticipantDtoApi(participant);
        ParticipantDto participantDto = participantDtoMapper.participantToParticipantDto(participant);
        messagesService.sendMessage(Channels.GROUP, GroupMessageTypeApi.PARTICIPANT,
                Operation.DELETE, participantDtoApi);
        return participantDto;
    }

    @Transactional
    public ParticipantDto createInviteCode(Long participantId, Long groupId, Long userId) {
        groupUtilsService.checkAndReturnParticipant(userId, groupId, true, true);
        Participant participant = groupUtilsService.checkParticipantInGroupAndReturn(groupId, participantId,
                false, true);
        if (participant.getUserId() != null) {
            throw new ParticipantServiceException("Can't set invite code to participant which has connected user");
        }
        String inviteCode = appUtils.generateInviteCode();
        participant.setInvitationCode(inviteCode);
        participantRepository.save(participant);
        return participantDtoMapper.participantToParticipantDto(participant);
    }

    @Transactional
    public ParticipantDto deleteInviteCode(Long participantId, Long groupId, Long userId) {
        groupUtilsService.checkAndReturnParticipant(userId, groupId, true, true);
        Participant participant = groupUtilsService.checkParticipantInGroupAndReturn(groupId, participantId,
                false, true);
        participant.setInvitationCode(null);
        participantRepository.save(participant);
        return participantDtoMapper.participantToParticipantDto(participant);
    }
}
