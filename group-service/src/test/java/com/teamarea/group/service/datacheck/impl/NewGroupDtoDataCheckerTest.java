package com.teamarea.group.service.datacheck.impl;

import com.teamarea.group.domain.dto.NewGroupDto;
import com.teamarea.group.service.datacheck.DataCheckException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@ActiveProfiles("test")
class NewGroupDtoDataCheckerTest {
    @Autowired
    private NewGroupDtoDataChecker newGroupDtoDataChecker;

    @Test
    void checkTest() {
        NewGroupDto newGroupDto = new NewGroupDto("name", "about");
        newGroupDtoDataChecker.check(newGroupDto);
        newGroupDto.setName("");
        assertThrows(DataCheckException.class, () -> newGroupDtoDataChecker.check(newGroupDto));
        newGroupDto.setName(null);
        assertThrows(DataCheckException.class, () -> newGroupDtoDataChecker.check(newGroupDto));
    }

    @Test
    void getDataClass() {
        assertEquals(NewGroupDto.class, newGroupDtoDataChecker.getDataClass());
    }
}