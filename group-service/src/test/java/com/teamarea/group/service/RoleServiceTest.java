package com.teamarea.group.service;

import com.teamarea.group.api.dto.RoleDtoApi;
import com.teamarea.group.domain.dto.NewRoleDto;
import com.teamarea.group.domain.dto.RoleDto;
import com.teamarea.group.domain.entity.Participant;
import com.teamarea.group.domain.entity.Role;
import com.teamarea.group.repository.ParticipantRepository;
import com.teamarea.group.repository.RoleRepository;
import com.teamarea.group.service.messages.MessagesService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@ActiveProfiles("test")
class RoleServiceTest {

    @Autowired
    private RoleService roleService;
    @MockBean
    private ParticipantRepository participantRepository;
    @MockBean
    private RoleRepository roleRepository;
    @MockBean
    private MessagesService messagesService;
    @Captor
    private ArgumentCaptor<Role> roleArgumentCaptor;
    @Captor
    private ArgumentCaptor<RoleDtoApi> roleDtoApiArgumentCaptor;

    private NewRoleDto newRoleDto;
    private Participant participant;
    private Role role;

    @BeforeEach
    void setUp() {
        newRoleDto = defNewRoleDto();
        participant = defParticipant();
        role = defRole();
        Mockito.when(participantRepository.findByUserIdAndGroupId(3L, 2L))
                .thenReturn(Optional.ofNullable(participant));
        Mockito.when(roleRepository.saveAndFlush(Mockito.any())).thenReturn(role);
    }

    @Test
    void createRole() {
        RoleDto roleDto = roleService.createRole(newRoleDto, 2L, 3L);
        Mockito.verify(participantRepository).findByUserIdAndGroupId(3L, 2L);
        Mockito.verify(roleRepository).saveAndFlush(roleArgumentCaptor.capture());
        Role role = roleArgumentCaptor.getValue();
        assertEquals(2L, role.getGroupId());
        assertEquals("name1", role.getName());
        assertEquals("about1", role.getAbout());

        Mockito.verify(messagesService).sendMessage(Mockito.eq("group"), Mockito.eq("ROLE"),
                Mockito.eq("CREATE"), roleDtoApiArgumentCaptor.capture());
        RoleDtoApi roleDtoApi = roleDtoApiArgumentCaptor.getValue();
        assertEquals(4L, roleDtoApi.getId());
        assertEquals(2L, roleDtoApi.getGroupId());

        assertEquals(4L, roleDto.getId());
        assertEquals(2L, roleDto.getGroupId());
        assertEquals("name1", roleDto.getName());
        assertEquals("about1", roleDto.getAbout());
    }

    @Test
    void createRole_userNotInGroup() {
        Mockito.when(participantRepository.findByUserIdAndGroupId(3L, 2L))
                .thenReturn(Optional.empty());
        assertThrows(GroupUtilsException.class, () -> roleService.createRole(newRoleDto, 2L, 3L));
    }

    Role defRole() {
        Role role = new Role();
        role.setId(4L);
        role.setGroupId(2L);
        role.setName("name1");
        role.setAbout("about1");
        return role;
    }

    NewRoleDto defNewRoleDto() {
        return new NewRoleDto("name1", "about1");
    }

    Participant defParticipant() {
        Participant participant = new Participant();
        participant.setId(1L);
        participant.setGroupId(2L);
        participant.setUserId(3L);
        participant.setModerator(true);
        participant.setActive(true);
        return participant;
    }
}