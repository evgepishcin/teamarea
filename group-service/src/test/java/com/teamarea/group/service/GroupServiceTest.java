package com.teamarea.group.service;

import com.teamarea.group.api.dto.ParticipantDtoApi;
import com.teamarea.group.domain.dto.GroupDto;
import com.teamarea.group.domain.dto.NewGroupDto;
import com.teamarea.group.domain.entity.Group;
import com.teamarea.group.domain.entity.Participant;
import com.teamarea.group.repository.GroupRepository;
import com.teamarea.group.repository.ParticipantRepository;
import com.teamarea.group.service.messages.MessagesService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
class GroupServiceTest {

    @Autowired
    private GroupService groupService;
    @MockBean
    private GroupRepository groupRepository;
    @MockBean
    private ParticipantRepository participantRepository;
    @MockBean
    private MessagesService messagesService;
    @Captor
    private ArgumentCaptor<Group> newGroupDtoArgumentCaptor;
    @Captor
    private ArgumentCaptor<Participant> participantArgumentCaptor;
    @Captor
    private ArgumentCaptor<ParticipantDtoApi> participantDtoApiArgumentCaptor;


    @BeforeEach
    void setUp() {
        Group group = new Group();
        group.setId(2L);
        group.setName("group_name");
        group.setAbout("group_about");
        Mockito.when(groupRepository.saveAndFlush(Mockito.any())).thenReturn(group);

        Participant participant = new Participant();
        participant.setId(1L);
        participant.setGroupId(2L);
        participant.setUserId(3L);
        participant.setActive(true);
        participant.setModerator(true);
        Mockito.when(participantRepository.save(Mockito.any())).thenReturn(participant);
    }

    @Test
    void createGroupTest() {
        NewGroupDto newGroupDto = new NewGroupDto("group_name", "group_about");
        GroupDto groupDto = groupService.createGroup(newGroupDto, 3L);
        Mockito.verify(groupRepository).saveAndFlush(newGroupDtoArgumentCaptor.capture());
        Group group = newGroupDtoArgumentCaptor.getValue();
        assertEquals("group_name", group.getName());
        assertEquals("group_about", group.getAbout());

        Mockito.verify(participantRepository).save(participantArgumentCaptor.capture());
        Participant participant = participantArgumentCaptor.getValue();
        assertEquals(2L, participant.getGroupId());
        assertEquals(3L, participant.getUserId());
        assertTrue(participant.getModerator());
        assertTrue(participant.getActive());
        assertNull(participant.getRoleId());
        assertNull(participant.getSurname());
        assertNull(participant.getName());
        assertNull(participant.getPatronymic());

        Mockito.verify(messagesService).sendMessage(Mockito.eq("group"), Mockito.eq("PARTICIPANT"),
                Mockito.eq("CREATE"), participantDtoApiArgumentCaptor.capture());
        ParticipantDtoApi participantDtoApi = participantDtoApiArgumentCaptor.getValue();
        assertEquals(1L, participantDtoApi.getId());
        assertEquals(2L, participantDtoApi.getGroupId());
        assertEquals(3L, participantDtoApi.getUserId());
        assertTrue(participantDtoApi.getActive());
        assertTrue(participantDtoApi.getModerator());
        assertNull(participantDtoApi.getRoleId());

        assertEquals(2L, groupDto.getId());
        assertEquals("group_name", groupDto.getName());
        assertEquals("group_about", groupDto.getAbout());
    }
}