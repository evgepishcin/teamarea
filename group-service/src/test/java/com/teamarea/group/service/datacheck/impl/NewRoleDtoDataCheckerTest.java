package com.teamarea.group.service.datacheck.impl;

import com.teamarea.group.domain.dto.NewRoleDto;
import com.teamarea.group.service.datacheck.DataCheckException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@ActiveProfiles("test")
class NewRoleDtoDataCheckerTest {

    @Autowired
    private NewRoleDtoDataChecker newRoleDtoDataChecker;

    @Test
    void checkTest() {
        NewRoleDto newRoleDto = new NewRoleDto("name", "about");
        newRoleDtoDataChecker.check(newRoleDto);
        newRoleDto.setName(null);
        assertThrows(DataCheckException.class, () -> newRoleDtoDataChecker.check(newRoleDto));
        newRoleDto.setName("");
        assertThrows(DataCheckException.class, () -> newRoleDtoDataChecker.check(newRoleDto));
    }

    @Test
    void getDataClass() {
        assertEquals(NewRoleDto.class, newRoleDtoDataChecker.getDataClass());
    }

}