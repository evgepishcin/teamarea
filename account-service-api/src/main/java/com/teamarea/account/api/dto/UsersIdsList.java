package com.teamarea.account.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Информация о идентификаторах пользователей
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UsersIdsList {

    /**
     * Список идентификаторов пользователей
     */
    List<Long> usersIds;
}
