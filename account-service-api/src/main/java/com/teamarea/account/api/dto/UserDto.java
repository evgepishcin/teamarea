package com.teamarea.account.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

/**
 * Информация о пользователе
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {

    /**
     * Идентификатор пользователя
     */
    private Long userId;

    /**
     * Логин пользователя
     */
    private String login;

    /**
     * Статус подтверждения аккаунта
     */
    private Boolean confirm;

    /**
     * Дата создания пользователя
     */
    private Date createDate;

    /**
     * Статуст включенного аккаунта
     */
    private Boolean enabled;
}
