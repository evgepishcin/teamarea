package com.teamarea.account.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

/**
 * Информация о идентификаторах устройств
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DeviceUuidList {

    /**
     * Список идентификаторов устройств
     */
    private List<UUID> uuid;
}
