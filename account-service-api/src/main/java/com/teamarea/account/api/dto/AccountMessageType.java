package com.teamarea.account.api.dto;

import lombok.experimental.UtilityClass;

/**
 * Типы передаваемых сообщений
 */
@UtilityClass
public final class AccountMessageType {
    public final String USER = "USER";
    public final String USERS_ID_LIST = "USERS_ID_LIST";
    public final String DEVICE_UUID_LIST = "DEVICE_UUID_LIST";
}
