package com.teamarea.common.data.api;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * Класс для передачи данных между микросервисами через брокер сообщений
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class TransferData implements Serializable {

    /**
     * Тип сообщения. Сервис, который отправляет сообщение, записывает в это поле название
     * класса сообщения, что бы можно было узнать, в какой класс следует десериализовать данное сообщение
     */
    private String type;

    /**
     * Статус операции
     * @see Operation
     */
    private String operation;

    /**
     * Сериализованное в строку передаваемое сообщение
     */
    private String payload;
}
