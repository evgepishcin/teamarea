package com.teamarea.common.data.api;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Класс для передачи данных между микросервисами через брокер сообщений,
 * с указанием получателей сообщений
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class TransferDataWithUserIds {
    /**
     * Тип сообщения. Сервис, который отправляет сообщение записывает в это поле название
     * класса сообщения, что бы можно было узнать, в какой класс следует десериализовать данное сообщение
     */
    private String type;

    /**
     * Идентификаторы пользователей, для которых предназначается данное сообщение
     */
    private List<Long> usersIds;

    /**
     * Статус операции
     * @see Operation
     */
    private String operation;

    /**
     * Сериализованное в строку передаваемое сообщение
     */
    private String payload;
}
