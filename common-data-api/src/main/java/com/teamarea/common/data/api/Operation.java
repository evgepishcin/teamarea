package com.teamarea.common.data.api;

import lombok.experimental.UtilityClass;

/**
 * Статус операции
 */
@UtilityClass
public class Operation {
    public final String CREATE = "CREATE";
    public final String READ = "READ";
    public final String UPDATE = "UPDATE";
    public final String DELETE = "DELETE";
}
