package com.teamarea.notification.controller;

import com.teamarea.common.data.api.TransferData;
import com.teamarea.notification.service.NotificationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@RestController
@RequiredArgsConstructor
public class ListenerController {

    private static final long TIMEOUT = 15000L;

    private final NotificationService notificationService;

    @GetMapping("/notification/listen")
    public DeferredResult<List<TransferData>> listen(@RequestHeader("User-Id") Long userId) {
        DeferredResult<List<TransferData>> deferredResult = new DeferredResult<>(TIMEOUT);
        CompletableFuture.runAsync(() -> notificationService.addRequest(deferredResult, userId));
        return deferredResult;
    }
}
