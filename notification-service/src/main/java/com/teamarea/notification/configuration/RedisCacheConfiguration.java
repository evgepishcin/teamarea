package com.teamarea.notification.configuration;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.cache.RedisCacheManagerBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;

@Configuration
@RequiredArgsConstructor
public class RedisCacheConfiguration {

    public static final String CACHE_NAME = "notifications";
    private static final long TTL = 10L;

    @Bean
    public RedisCacheManagerBuilderCustomizer redisCacheManagerBuilderCustomizer() {
        return builder -> builder
                .withCacheConfiguration(CACHE_NAME,
                        org.springframework.data.redis.cache.RedisCacheConfiguration
                                .defaultCacheConfig(Thread.currentThread().getContextClassLoader())
                                .entryTtl(Duration.ofSeconds(TTL)));
    }

}
