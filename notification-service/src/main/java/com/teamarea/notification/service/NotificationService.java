package com.teamarea.notification.service;

import com.teamarea.common.data.api.TransferData;
import com.teamarea.common.data.api.TransferDataWithUserIds;
import com.teamarea.notification.configuration.RedisCacheConfiguration;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class NotificationService {

    private final Cache cache;
    private final Map<Long, DeferredResult<List<TransferData>>> usersConnections = new ConcurrentHashMap<>();

    public NotificationService(CacheManager cacheManager) {
        this.cache = cacheManager.getCache(RedisCacheConfiguration.CACHE_NAME);
    }

    public void notifyUsers(TransferDataWithUserIds data) {
        TransferData transferData = new TransferData();
        transferData.setType(data.getType());
        transferData.setOperation(data.getOperation());
        transferData.setPayload(data.getPayload());
        for (Long userId : data.getUsersIds()) {
            ArrayList<TransferData> lastData = Optional.ofNullable(cache.get(userId, ArrayList.class))
                    .orElse(new ArrayList<>());
            lastData.add(transferData);
            DeferredResult<List<TransferData>> result = usersConnections.get(userId);
            if (result != null) {
                result.setResult(lastData);
                cache.evict(userId);
            } else {
                cache.put(userId, lastData);
            }
        }
    }

    public void addRequest(DeferredResult<List<TransferData>> deferredResult, Long userId) {
        usersConnections.put(userId, deferredResult);
        deferredResult.onCompletion(() -> usersConnections.remove(userId));
        deferredResult.onTimeout(() -> usersConnections.remove(userId));
        deferredResult.onError(throwable -> usersConnections.remove(userId));
        ArrayList<TransferData> lastData = Optional.ofNullable(cache.get(userId, ArrayList.class))
                .orElse(new ArrayList<>());
        if (!lastData.isEmpty()) {
            deferredResult.setResult(lastData);
            cache.evict(userId);
        }
    }
}
