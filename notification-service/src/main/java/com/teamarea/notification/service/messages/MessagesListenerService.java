package com.teamarea.notification.service.messages;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.teamarea.common.data.api.TransferDataWithUserIds;
import com.teamarea.notification.service.NotificationService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MessagesListenerService {

    private final ObjectMapper objectMapper;
    private final NotificationService notificationService;

    @SneakyThrows
    @KafkaListener(topics = Channels.CLOUD_MESSAGE)
    public void listenNotify(String message) {
        TransferDataWithUserIds data = objectMapper.readValue(message, TransferDataWithUserIds.class);
        notificationService.notifyUsers(data);
    }
}
