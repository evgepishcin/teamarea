package com.teamarea.notification;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;


@SpringBootTest
@ActiveProfiles("test")
class NotificationAppTest {

    @Test
    @SuppressWarnings("java:S2699")
    void contextLoads() {
    }

}