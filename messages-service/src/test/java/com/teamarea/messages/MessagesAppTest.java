package com.teamarea.messages;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
class MessagesAppTest {

    @Test
    @SuppressWarnings("java:S2699")
    void contextLoads() {
    }

}