package com.teamarea.messages.service;

import com.teamarea.common.data.api.Operation;
import com.teamarea.messages.dao.PersonalMessageDao;
import com.teamarea.messages.domain.dto.NewMessageDto;
import com.teamarea.messages.domain.dto.NotificationStatusDto;
import com.teamarea.messages.domain.dto.PersonalMessageDto;
import com.teamarea.messages.domain.entity.Participant;
import com.teamarea.messages.domain.entity.PersonalMessage;
import com.teamarea.messages.domain.entity.PersonalMessageNotification;
import com.teamarea.messages.domain.mapper.PersonalMessageDtoMapper;
import com.teamarea.messages.repository.ParticipantRepository;
import com.teamarea.messages.repository.PersonalMessageNotificationRepository;
import com.teamarea.messages.service.messages.Channels;
import com.teamarea.messages.service.messages.MessagesService;
import com.teamarea.messages.service.messages.MessagesTypes;
import lombok.RequiredArgsConstructor;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PersonalMessageService {

    private final AppUtilsService appUtilsService;
    private final ParticipantRepository participantRepository;
    private final PersonalMessageDao personalMessageDao;
    private final PersonalMessageDtoMapper personalMessageDtoMapper;
    private final PersonalMessageNotificationRepository personalMessageNotificationRepository;
    private final MessagesService messagesService;

    @Transactional
    public PersonalMessageDto sendPersonalMessage(NewMessageDto newMessageDto, Long participantId, Long groupId, Long userId) {
        Participant participant = appUtilsService.checkAndReturnParticipant(userId, groupId, false, true);
        appUtilsService.checkParticipantInGroupAndReturn(participantId, groupId);
        PersonalMessage personalMessage = new PersonalMessage();
        personalMessage.setText(newMessageDto.getText());
        personalMessage.setSenderId(participant.getId());
        personalMessage.setRecipientId(participantId);
        personalMessage.setCreateDate(new Date());
        personalMessage = personalMessageDao.savePersonalMessage(personalMessage);
        PersonalMessageDto personalMessageDto = personalMessageDtoMapper
                .personalMessageToPersonalMessageDto(personalMessage);
        notifyUsersAboutPersonalMessage(personalMessageDto);
        return personalMessageDto;

    }

    @Transactional
    public List<PersonalMessage> getPersonalMessages(Long participantId, Long lastId, Long groupId, Long userId) {
        Participant participant = appUtilsService.checkAndReturnParticipant(userId, groupId, false, true);
        Participant interlocutor = appUtilsService.checkParticipantInGroupAndReturn(participantId, groupId);
        Pair<List<PersonalMessage>, List<PersonalMessage>> selectedMessages =
                personalMessageDao.getPersonalMessages(participant.getId(), participantId, lastId);
        List<PersonalMessage> messages = selectedMessages.getFirst();
        List<PersonalMessageDto> readiedMessages = selectedMessages.getSecond().stream()
                .map(personalMessageDtoMapper::personalMessageToPersonalMessageDto).toList();
        if (!readiedMessages.isEmpty()) {
            List<Long> users = new ArrayList<>();
            users.add(participant.getUserId());
            if (interlocutor.getUserId() != null) {
                users.add(interlocutor.getUserId());
            }
            messagesService.sendMessage(Channels.NOTIFY_ONLINE, MessagesTypes.PERSONAL_MESSAGE_LIST, Operation.UPDATE,
                    readiedMessages, users);
        }
        return messages;
    }

    @Transactional(readOnly = true)
    public List<PersonalMessageDto> getLastPersonalMessages(Long lastId, Long groupId, Long userId) {
        Participant participant = appUtilsService.checkAndReturnParticipant(userId, groupId, false, true);
        List<PersonalMessage> messages = personalMessageDao.getLastPersonalMessages(participant.getId(), lastId);
        return messages.stream().map(personalMessageDtoMapper::personalMessageToPersonalMessageDto).toList();
    }

    @Transactional
    public NotificationStatusDto notificationSwitch(Long participantId, Long groupId, Long userId, Boolean enabled) {
        Participant participant = appUtilsService.checkAndReturnParticipant(userId, groupId, false, true);
        appUtilsService.checkParticipantInGroupAndReturn(participantId, groupId);
        personalMessageNotificationRepository.deleteAllByParticipantIdAndAndSenderId(participant.getId(), participantId);
        if (Boolean.FALSE.equals(enabled)) {
            PersonalMessageNotification notification = new PersonalMessageNotification();
            notification.setParticipantId(participant.getId());
            notification.setSenderId(participantId);
            personalMessageNotificationRepository.save(notification);
            return new NotificationStatusDto(false);
        }
        return new NotificationStatusDto(true);
    }

    @Transactional(readOnly = true)
    public NotificationStatusDto getNotificationStatus(Long participantId, Long groupId, Long userId) {
        Participant participant = appUtilsService.checkAndReturnParticipant(userId, groupId, false, true);
        if (personalMessageNotificationRepository
                .findByParticipantIdAndAndSenderId(participant.getId(), participantId).isPresent()) {
            return new NotificationStatusDto(false);
        }
        return new NotificationStatusDto(true);
    }

    private void notifyUsersAboutPersonalMessage(PersonalMessageDto personalMessageDto) {
        notifyOnlineUsersAboutPersonalMessage(personalMessageDto);
        cloudNotifyUsersAboutPersonalMessage(personalMessageDto);
    }

    private void notifyOnlineUsersAboutPersonalMessage(PersonalMessageDto personalMessageDto) {
        List<Long> users = new ArrayList<>();
        Optional<Participant> sender = participantRepository.findById(personalMessageDto.getSenderId());
        Optional<Participant> recipient = participantRepository.findById(personalMessageDto.getRecipientId());
        sender.ifPresent(participant -> users.add(participant.getUserId()));
        if (recipient.isPresent() && recipient.get().getUserId() != null) {
            users.add(recipient.get().getUserId());
        }
        messagesService.sendMessage(Channels.NOTIFY_ONLINE, MessagesTypes.PERSONAL_MESSAGE, Operation.CREATE,
                personalMessageDto, users);
    }

    private void cloudNotifyUsersAboutPersonalMessage(PersonalMessageDto personalMessageDto) {
        if (personalMessageNotificationRepository
                .findByParticipantIdAndAndSenderId(personalMessageDto.getRecipientId(),
                        personalMessageDto.getSenderId()).isEmpty()) {
            Optional<Participant> participant = participantRepository
                    .findById(personalMessageDto.getRecipientId());
            if (participant.isPresent() && participant.get().getUserId() != null) {
                messagesService.sendMessage(Channels.CLOUD_MESSAGE, MessagesTypes.PERSONAL_MESSAGE, Operation.CREATE,
                        personalMessageDto, List.of(participant.get().getUserId()));
            }
        }
    }
}
