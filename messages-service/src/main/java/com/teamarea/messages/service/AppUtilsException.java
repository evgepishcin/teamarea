package com.teamarea.messages.service;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class AppUtilsException extends RuntimeException {
    private final HttpStatus httpStatus;

    public AppUtilsException(String message) {
        super(message);
        httpStatus = HttpStatus.FORBIDDEN;
    }

    public AppUtilsException(String message, HttpStatus httpStatus) {
        super(message);
        this.httpStatus = httpStatus;
    }
}
