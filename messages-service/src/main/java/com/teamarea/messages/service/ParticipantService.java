package com.teamarea.messages.service;

import com.teamarea.messages.dao.ParticipantDao;
import com.teamarea.messages.domain.entity.Participant;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class ParticipantService {

    private final ParticipantDao participantDao;

    @Transactional
    public void createParticipant(Participant participant) {
        participantDao.saveParticipant(participant);
    }

    @Transactional
    public void updateParticipant(Participant participant) {
        participantDao.saveParticipant(participant);
    }

    @Transactional
    public void deleteParticipant(Long participantId) {
        participantDao.disableParticipant(participantId);
    }

    @Transactional
    public void disableAllGroupParticipants(Long groupId) {
        participantDao.disableAllGroupParticipants(groupId);
    }
}
