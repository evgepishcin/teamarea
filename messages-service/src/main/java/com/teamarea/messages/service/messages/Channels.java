package com.teamarea.messages.service.messages;

import lombok.experimental.UtilityClass;

/**
 * Канал для отправки сообщений в брокер сообщений
 */
@UtilityClass
public class Channels {
    public final String GROUP = "group";
    public final String ACCOUNT = "account";
    public final String NOTIFY_ONLINE = "notify";
    public final String CLOUD_MESSAGE = "cloud-message";
    public final String MAIL = "mail";
}
