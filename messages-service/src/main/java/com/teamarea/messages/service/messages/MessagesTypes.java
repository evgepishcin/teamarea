package com.teamarea.messages.service.messages;

import lombok.experimental.UtilityClass;

@UtilityClass
public class MessagesTypes {
    public final String CHAT_MESSAGE = "CHAT_MESSAGE";
    public final String CHAT_MESSAGE_LIST = "CHAT_MESSAGE_LIST";
    public final String CHANNEL_MESSAGE = "CHANNEL_MESSAGE";
    public final String PERSONAL_MESSAGE = "PERSONAL_MESSAGE";
    public final String PERSONAL_MESSAGE_LIST = "PERSONAL_MESSAGE_LIST";
}
