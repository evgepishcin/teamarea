package com.teamarea.messages.service.messages.impl;

import com.teamarea.common.data.api.Operation;
import com.teamarea.group.api.dto.GroupMessageTypeApi;
import com.teamarea.group.api.dto.ParticipantDtoApi;
import com.teamarea.messages.domain.entity.Participant;
import com.teamarea.messages.domain.mapper.ParticipantDtoApiMapper;
import com.teamarea.messages.service.ParticipantService;
import com.teamarea.messages.service.messages.MessageWorker;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ParticipantMessageWorker implements MessageWorker<ParticipantDtoApi> {

    private final ParticipantDtoApiMapper participantDtoApiMapper;
    private final ParticipantService participantService;

    @Override
    public String getMessageType() {
        return GroupMessageTypeApi.PARTICIPANT;
    }

    @Override
    public void accept(ParticipantDtoApi participantDtoApi, String operation) {
        Participant participant = participantDtoApiMapper.participantDtoApiToParticipant(participantDtoApi);
        if (Operation.CREATE.equals(operation)) {
            participantService.createParticipant(participant);
        } else if (Operation.UPDATE.equals(operation)) {
            participantService.updateParticipant(participant);
        } else if (Operation.DELETE.equals(operation)) {
            participantService.deleteParticipant(participant.getId());
        }
    }

    @Override
    public Class<?> getMessageClass() {
        return ParticipantDtoApi.class;
    }
}
