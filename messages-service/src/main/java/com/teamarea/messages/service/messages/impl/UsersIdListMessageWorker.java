package com.teamarea.messages.service.messages.impl;

import com.teamarea.account.api.dto.AccountMessageType;
import com.teamarea.account.api.dto.UsersIdsList;
import com.teamarea.common.data.api.Operation;
import com.teamarea.messages.dao.ParticipantDao;
import com.teamarea.messages.service.messages.MessageWorker;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UsersIdListMessageWorker implements MessageWorker<UsersIdsList> {

    private final ParticipantDao participantDao;

    @Override
    public String getMessageType() {
        return AccountMessageType.USERS_ID_LIST;
    }

    @Override
    public void accept(UsersIdsList usersIdsList, String operation) {
        if (Operation.DELETE.equals(operation)) {
            if (!usersIdsList.getUsersIds().isEmpty()) {
                participantDao.removeUsers(usersIdsList.getUsersIds());
            }
        }
    }

    @Override
    public Class<?> getMessageClass() {
        return UsersIdsList.class;
    }
}
