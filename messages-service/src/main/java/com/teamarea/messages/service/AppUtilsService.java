package com.teamarea.messages.service;

import com.teamarea.messages.domain.entity.*;
import com.teamarea.messages.repository.*;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AppUtilsService {

    private final ParticipantRepository participantRepository;
    private final RoleRepository roleRepository;
    private final ChatRepository chatRepository;
    private final ChatRoleRepository chatRoleRepository;
    private final ChatParticipantRepository chatParticipantRepository;
    private final ChannelRepository channelRepository;
    private final ChannelRoleRepository channelRoleRepository;
    private final ChannelParticipantRepository channelParticipantRepository;

    /**
     * Получение участника группы по номеру группы и идентификатору пользователя
     * с проверкой на права модетаора и статус активного профиля
     *
     * @param userId         идентификатор пользователя
     * @param groupId        идентификатор группы
     * @param checkModerator проверить права модератора
     * @param checkActive    проверить статус активного участника группы
     * @return усастник группы, найденный по полям userId и groupId
     * @throws AppUtilsException данное исключение выбрасывается, если не прошла хоть одна проверка
     */
    public Participant checkAndReturnParticipant(long userId, long groupId,
                                                 boolean checkModerator, boolean checkActive)
            throws AppUtilsException {
        Optional<Participant> optionalParticipant = participantRepository.findByUserIdAndGroupId(userId, groupId);
        Participant participant = optionalParticipant.orElseThrow(() ->
                new AppUtilsException("This user is not " +
                        "a member of the specified group!", HttpStatus.FORBIDDEN));
        checkModeratorAndActive(participant, checkModerator, checkActive);
        return participant;
    }

    public List<Role> checkRolesInGroupAndReturn(List<Long> ids, Long groupId) throws AppUtilsException {
        List<Role> roles = roleRepository.findAllByIdInAndGroupIdEquals(ids, groupId);
        if (ids.size() != roles.size()) {
            throw new AppUtilsException("Some roles do not belong to the specified group");
        }
        return roles;
    }

    public Participant checkParticipantInGroupAndReturn(Long participantId, Long groupId) {
        Optional<Participant> participant = participantRepository
                .findByIdAndGroupIdAndActiveIsTrue(participantId, groupId);
        return participant.orElseThrow(()
                -> new AppUtilsException("Participants do not belong to the specified group"));
    }

    public List<Participant> checkParticipantsInGroupAndReturn(List<Long> ids, Long groupId) throws AppUtilsException {
        List<Participant> participants = participantRepository.findAllByIdInAndGroupIdEqualsAndActive(ids, groupId);
        if (ids.size() != participants.size()) {
            throw new AppUtilsException("Some participants do not belong to the specified group");
        }
        return participants;
    }

    public Chat checkChatInGroupAndReturn(Long chatId, Long groupId) throws AppUtilsException {
        Chat chat = chatRepository.findById(chatId).orElseThrow(() ->
                new AppUtilsException("Chat does not belong to this group"));
        if (!Objects.equals(chat.getGroupId(), groupId)) {
            throw new AppUtilsException("Chat does not belong to this group");
        }
        return chat;
    }

    public Channel checkChannelInGroupAndReturn(Long channelId, Long groupId) throws AppUtilsException {
        Channel channel = channelRepository.findById(channelId).orElseThrow(() ->
                new AppUtilsException("Channel does not belong to this group"));
        if (!Objects.equals(channel.getGroupId(), groupId)) {
            throw new AppUtilsException("Channel does not belong to this group");
        }
        return channel;
    }

    public void checkParticipantAssociatedWithChat(Long participantId, Long chatId) throws AppUtilsException {
        Participant participant = participantRepository.findById(participantId).orElseThrow(() ->
                new AppUtilsException("Participant with this id does not exist"));
        if (participant.getRoleId() != null) {
            Optional<ChatRole> chatRole = chatRoleRepository.findByRoleIdAndChatId(participant.getRoleId(), chatId);
            if (chatRole.isPresent()) {
                return;
            }
        }
        Optional<ChatParticipant> chatParticipant =
                chatParticipantRepository.findByParticipantIdAndChatId(participantId, chatId);
        if (chatParticipant.isEmpty()) {
            throw new AppUtilsException("The participant is not associated with this chat");
        }
    }

    public void checkParticipantAssociatedWithChannel(Long participantId, Long channelId, boolean checkCanWriteToChannel)
            throws AppUtilsException {
        Participant participant = participantRepository.findById(participantId).orElseThrow(() ->
                new AppUtilsException("Participant with this id does not exist"));
        if (participant.getRoleId() != null) {
            Optional<ChannelRole> channelRole =
                    channelRoleRepository.findAllByRoleIdAndChannelId(participant.getRoleId(), channelId);
            if (channelRole.isPresent()) {
                if (checkCanWriteToChannel) {
                    if (Boolean.TRUE.equals(channelRole.get().getWriteAccess())) {
                        return;
                    }
                } else {
                    return;
                }
            }
        }
        Optional<ChannelParticipant> channelParticipant =
                channelParticipantRepository.findAllByParticipantIdAndChannelId(participantId, channelId);
        if (channelParticipant.isEmpty()) {
            throw new AppUtilsException("The participant is not associated with this channel");
        } else {
            if (checkCanWriteToChannel && Boolean.FALSE.equals(channelParticipant.get().getWriteAccess())) {
                throw new AppUtilsException("The participant is not associated with this channel or does not" +
                        "have write access");

            }
        }
    }

    private void checkModeratorAndActive(Participant participant, boolean checkModerator, boolean checkActive) {
        if (checkModerator && Boolean.FALSE.equals(participant.getModerator())) {
            throw new AppUtilsException("This participant does not have " +
                    "moderator rights for this action", HttpStatus.FORBIDDEN);
        }
        if (checkActive && Boolean.FALSE.equals(participant.getActive())) {
            throw new AppUtilsException("This participant was disabled!", HttpStatus.FORBIDDEN);
        }
    }
}
