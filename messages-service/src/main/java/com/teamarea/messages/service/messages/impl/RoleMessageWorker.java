package com.teamarea.messages.service.messages.impl;

import com.teamarea.common.data.api.Operation;
import com.teamarea.group.api.dto.GroupMessageTypeApi;
import com.teamarea.group.api.dto.RoleDtoApi;
import com.teamarea.messages.domain.entity.Role;
import com.teamarea.messages.domain.mapper.RoleDtoApiMapper;
import com.teamarea.messages.service.RoleService;
import com.teamarea.messages.service.messages.MessageWorker;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class RoleMessageWorker implements MessageWorker<RoleDtoApi> {

    private final RoleDtoApiMapper roleDtoApiMapper;
    private final RoleService roleService;

    @Override
    public String getMessageType() {
        return GroupMessageTypeApi.ROLE;
    }

    @Override
    public void accept(RoleDtoApi roleDtoApi, String operation) {
        Role role = roleDtoApiMapper.roleDtoApiToRole(roleDtoApi);
        if (Operation.CREATE.equals(operation)) {
            roleService.createRole(role);
        } else if (Operation.UPDATE.equals(operation)) {
            roleService.updateRole(role);
        } else if (Operation.DELETE.equals(operation)) {
            roleService.deleteRole(role.getId());
        }
    }

    @Override
    public Class<?> getMessageClass() {
        return RoleDtoApi.class;
    }
}
