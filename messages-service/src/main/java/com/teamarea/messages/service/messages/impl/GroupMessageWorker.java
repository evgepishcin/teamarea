package com.teamarea.messages.service.messages.impl;

import com.teamarea.common.data.api.Operation;
import com.teamarea.group.api.dto.GroupDtoApi;
import com.teamarea.group.api.dto.GroupMessageTypeApi;
import com.teamarea.messages.service.ParticipantService;
import com.teamarea.messages.service.messages.MessageWorker;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class GroupMessageWorker implements MessageWorker<GroupDtoApi> {

    private final ParticipantService participantService;

    @Override
    public String getMessageType() {
        return GroupMessageTypeApi.GROUP;
    }

    @Override
    public void accept(GroupDtoApi groupDtoApi, String operation) {
        if (Operation.DELETE.equals(operation)) {
            participantService.disableAllGroupParticipants(groupDtoApi.getId());
        }
    }

    @Override
    public Class<?> getMessageClass() {
        return GroupDtoApi.class;
    }
}
