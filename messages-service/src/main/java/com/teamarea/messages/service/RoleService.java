package com.teamarea.messages.service;

import com.teamarea.messages.dao.RoleDao;
import com.teamarea.messages.domain.entity.Role;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class RoleService {
    private final RoleDao roleDao;

    @Transactional
    public void createRole(Role role) {
        roleDao.saveRole(role);
    }

    @Transactional
    public void updateRole(Role role) {
        roleDao.saveRole(role);
    }

    @Transactional
    public void deleteRole(Long roleId) {
        roleDao.deleteRole(roleId);
    }
}
