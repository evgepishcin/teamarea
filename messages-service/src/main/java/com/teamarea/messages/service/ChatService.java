package com.teamarea.messages.service;

import com.teamarea.common.data.api.Operation;
import com.teamarea.messages.dao.ChatDao;
import com.teamarea.messages.dao.ChatMessageDao;
import com.teamarea.messages.domain.dto.*;
import com.teamarea.messages.domain.entity.Chat;
import com.teamarea.messages.domain.entity.ChatMessage;
import com.teamarea.messages.domain.entity.ChatNotification;
import com.teamarea.messages.domain.entity.Participant;
import com.teamarea.messages.domain.mapper.ChatDtoMapper;
import com.teamarea.messages.domain.mapper.ChatInfoDtoMapper;
import com.teamarea.messages.domain.mapper.ChatMessageDtoMapper;
import com.teamarea.messages.domain.mapper.NewChatDtoMapper;
import com.teamarea.messages.repository.ChatNotificationRepository;
import com.teamarea.messages.repository.ChatParticipantRepository;
import com.teamarea.messages.repository.ChatRepository;
import com.teamarea.messages.repository.ChatRoleRepository;
import com.teamarea.messages.service.messages.Channels;
import com.teamarea.messages.service.messages.MessagesService;
import com.teamarea.messages.service.messages.MessagesTypes;
import lombok.RequiredArgsConstructor;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ChatService {

    private final AppUtilsService appUtilsService;
    private final NewChatDtoMapper newChatDtoMapper;
    private final ChatDtoMapper chatDtoMapper;
    private final ChatInfoDtoMapper chatInfoDtoMapper;
    private final ChatMessageDtoMapper chatMessageDtoMapper;
    private final ChatDao chatDao;
    private final ChatMessageDao chatMessageDao;
    private final ChatRepository chatRepository;
    private final ChatNotificationRepository chatNotificationRepository;
    private final ChatRoleRepository chatRoleRepository;
    private final ChatParticipantRepository chatParticipantRepository;
    private final MessagesService messagesService;

    @Transactional
    public ChatDto createChat(NewChatDto newChatDto, Long groupId, Long userId) {
        appUtilsService.checkAndReturnParticipant(userId, groupId, true, true);
        appUtilsService.checkRolesInGroupAndReturn(newChatDto.getRoles(), groupId);
        appUtilsService.checkParticipantsInGroupAndReturn(newChatDto.getParticipants(), groupId);
        Chat chat = newChatDtoMapper.newChatDtoToChat(newChatDto);
        chat.setGroupId(groupId);
        chat = chatDao.saveChat(chat, newChatDto.getRoles(), newChatDto.getParticipants());
        return chatDtoMapper.chatToChatDto(chat);
    }

    @Transactional(readOnly = true)
    public ChatInfoDto getChat(Long chatId, Long groupId, Long userId) {
        Participant participant = appUtilsService.checkAndReturnParticipant(userId, groupId, false, true);
        Chat chat = appUtilsService.checkChatInGroupAndReturn(chatId, groupId);
        if (Boolean.FALSE.equals(participant.getModerator())) {
            appUtilsService.checkParticipantAssociatedWithChat(participant.getId(), chatId);
        }
        ChatInfoDto chatInfoDto = chatInfoDtoMapper.chatToChatInfoDto(chat);
        chatInfoDto.setRoles(chatRoleRepository.selectAllRolesIdsByChatId(chatId));
        chatInfoDto.setParticipants(chatParticipantRepository.selectAllParticipantsIdsByChatId(chatId));
        chatInfoDto.setNotifications(chatNotificationRepository.findByParticipantIdAndChatId(participant.getId(),
                chatId).isEmpty());
        return chatInfoDto;
    }

    @Transactional
    public ChatDto updateChat(NewChatDto newChatDto, Long chatId, Long groupId, Long userId) {
        appUtilsService.checkAndReturnParticipant(userId, groupId, true, true);
        appUtilsService.checkRolesInGroupAndReturn(newChatDto.getRoles(), groupId);
        appUtilsService.checkParticipantsInGroupAndReturn(newChatDto.getParticipants(), groupId);
        appUtilsService.checkChatInGroupAndReturn(chatId, groupId);
        Chat chat = newChatDtoMapper.newChatDtoToChat(newChatDto);
        chat.setId(chatId);
        chat.setGroupId(groupId);
        chat = chatDao.saveChat(chat, newChatDto.getRoles(), newChatDto.getParticipants());
        return chatDtoMapper.chatToChatDto(chat);
    }

    @Transactional
    public void deleteChat(Long chatId, Long groupId, Long userId) {
        appUtilsService.checkAndReturnParticipant(userId, groupId, true, true);
        appUtilsService.checkChatInGroupAndReturn(chatId, groupId);
        chatDao.deleteChat(chatId);
    }

    @Transactional(readOnly = true)
    public List<ChatDto> getGroupChats(Long groupId, Long userId) {
        appUtilsService.checkAndReturnParticipant(userId, groupId, true, true);
        List<Chat> chats = chatRepository.findAllByGroupId(groupId);
        return chats.stream().map(chatDtoMapper::chatToChatDto).toList();
    }

    @Transactional(readOnly = true)
    public List<ChatDto> getParticipantChats(Long groupId, Long userId) {
        Participant participant = appUtilsService.checkAndReturnParticipant(userId, groupId, false, true);
        List<Chat> chats = new ArrayList<>();
        if (participant.getRoleId() != null) {
            chats.addAll(chatRepository.findAllByRoleId(participant.getRoleId()));
        }
        chats.addAll(chatRepository.findAllByParticipantId(participant.getId()));
        return chats.stream().map(chatDtoMapper::chatToChatDto).toList();
    }

    @Transactional
    public ChatMessageDto sendChatMessage(NewMessageDto newMessageDto, Long chatId, Long groupId, Long userId) {
        Participant participant = appUtilsService.checkAndReturnParticipant(userId, groupId, false, true);
        appUtilsService.checkParticipantAssociatedWithChat(participant.getId(), chatId);
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setChatId(chatId);
        chatMessage.setSenderId(participant.getId());
        chatMessage.setText(newMessageDto.getText());
        chatMessage.setCreateDate(new Date());
        chatMessage = chatMessageDao.saveChatMessage(chatMessage);
        ChatMessageDto chatMessageDto = chatMessageDtoMapper.chatToChatMessageDto(chatMessage);
        notifyUsersAboutChatMessage(chatMessageDto, userId);
        return chatMessageDto;
    }

    @Transactional
    public List<ChatMessageDto> getChatMessages(Long chatId, Long lastId, Long groupId, Long userId) {
        Participant participant = appUtilsService.checkAndReturnParticipant(userId, groupId, false, true);
        appUtilsService.checkParticipantAssociatedWithChat(participant.getId(), chatId);
        Pair<List<ChatMessage>, List<ChatMessage>> selectedMessages =
                chatMessageDao.getChatMessages(chatId, participant.getId(), lastId);
        List<ChatMessage> messages = selectedMessages.getFirst();
        List<ChatMessageDto> readiedMessagesDto = selectedMessages.getSecond().stream()
                .map(chatMessageDtoMapper::chatToChatMessageDto).toList();
        if (!readiedMessagesDto.isEmpty()) {
            List<Long> chatUsers = getChatUsers(chatId);
            messagesService.sendMessage(Channels.NOTIFY_ONLINE, MessagesTypes.CHAT_MESSAGE_LIST, Operation.UPDATE,
                    readiedMessagesDto, chatUsers);
        }
        return messages.stream().map(chatMessageDtoMapper::chatToChatMessageDto).toList();
    }

    @Transactional
    public NotificationStatusDto notificationSwitch(Long chatId, Long groupId, Long userId, Boolean enabled) {
        Participant participant = appUtilsService.checkAndReturnParticipant(userId, groupId, false, true);
        appUtilsService.checkChatInGroupAndReturn(chatId, groupId);
        chatNotificationRepository.deleteAllByParticipantIdAndChatId(participant.getId(), chatId);
        if (Boolean.FALSE.equals(enabled)) {
            ChatNotification chatNotification = new ChatNotification();
            chatNotification.setChatId(chatId);
            chatNotification.setParticipantId(participant.getId());
            chatNotificationRepository.save(chatNotification);
            return new NotificationStatusDto(false);
        }
        return new NotificationStatusDto(true);
    }

    @Transactional(readOnly = true)
    public NotificationStatusDto getNotificationStatus(Long chatId, Long groupId, Long userId) {
        Participant participant = appUtilsService.checkAndReturnParticipant(userId, groupId, false, true);
        if (chatNotificationRepository.findByParticipantIdAndChatId(participant.getId(), chatId).isPresent()) {
            return new NotificationStatusDto(false);
        }
        return new NotificationStatusDto(true);
    }

    private void notifyUsersAboutChatMessage(ChatMessageDto chatMessageDto, Long userId) {
        notifyOnlineUsersAboutChatMessage(chatMessageDto);
        cloudNotifyUsersAboutChatMessage(chatMessageDto, userId);
    }

    private void notifyOnlineUsersAboutChatMessage(ChatMessageDto chatMessageDto) {
        List<Long> chatUsers = getChatUsers(chatMessageDto.getChatId());
        messagesService.sendMessage(Channels.NOTIFY_ONLINE, MessagesTypes.CHAT_MESSAGE, Operation.CREATE,
                chatMessageDto, chatUsers);
    }

    private void cloudNotifyUsersAboutChatMessage(ChatMessageDto chatMessageDto, Long senderUserId) {
        List<Long> chatUsers = getChatUsers(chatMessageDto.getChatId());
        List<Long> usersIdsNotification = new ArrayList<>(chatUsers);
        usersIdsNotification.remove(senderUserId);
        usersIdsNotification.removeAll(chatNotificationRepository.getUsersIdsByParticipantIdsIn(chatUsers));
        messagesService.sendMessage(Channels.CLOUD_MESSAGE, MessagesTypes.CHAT_MESSAGE, Operation.CREATE,
                chatMessageDto, usersIdsNotification);
    }

    private List<Long> getChatUsers(Long chatId) {
        List<Long> chatUsers = chatParticipantRepository.selectUserIdsByChatId(chatId);
        chatUsers.addAll(chatRoleRepository.selectUserIdsByChatId(chatId));
        chatUsers = chatUsers.stream().distinct().toList();
        return chatUsers;
    }
}
