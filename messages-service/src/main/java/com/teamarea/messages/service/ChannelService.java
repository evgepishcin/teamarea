package com.teamarea.messages.service;

import com.teamarea.common.data.api.Operation;
import com.teamarea.messages.dao.ChannelDao;
import com.teamarea.messages.dao.ChannelMessageDao;
import com.teamarea.messages.domain.dto.*;
import com.teamarea.messages.domain.entity.*;
import com.teamarea.messages.domain.mapper.ChannelDtoMapper;
import com.teamarea.messages.domain.mapper.ChannelInfoDtoMapper;
import com.teamarea.messages.domain.mapper.ChannelMessageDtoMapper;
import com.teamarea.messages.domain.mapper.NewChannelDtoMapper;
import com.teamarea.messages.repository.*;
import com.teamarea.messages.service.messages.Channels;
import com.teamarea.messages.service.messages.MessagesService;
import com.teamarea.messages.service.messages.MessagesTypes;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ChannelService {

    private final AppUtilsService appUtilsService;
    private final ChannelDtoMapper channelDtoMapper;
    private final ChannelInfoDtoMapper channelInfoDtoMapper;
    private final NewChannelDtoMapper newChannelDtoMapper;
    private final ChannelMessageDtoMapper channelMessageDtoMapper;
    private final ChannelDao channelDao;
    private final ChannelMessageDao channelMessageDao;
    private final ChannelRepository channelRepository;
    private final ChannelRoleRepository channelRoleRepository;
    private final ChannelParticipantRepository channelParticipantRepository;
    private final ChannelNotificationRepository channelNotificationRepository;
    private final ChannelNotificationMailRepository channelNotificationMailRepository;
    private final MessagesService messagesService;

    @Transactional
    public ChannelDto createChannel(NewChannelDto newChannelDto, Long groupId, Long userId) {
        appUtilsService.checkAndReturnParticipant(userId, groupId, true, true);
        appUtilsService.checkRolesInGroupAndReturn(newChannelDto.getRolesRead(), groupId);
        appUtilsService.checkRolesInGroupAndReturn(newChannelDto.getRolesReadWrite(), groupId);
        appUtilsService.checkParticipantsInGroupAndReturn(newChannelDto.getParticipantsRead(), groupId);
        appUtilsService.checkParticipantsInGroupAndReturn(newChannelDto.getParticipantsReadWrite(), groupId);
        Channel channel = newChannelDtoMapper.newChannelDtoToChannel(newChannelDto);
        channel.setGroupId(groupId);
        channel = channelDao.saveChannel(channel, newChannelDto.getRolesRead(), newChannelDto.getRolesReadWrite(),
                newChannelDto.getParticipantsRead(), newChannelDto.getParticipantsReadWrite());
        return channelDtoMapper.channelToChannelDto(channel);
    }

    @Transactional(readOnly = true)
    public ChannelInfoDto getChannel(Long channelId, Long groupId, Long userId) {
        Participant participant = appUtilsService.checkAndReturnParticipant(userId, groupId, false, true);
        Channel channel = appUtilsService.checkChannelInGroupAndReturn(channelId, groupId);
        if (Boolean.FALSE.equals(participant.getModerator())) {
            appUtilsService.checkParticipantAssociatedWithChannel(participant.getId(), channelId, false);
        }
        ChannelInfoDto channelInfoDto = channelInfoDtoMapper.channelToChannelInfoDto(channel);
        channelInfoDto.setRolesRead(channelRoleRepository
                .selectAllRolesIdsByChannelIdAndWriteAccess(channelId, false));
        channelInfoDto.setRolesReadWrite(channelRoleRepository
                .selectAllRolesIdsByChannelIdAndWriteAccess(channelId, true));
        channelInfoDto.setParticipantsRead(channelParticipantRepository
                .selectAllParticipantsIdsByChannelIdAndWriteAccess(channelId, false));
        channelInfoDto.setParticipantsReadWrite(channelParticipantRepository
                .selectAllParticipantsIdsByChannelIdAndWriteAccess(channelId, true));
        channelInfoDto.setNotifications(channelNotificationRepository
                .findByParticipantIdAndChannelId(participant.getId(), channelId).isEmpty());
        channelInfoDto.setEmailNotifications(channelNotificationMailRepository
                .findByParticipantIdAndChannelId(participant.getId(), channelId).isEmpty());
        return channelInfoDto;
    }

    @Transactional(readOnly = true)
    public List<ChannelDto> getGroupChannels(Long groupId, Long userId) {
        appUtilsService.checkAndReturnParticipant(userId, groupId, true, true);
        List<Channel> channels = channelRepository.findAllByGroupId(groupId);
        return channels.stream().map(channelDtoMapper::channelToChannelDto).toList();
    }

    @Transactional(readOnly = true)
    public List<ChannelDto> getParticipantChannels(Long groupId, Long userId) {
        Participant participant = appUtilsService.checkAndReturnParticipant(userId, groupId, false, true);
        List<Channel> channels = new ArrayList<>();
        if (participant.getRoleId() != null) {
            channels.addAll(channelRepository.findAllByRoleId(participant.getRoleId()));
        }
        channels.addAll(channelRepository.findAllByParticipantId(participant.getId()));
        return channels.stream().map(channelDtoMapper::channelToChannelDto).toList();
    }

    @Transactional
    public ChannelDto updateChannel(NewChannelDto newChannelDto, Long channelId, Long groupId, Long userId) {
        appUtilsService.checkAndReturnParticipant(userId, groupId, true, true);
        appUtilsService.checkRolesInGroupAndReturn(newChannelDto.getRolesRead(), groupId);
        appUtilsService.checkRolesInGroupAndReturn(newChannelDto.getRolesReadWrite(), groupId);
        appUtilsService.checkParticipantsInGroupAndReturn(newChannelDto.getParticipantsRead(), groupId);
        appUtilsService.checkParticipantsInGroupAndReturn(newChannelDto.getParticipantsReadWrite(), groupId);
        Channel channel = appUtilsService.checkChannelInGroupAndReturn(channelId, groupId);
        channel.setId(channelId);
        channel.setGroupId(groupId);
        channel = channelDao.saveChannel(channel, newChannelDto.getRolesRead(), newChannelDto.getRolesReadWrite(),
                newChannelDto.getParticipantsRead(), newChannelDto.getParticipantsReadWrite());
        return channelDtoMapper.channelToChannelDto(channel);
    }

    @Transactional
    public void deleteChannel(Long channelId, Long groupId, Long userId) {
        appUtilsService.checkAndReturnParticipant(userId, groupId, true, true);
        appUtilsService.checkChannelInGroupAndReturn(channelId, groupId);
        channelDao.deleteChannel(channelId);
    }

    @Transactional
    public ChannelMessageDto sendChannelMessage(NewMessageDto newMessageDto, Long channelId, Long groupId, Long userId) {
        Participant participant = appUtilsService.checkAndReturnParticipant(userId, groupId, false, true);
        appUtilsService.checkParticipantAssociatedWithChannel(participant.getId(), channelId, true);
        ChannelMessage channelMessage = new ChannelMessage();
        channelMessage.setChannelId(channelId);
        channelMessage.setSenderId(participant.getId());
        channelMessage.setText(newMessageDto.getText());
        channelMessage.setCreateDate(new Date());
        channelMessage = channelDao.saveChannelMessage(channelMessage);
        ChannelMessageDto channelMessageDto = channelMessageDtoMapper.channelMessageToChannelMessageDto(channelMessage);
        notifyOnlineUsersAboutChannelMessage(channelMessage);
        cloudNotifyUsersAboutChannelMessage(channelMessage, participant.getUserId());
        mailNotifyUsersAboutChannelMessage(channelMessage, participant.getUserId());
        return channelMessageDto;
    }

    @Transactional(readOnly = true)
    public List<ChannelMessageDto> getChannelMessages(Long channelId, Long groupId, Long userId, Long lastId, Boolean filterMy) {
        Participant participant = appUtilsService.checkAndReturnParticipant(userId, groupId, false, true);
        appUtilsService.checkParticipantAssociatedWithChannel(participant.getId(), channelId, false);
        List<ChannelMessage> channelMessages;
        if (Boolean.TRUE.equals(filterMy)) {
            channelMessages = channelMessageDao.getParticipantChannelMessages(channelId, lastId, participant.getId());
        } else {
            channelMessages = channelMessageDao.getChannelMessages(channelId, lastId);
        }
        return channelMessages.stream().map(channelMessageDtoMapper::channelMessageToChannelMessageDto).toList();
    }

    @Transactional
    public NotificationStatusDto notificationSwitch(Long channelId, Long groupId, Long userId, Boolean enabled) {
        Participant participant = appUtilsService.checkAndReturnParticipant(userId, groupId, false, true);
        appUtilsService.checkChannelInGroupAndReturn(channelId, groupId);
        channelNotificationRepository.deleteAllByParticipantIdAndChannelId(participant.getId(), channelId);
        if (Boolean.FALSE.equals(enabled)) {
            ChannelNotification channelNotification = new ChannelNotification();
            channelNotification.setChannelId(channelId);
            channelNotification.setParticipantId(participant.getId());
            channelNotificationRepository.save(channelNotification);
            return new NotificationStatusDto(false);
        }
        return new NotificationStatusDto(true);
    }

    @Transactional
    public NotificationStatusDto notificationMailSwitch(Long channelId, Long groupId, Long userId, Boolean enabled) {
        Participant participant = appUtilsService.checkAndReturnParticipant(userId, groupId, false, true);
        appUtilsService.checkChannelInGroupAndReturn(channelId, groupId);
        channelNotificationMailRepository.deleteAllByParticipantIdAndChannelId(participant.getId(), channelId);
        if (Boolean.FALSE.equals(enabled)) {
            ChannelNotificationMail channelNotificationMail = new ChannelNotificationMail();
            channelNotificationMail.setChannelId(channelId);
            channelNotificationMail.setParticipantId(participant.getId());
            channelNotificationMailRepository.save(channelNotificationMail);
            return new NotificationStatusDto(false);
        }
        return new NotificationStatusDto(true);
    }

    @Transactional(readOnly = true)
    public NotificationStatusDto getNotificationStatus(Long channelId, Long groupId, Long userId) {
        Participant participant = appUtilsService.checkAndReturnParticipant(userId, groupId, false, true);
        if (channelNotificationRepository.findByParticipantIdAndChannelId(participant.getId(), channelId).isPresent()) {
            return new NotificationStatusDto(false);
        }
        return new NotificationStatusDto(true);
    }

    @Transactional(readOnly = true)
    public NotificationStatusDto getNotificationMailStatus(Long channelId, Long groupId, Long userId) {
        Participant participant = appUtilsService.checkAndReturnParticipant(userId, groupId, false, true);
        if (channelNotificationMailRepository.findByParticipantIdAndChannelId(participant.getId(), channelId).isPresent()) {
            return new NotificationStatusDto(false);
        }
        return new NotificationStatusDto(true);
    }


    private void notifyOnlineUsersAboutChannelMessage(ChannelMessage channelMessage) {
        List<Long> channelUsers = getChannelUsers(channelMessage.getChannelId());
        ChannelMessageDto dto = channelMessageDtoMapper.channelMessageToChannelMessageDto(channelMessage);
        messagesService.sendMessage(Channels.NOTIFY_ONLINE, MessagesTypes.CHANNEL_MESSAGE,
                Operation.CREATE, dto, channelUsers);
    }

    private void cloudNotifyUsersAboutChannelMessage(ChannelMessage channelMessage, Long senderUserId) {
        List<Long> channelUsers = getChannelUsersOnlyReadAccess(channelMessage.getChannelId());
        List<Long> usersIdsNotification = new ArrayList<>(channelUsers);
        usersIdsNotification.remove(senderUserId);
        usersIdsNotification.removeAll(channelNotificationRepository.getUsersIdsByParticipantIdsIn(channelUsers));
        ChannelMessageDto dto = channelMessageDtoMapper.channelMessageToChannelMessageDto(channelMessage);
        messagesService.sendMessage(Channels.CLOUD_MESSAGE, MessagesTypes.CHANNEL_MESSAGE,
                Operation.CREATE, dto, usersIdsNotification);
    }

    private void mailNotifyUsersAboutChannelMessage(ChannelMessage channelMessage, Long senderUserId) {
        List<Long> channelUsers = getChannelUsersOnlyReadAccess(channelMessage.getChannelId());
        List<Long> usersIdsNotification = new ArrayList<>(channelUsers);
        usersIdsNotification.remove(senderUserId);
        usersIdsNotification.removeAll(channelNotificationRepository.getUsersIdsByParticipantIdsIn(channelUsers));
        com.teamarea.mail.api.dto.ChannelMessageDto dto = new com.teamarea.mail.api.dto.ChannelMessageDto();
        dto.setText(channelMessage.getText());
        dto.setCreateDate(channelMessage.getCreateDate());
        dto.setUsersIds(usersIdsNotification);
        dto.setChannelName(channelRepository.findById(channelMessage.getChannelId())
                .map(Channel::getName).orElse(""));
        messagesService.sendMessage(Channels.MAIL, MessagesTypes.CHANNEL_MESSAGE,
                Operation.CREATE, dto);
    }

    private List<Long> getChannelUsers(Long channelId) {
        List<Long> channelUsers = channelParticipantRepository.selectUserIdsByChannelId(channelId);
        channelUsers.addAll(channelRoleRepository.selectUserIdsByChannelId(channelId));
        channelUsers = channelUsers.stream().distinct().toList();
        return channelUsers;
    }

    private List<Long> getChannelUsersOnlyReadAccess(Long channelId) {
        List<Long> channelUsers = channelParticipantRepository.selectUserIdsByChannelIdAndOnlyReadAccess(channelId);
        channelUsers.addAll(channelRoleRepository.selectUserIdsByChannelIdOnlyReadAccess(channelId));
        channelUsers = channelUsers.stream().distinct().toList();
        return channelUsers;
    }
}
