package com.teamarea.messages.service.messages;

public class MessagesListenerServiceException extends RuntimeException {
    public MessagesListenerServiceException(String message) {
        super(message);
    }
}
