package com.teamarea.messages.dao;

import com.teamarea.messages.domain.entity.ChannelMessage;
import com.teamarea.messages.repository.ChannelMessageRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;

@Component
@RequiredArgsConstructor
public class ChannelMessageDao {

    private static final Integer MESSAGES_COUNT = 20;
    private final ChannelMessageRepository channelMessageRepository;

    @Transactional(readOnly = true)
    public List<ChannelMessage> getChannelMessages(Long channelId, Long lastId) {
        List<ChannelMessage> messages;
        if (lastId == null) {
            messages = channelMessageRepository.findLastMessages(channelId, PageRequest.of(0, MESSAGES_COUNT));
        } else {
            messages = channelMessageRepository.findLastMessages(channelId, lastId,
                    PageRequest.of(0, MESSAGES_COUNT));
        }
        Collections.reverse(messages);
        return messages;
    }

    @Transactional(readOnly = true)
    public List<ChannelMessage> getParticipantChannelMessages(Long channelId, Long lastId, Long participantId) {
        List<ChannelMessage> messages;
        if (lastId == null) {
            messages = channelMessageRepository
                    .findLastParticipantMessages(channelId, participantId, PageRequest.of(0, MESSAGES_COUNT));
        } else {
            messages = channelMessageRepository
                    .findLastParticipantMessages(channelId, participantId, lastId, PageRequest.of(0, MESSAGES_COUNT));
        }
        Collections.reverse(messages);
        return messages;
    }
}
