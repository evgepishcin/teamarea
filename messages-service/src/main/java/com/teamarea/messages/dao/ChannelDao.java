package com.teamarea.messages.dao;

import com.teamarea.messages.domain.entity.Channel;
import com.teamarea.messages.domain.entity.ChannelMessage;
import com.teamarea.messages.domain.entity.ChannelParticipant;
import com.teamarea.messages.domain.entity.ChannelRole;
import com.teamarea.messages.repository.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
@RequiredArgsConstructor
public class ChannelDao {

    private final ChannelRepository channelRepository;
    private final ChannelRoleRepository channelRoleRepository;
    private final ChannelParticipantRepository channelParticipantRepository;
    private final ChannelMessageRepository channelMessageRepository;
    private final ChannelNotificationRepository channelNotificationRepository;
    private final ChannelNotificationMailRepository channelNotificationMailRepository;

    @Transactional
    public Channel saveChannel(Channel channel, List<Long> rolesRead, List<Long> rolesReadWrite,
                               List<Long> participantsRead, List<Long> participantsReadWrite) {
        Channel returnedChannel = channelRepository.saveAndFlush(channel);
        channelRoleRepository.deleteAllByChannelId(returnedChannel.getId());
        channelParticipantRepository.deleteAllByChannelId(returnedChannel.getId());
        if (!rolesRead.isEmpty()) {
            List<ChannelRole> channelRoles = rolesRead.stream()
                    .map(role -> new ChannelRole(channel.getId(), role, false)).toList();
            channelRoleRepository.saveAll(channelRoles);
        }
        if (!rolesReadWrite.isEmpty()) {
            List<ChannelRole> channelRoles = rolesReadWrite.stream()
                    .map(role -> new ChannelRole(channel.getId(), role, true)).toList();
            channelRoleRepository.saveAll(channelRoles);
        }
        if (!participantsRead.isEmpty()) {
            List<ChannelParticipant> channelParticipants = participantsRead.stream()
                    .map(participant -> new ChannelParticipant(channel.getId(), participant, false))
                    .toList();
            channelParticipantRepository.saveAll(channelParticipants);
        }
        if (!participantsReadWrite.isEmpty()) {
            List<ChannelParticipant> channelParticipants = participantsReadWrite.stream()
                    .map(participant -> new ChannelParticipant(channel.getId(), participant, true))
                    .toList();
            channelParticipantRepository.saveAll(channelParticipants);
        }
        return returnedChannel;
    }

    @Transactional
    public void deleteChannel(Long channelId) {
        Channel channel = channelRepository.findById(channelId).orElse(null);
        if (channel == null) {
            return;
        }
        channelNotificationMailRepository.deleteAllByChannelId(channelId);
        channelNotificationRepository.deleteAllByChannelId(channelId);
        channelMessageRepository.deleteAllByChannelId(channelId);
        channelParticipantRepository.deleteAllByChannelId(channelId);
        channelRoleRepository.deleteAllByChannelId(channelId);
        channelRepository.deleteById(channelId);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public ChannelMessage saveChannelMessage(ChannelMessage channelMessage) {
        return channelMessageRepository.saveAndFlush(channelMessage);
    }
}
