package com.teamarea.messages.dao;

import com.teamarea.messages.domain.entity.Participant;
import com.teamarea.messages.repository.ParticipantRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
@RequiredArgsConstructor
public class ParticipantDao {

    private final ParticipantRepository participantRepository;

    @Transactional
    public Participant saveParticipant(Participant participant) {
        return participantRepository.save(participant);
    }

    @Transactional
    public void disableParticipant(Long participantId) {
        Participant participant = participantRepository.findById(participantId).orElse(null);
        if (participant == null) {
            return;
        }
        participant.setActive(false);
        participant.setUserId(null);
        participantRepository.save(participant);
    }

    @Transactional
    public void disableAllGroupParticipants(Long groupId) {
        participantRepository.disableAllGroupParticipants(groupId);
    }

    @Transactional
    public void removeUsers(List<Long> usersIds) {
        participantRepository.removeUsers(usersIds);
    }
}
