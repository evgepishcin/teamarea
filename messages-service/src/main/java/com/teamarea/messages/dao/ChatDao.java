package com.teamarea.messages.dao;

import com.teamarea.messages.domain.entity.Chat;
import com.teamarea.messages.domain.entity.ChatParticipant;
import com.teamarea.messages.domain.entity.ChatRole;
import com.teamarea.messages.repository.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
@RequiredArgsConstructor
public class ChatDao {

    private final ChatRepository chatRepository;
    private final ChatRoleRepository chatRoleRepository;
    private final ChatParticipantRepository chatParticipantRepository;
    private final ChatMessageRepository chatMessageRepository;
    private final ChatNotificationRepository chatNotificationRepository;

    @Transactional
    public Chat saveChat(Chat chat, List<Long> roles, List<Long> participants) {
        Chat returnedChat = chatRepository.saveAndFlush(chat);
        chatRoleRepository.deleteAllByChatId(chat.getId());
        chatParticipantRepository.deleteAllByChatId(chat.getId());
        if (!roles.isEmpty()) {
            List<ChatRole> chatRoles = roles.stream().map(role -> new ChatRole(chat.getId(), role)).toList();
            chatRoleRepository.saveAll(chatRoles);
        }
        if (!participants.isEmpty()) {
            List<ChatParticipant> chatParticipants = participants.stream().map(participant ->
                    new ChatParticipant(chat.getId(), participant)).toList();
            chatParticipantRepository.saveAll(chatParticipants);
        }
        return returnedChat;
    }

    @Transactional
    public void deleteChat(Long chatId) {
        Chat chat = chatRepository.findById(chatId).orElse(null);
        if (chat == null) {
            return;
        }
        chatNotificationRepository.deleteAllByChatId(chatId);
        chatMessageRepository.deleteAllByChatId(chatId);
        chatParticipantRepository.deleteAllByChatId(chatId);
        chatRoleRepository.deleteAllByChatId(chatId);
        chatRepository.deleteById(chatId);
    }
}
