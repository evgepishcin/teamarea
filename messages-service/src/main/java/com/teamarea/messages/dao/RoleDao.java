package com.teamarea.messages.dao;

import com.teamarea.messages.domain.entity.Role;
import com.teamarea.messages.repository.ChannelRoleRepository;
import com.teamarea.messages.repository.ChatRoleRepository;
import com.teamarea.messages.repository.ParticipantRepository;
import com.teamarea.messages.repository.RoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
public class RoleDao {

    private final RoleRepository roleRepository;
    private final ParticipantRepository participantRepository;
    private final ChatRoleRepository chatRoleRepository;
    private final ChannelRoleRepository channelRoleRepository;

    @Transactional
    public Role saveRole(Role role) {
        return roleRepository.save(role);
    }

    @Transactional
    public void deleteRole(Long roleId) {
        Role role = roleRepository.findById(roleId).orElse(null);
        if (role == null) {
            return;
        }
        channelRoleRepository.deleteAllByRoleId(roleId);
        chatRoleRepository.deleteAllByRoleId(roleId);
        participantRepository.removeRole(roleId);
        roleRepository.deleteAllById(roleId);
    }
}
