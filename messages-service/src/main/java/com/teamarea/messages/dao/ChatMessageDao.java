package com.teamarea.messages.dao;

import com.teamarea.messages.domain.entity.ChatMessage;
import com.teamarea.messages.repository.ChatMessageRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Component
@RequiredArgsConstructor
public class ChatMessageDao {

    private static final Integer MESSAGES_COUNT = 20;
    private final ChatMessageRepository chatMessageRepository;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public ChatMessage saveChatMessage(ChatMessage chatMessage) {
        return chatMessageRepository.save(chatMessage);
    }

    /**
     * Получение сообщений из чата
     *
     * @param chatId        идентификатор чата
     * @param participantId иднетнификатор пользователя системы, который запрашивает сообщения
     * @param lastId        идентификатор сообщения, до которого буду выбраны сообщения
     * @return Pair.first - выбранные сообщения, Pair.second - сообщения, которые стали прочитанным
     */
    @Transactional
    public Pair<List<ChatMessage>, List<ChatMessage>> getChatMessages(Long chatId, Long participantId, Long lastId) {
        List<ChatMessage> chatMessages;
        if (lastId == null) {
            chatMessages = chatMessageRepository.findLastMessages(chatId, PageRequest.of(0, MESSAGES_COUNT));
        } else {
            chatMessages = chatMessageRepository.findLastMessagesBeforeId(chatId, lastId,
                    PageRequest.of(0, MESSAGES_COUNT));
        }
        List<ChatMessage> readiedMessages = new ArrayList<>();
        chatMessages.forEach(message -> {
            if (message.getReadDate() == null && !message.getSenderId().equals(participantId)) {
                message.setReadDate(new Date());
                readiedMessages.add(message);
            }
        });
        chatMessageRepository.saveAll(readiedMessages);
        Collections.reverse(chatMessages);
        return Pair.of(chatMessages, readiedMessages);
    }
}
