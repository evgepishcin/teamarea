package com.teamarea.messages.dao;

import com.teamarea.messages.domain.comparator.PersonalMessageIdComparator;
import com.teamarea.messages.domain.entity.PersonalMessage;
import com.teamarea.messages.repository.PersonalMessageRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Component
@RequiredArgsConstructor
public class PersonalMessageDao {

    private static final Integer MESSAGES_COUNT = 20;

    private final PersonalMessageRepository personalMessageRepository;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public PersonalMessage savePersonalMessage(PersonalMessage personalMessage) {
        return personalMessageRepository.saveAndFlush(personalMessage);
    }

    @Transactional
    public Pair<List<PersonalMessage>, List<PersonalMessage>> getPersonalMessages(
            Long senderId, Long recipientId, Long lastId) {
        List<PersonalMessage> personalMessages;
        if (lastId == null) {
            personalMessages = personalMessageRepository
                    .findAllBySenderIdAndRecipientId(senderId, recipientId, PageRequest.of(0, MESSAGES_COUNT));
            personalMessages.addAll(personalMessageRepository
                    .findAllBySenderIdAndRecipientId(recipientId, senderId, PageRequest.of(0, MESSAGES_COUNT)));
        } else {
            personalMessages = personalMessageRepository
                    .findAllBySenderIdAndRecipientId(senderId, recipientId,
                            lastId, PageRequest.of(0, MESSAGES_COUNT));
            personalMessages.addAll(personalMessageRepository
                    .findAllBySenderIdAndRecipientId(recipientId, senderId, lastId,
                            PageRequest.of(0, MESSAGES_COUNT)));
        }
        personalMessages.sort(new PersonalMessageIdComparator());
        if (personalMessages.size() > MESSAGES_COUNT) {
            personalMessages = personalMessages.subList(0, MESSAGES_COUNT);
        }
        List<PersonalMessage> readiedMessages = new ArrayList<>();
        personalMessages.forEach(message -> {
            if (message.getReadDate() == null && !message.getSenderId().equals(senderId)) {
                message.setReadDate(new Date());
                readiedMessages.add(message);
            }
        });
        personalMessageRepository.saveAll(readiedMessages);
        Collections.reverse(personalMessages);
        return Pair.of(personalMessages, readiedMessages);
    }

    @Transactional(readOnly = true)
    public List<PersonalMessage> getLastPersonalMessages(Long participantId, Long lastId) {
        List<PersonalMessage> messages;
        if (lastId == null) {
            messages = personalMessageRepository
                    .findLastPersonalMessages(participantId, MESSAGES_COUNT.longValue());
        } else {
            messages = personalMessageRepository
                    .findLastPersonalMessages(participantId, lastId, MESSAGES_COUNT.longValue());
        }
        Collections.reverse(messages);
        return messages;
    }
}
