package com.teamarea.messages.domain.mapper;

import com.teamarea.messages.domain.dto.ChannelDto;
import com.teamarea.messages.domain.entity.Channel;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ChannelDtoMapper {
    ChannelDto channelToChannelDto(Channel source);
}
