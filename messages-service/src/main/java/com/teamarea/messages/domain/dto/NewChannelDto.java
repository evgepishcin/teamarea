package com.teamarea.messages.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class NewChannelDto {
    @NotBlank
    private String name;
    private String about;
    @NotNull
    private List<Long> rolesRead;
    @NotNull
    private List<Long> participantsRead;
    @NotNull
    private List<Long> rolesReadWrite;
    @NotNull
    private List<Long> participantsReadWrite;
}
