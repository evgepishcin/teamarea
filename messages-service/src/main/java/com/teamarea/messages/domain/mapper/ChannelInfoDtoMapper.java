package com.teamarea.messages.domain.mapper;

import com.teamarea.messages.domain.dto.ChannelInfoDto;
import com.teamarea.messages.domain.entity.Channel;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ChannelInfoDtoMapper {
    ChannelInfoDto channelToChannelInfoDto(Channel source);
}
