package com.teamarea.messages.domain.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "channel_role")
@Getter
@Setter
@NoArgsConstructor
public class ChannelRole {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "channel_role_id_gen")
    @SequenceGenerator(name = "channel_role_id_gen", sequenceName = "channel_role_id_seq", allocationSize = 1)
    private Long id;
    private Long channelId;
    private Long roleId;
    private Boolean writeAccess;

    public ChannelRole(Long channelId, Long roleId, Boolean writeAccess) {
        this.channelId = channelId;
        this.roleId = roleId;
        this.writeAccess = writeAccess;
    }
}
