package com.teamarea.messages.domain.mapper;

import com.teamarea.messages.domain.dto.ChannelMessageDto;
import com.teamarea.messages.domain.entity.ChannelMessage;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ChannelMessageDtoMapper {
    ChannelMessageDto channelMessageToChannelMessageDto(ChannelMessage source);
}
