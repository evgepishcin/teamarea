package com.teamarea.messages.domain.mapper;

import com.teamarea.messages.domain.dto.NewChatDto;
import com.teamarea.messages.domain.entity.Chat;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface NewChatDtoMapper {
    Chat newChatDtoToChat(NewChatDto source);
}
