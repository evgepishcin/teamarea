package com.teamarea.messages.domain.mapper;

import com.teamarea.messages.domain.dto.ChatDto;
import com.teamarea.messages.domain.entity.Chat;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ChatDtoMapper {
    ChatDto chatToChatDto(Chat source);
}
