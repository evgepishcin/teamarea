package com.teamarea.messages.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class NewChatDto {
    @NotBlank(message = "name is mandatory")
    private String name;
    private String about;
    @NotNull
    private List<Long> roles;
    @NotNull
    private List<Long> participants;
}
