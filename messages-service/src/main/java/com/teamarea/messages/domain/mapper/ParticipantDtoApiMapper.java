package com.teamarea.messages.domain.mapper;

import com.teamarea.group.api.dto.ParticipantDtoApi;
import com.teamarea.messages.domain.entity.Participant;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ParticipantDtoApiMapper {
    Participant participantDtoApiToParticipant(ParticipantDtoApi source);

    ParticipantDtoApi participantToParticipantDtoApi(Participant source);
}
