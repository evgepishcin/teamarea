package com.teamarea.messages.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PersonalMessageDto {
    private Long id;
    private Long senderId;
    private Long recipientId;
    private String text;
    private Date createDate;
    private Date readDate;
}
