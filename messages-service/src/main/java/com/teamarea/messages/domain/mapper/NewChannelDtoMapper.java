package com.teamarea.messages.domain.mapper;

import com.teamarea.messages.domain.dto.NewChannelDto;
import com.teamarea.messages.domain.entity.Channel;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface NewChannelDtoMapper {
    Channel newChannelDtoToChannel(NewChannelDto source);
}
