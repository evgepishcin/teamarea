package com.teamarea.messages.domain.mapper;

import com.teamarea.messages.domain.dto.PersonalMessageDto;
import com.teamarea.messages.domain.entity.PersonalMessage;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PersonalMessageDtoMapper {
    PersonalMessageDto personalMessageToPersonalMessageDto(PersonalMessage source);
}
