package com.teamarea.messages.domain.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "chat_message")
@Getter
@Setter
@NoArgsConstructor
public class ChatMessage {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "chat_message_id_gen")
    @SequenceGenerator(name = "chat_message_id_gen", sequenceName = "chat_message_id_seq", allocationSize = 1)
    private Long id;
    private Long chatId;
    private Long senderId;
    private String text;
    private Date createDate;
    private Date readDate;
}
