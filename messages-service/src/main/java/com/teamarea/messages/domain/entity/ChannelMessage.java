package com.teamarea.messages.domain.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "channel_message")
@Getter
@Setter
@NoArgsConstructor
public class ChannelMessage {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "channel_message_id_gen")
    @SequenceGenerator(name = "channel_message_id_gen", sequenceName = "channel_message_id_seq", allocationSize = 1)
    private Long id;
    private Long channelId;
    private Long senderId;
    private String text;
    private Date createDate;
}
