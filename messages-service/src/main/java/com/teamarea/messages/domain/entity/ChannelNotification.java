package com.teamarea.messages.domain.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "channel_notification")
@Getter
@Setter
@NoArgsConstructor
public class ChannelNotification {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "channel_notification_id_gen")
    @SequenceGenerator(name = "channel_notification_id_gen", sequenceName = "channel_notification_id_seq",
            allocationSize = 1)
    private Long id;
    private Long channelId;
    private Long participantId;
}
