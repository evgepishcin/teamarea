package com.teamarea.messages.domain.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "channel_participant")
@Getter
@Setter
@NoArgsConstructor
public class ChannelParticipant {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "channel_participant_id_gen")
    @SequenceGenerator(name = "channel_participant_id_gen", sequenceName = "channel_participant_id_seq", allocationSize = 1)
    private Long id;
    private Long channelId;
    private Long participantId;
    private Boolean writeAccess;

    public ChannelParticipant(Long channelId, Long participantId, Boolean writeAccess) {
        this.channelId = channelId;
        this.participantId = participantId;
        this.writeAccess = writeAccess;
    }
}
