package com.teamarea.messages.domain.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "chat_notification")
@Getter
@Setter
@NoArgsConstructor
public class ChatNotification {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "chat_notification_id_gen")
    @SequenceGenerator(name = "chat_notification_id_gen", sequenceName = "chat_notification_id_seq", allocationSize = 1)
    private Long id;
    private Long chatId;
    private Long participantId;
}
