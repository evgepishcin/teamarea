package com.teamarea.messages.domain.mapper;

import com.teamarea.messages.domain.dto.ChatInfoDto;
import com.teamarea.messages.domain.entity.Chat;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ChatInfoDtoMapper {
    ChatInfoDto chatToChatInfoDto(Chat source);
}
