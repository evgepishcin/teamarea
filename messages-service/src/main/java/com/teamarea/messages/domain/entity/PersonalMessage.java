package com.teamarea.messages.domain.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "personal_message")
@Getter
@Setter
@NoArgsConstructor
public class PersonalMessage {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "personal_message_id_gen")
    @SequenceGenerator(name = "personal_message_id_gen", sequenceName = "personal_message_id_seq", allocationSize = 1)
    private Long id;
    private Long senderId;
    private Long recipientId;
    private String text;
    private Date createDate;
    private Date readDate;
}
