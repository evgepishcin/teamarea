package com.teamarea.messages.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ChatInfoDto {
    private Long id;
    private Long groupId;
    private String name;
    private String about;
    private List<Long> roles;
    private List<Long> participants;
    private Boolean notifications;
}
