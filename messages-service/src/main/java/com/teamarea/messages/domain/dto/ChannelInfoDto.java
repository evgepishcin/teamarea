package com.teamarea.messages.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ChannelInfoDto {
    private Long id;
    private Long groupId;
    private String name;
    private String about;
    private List<Long> rolesRead;
    private List<Long> participantsRead;
    private List<Long> rolesReadWrite;
    private List<Long> participantsReadWrite;
    private Boolean notifications;
    private Boolean emailNotifications;
}
