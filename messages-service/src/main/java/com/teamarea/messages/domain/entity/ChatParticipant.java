package com.teamarea.messages.domain.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "chat_participant")
@Getter
@Setter
@NoArgsConstructor
public class ChatParticipant {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "chat_participant_id_gen")
    @SequenceGenerator(name = "chat_participant_id_gen", sequenceName = "chat_participant_id_seq", allocationSize = 1)
    private Long id;
    private Long chatId;
    private Long participantId;

    public ChatParticipant(Long chatId, Long participantId) {
        this.chatId = chatId;
        this.participantId = participantId;
    }
}
