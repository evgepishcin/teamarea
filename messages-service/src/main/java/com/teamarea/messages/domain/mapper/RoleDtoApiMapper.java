package com.teamarea.messages.domain.mapper;

import com.teamarea.group.api.dto.RoleDtoApi;
import com.teamarea.messages.domain.entity.Role;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface RoleDtoApiMapper {
    Role roleDtoApiToRole(RoleDtoApi source);
}
