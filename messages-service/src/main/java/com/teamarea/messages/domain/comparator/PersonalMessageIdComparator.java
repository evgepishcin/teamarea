package com.teamarea.messages.domain.comparator;

import com.teamarea.messages.domain.entity.PersonalMessage;

import java.util.Comparator;

public class PersonalMessageIdComparator implements Comparator<PersonalMessage> {
    @Override
    public int compare(PersonalMessage o1, PersonalMessage o2) {
        return o1.getId().compareTo(o2.getId());
    }
}
