package com.teamarea.messages.domain.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "chat_role")
@Getter
@Setter
@NoArgsConstructor
public class ChatRole {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "chat_role_id_gen")
    @SequenceGenerator(name = "chat_role_id_gen", sequenceName = "chat_role_id_seq", allocationSize = 1)
    private Long id;
    private Long chatId;
    private Long roleId;

    public ChatRole(Long chatId, Long roleId) {
        this.chatId = chatId;
        this.roleId = roleId;
    }
}
