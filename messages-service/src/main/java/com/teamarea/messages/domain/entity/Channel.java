package com.teamarea.messages.domain.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "channel")
@Getter
@Setter
@NoArgsConstructor
public class Channel {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "channel_id_gen")
    @SequenceGenerator(name = "channel_id_gen", sequenceName = "channel_id_seq", allocationSize = 1)
    private Long id;
    private Long groupId;
    private String name;
    private String about;
}
