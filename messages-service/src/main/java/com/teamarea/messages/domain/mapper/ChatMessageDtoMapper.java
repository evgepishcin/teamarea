package com.teamarea.messages.domain.mapper;

import com.teamarea.messages.domain.dto.ChatMessageDto;
import com.teamarea.messages.domain.entity.ChatMessage;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ChatMessageDtoMapper {
    ChatMessageDto chatToChatMessageDto(ChatMessage source);
}
