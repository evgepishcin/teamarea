package com.teamarea.messages.domain.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "personal_message_notification")
@Getter
@Setter
@NoArgsConstructor
public class PersonalMessageNotification {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "personal_message_notification_id_gen")
    @SequenceGenerator(name = "personal_message_notification_id_gen",
            sequenceName = "personal_message_notification_id_seq", allocationSize = 1)
    private Long id;
    private Long participantId;
    private Long senderId;
}
