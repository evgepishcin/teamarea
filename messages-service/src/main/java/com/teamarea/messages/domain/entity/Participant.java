package com.teamarea.messages.domain.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "participant")
@Getter
@Setter
@NoArgsConstructor
public class Participant {
    @Id
    private Long id;
    private Long groupId;
    private Long userId;
    private Boolean active;
    private Boolean moderator;
    private Long roleId;
}
