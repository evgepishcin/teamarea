package com.teamarea.messages.repository;

import com.teamarea.messages.domain.entity.ChannelNotification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ChannelNotificationRepository extends JpaRepository<ChannelNotification, Long> {

    @Query("DELETE FROM ChannelNotification cn where cn.channelId = :channelId")
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    void deleteAllByChannelId(Long channelId);

    @Query("SELECT cn FROM ChannelNotification cn where cn.participantId = :participantId and cn.channelId = :channelId")
    Optional<ChannelNotification> findByParticipantIdAndChannelId(Long participantId, Long channelId);

    @Query("DELETE FROM ChannelNotification cn where cn.participantId = :participantId and cn.channelId = :channelId")
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    void deleteAllByParticipantIdAndChannelId(Long participantId, Long channelId);

    @Query("SELECT p.userId FROM Participant p join ChannelNotification cn on p.id = cn.participantId " +
            "where p.userId is not null and p.id in :ids")
    List<Long> getUsersIdsByParticipantIdsIn(List<Long> ids);
}
