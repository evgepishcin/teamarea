package com.teamarea.messages.repository;

import com.teamarea.messages.domain.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RoleRepository extends JpaRepository<Role, Long> {

    @Query("DELETE FROM Role r where r.id = :id")
    @Modifying(flushAutomatically = true, clearAutomatically = true)
    void deleteAllById(Long id);

    @Query("SELECT r FROM Role r where r.id in :ids and r.groupId = :groupId")
    List<Role> findAllByIdInAndGroupIdEquals(List<Long> ids, Long groupId);
}
