package com.teamarea.messages.repository;

import com.teamarea.messages.domain.entity.Chat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ChatRepository extends JpaRepository<Chat, Long> {

    @Query("SELECT c FROM Chat c where c.groupId = :groupId")
    List<Chat> findAllByGroupId(Long groupId);

    @Query("SELECT c FROM Chat c join ChatRole cr on c.id = cr.chatId where cr.roleId = :roleId")
    List<Chat> findAllByRoleId(Long roleId);

    @Query("SELECT c FROM Chat c join ChatParticipant cp on c.id = cp.chatId where cp.participantId = :participantId")
    List<Chat> findAllByParticipantId(Long participantId);
}
