package com.teamarea.messages.repository;

import com.teamarea.messages.domain.entity.ChannelRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ChannelRoleRepository extends JpaRepository<ChannelRole, Long> {

    @Query("DELETE FROM ChannelRole cr where cr.roleId = :roleId")
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    void deleteAllByRoleId(Long roleId);

    @Query("DELETE FROM ChannelRole cr where cr.channelId = :channelId")
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    void deleteAllByChannelId(Long channelId);

    @Query("SELECT cr FROM ChannelRole cr where cr.roleId = :roleId and cr.channelId = :channelId")
    Optional<ChannelRole> findAllByRoleIdAndChannelId(Long roleId, Long channelId);

    @Query("SELECT cr.roleId FROM ChannelRole cr where cr.channelId = :channelId and cr.writeAccess = :writeAccess")
    List<Long> selectAllRolesIdsByChannelIdAndWriteAccess(Long channelId, Boolean writeAccess);

    @Query("SELECT p.userId FROM Participant p join ChannelRole cr on p.roleId = cr.roleId " +
            "where cr.channelId = :channelId and p.userId is not null ")
    List<Long> selectUserIdsByChannelId(Long channelId);

    @Query("SELECT p.userId FROM Participant p join ChannelRole cr on p.roleId = cr.roleId " +
            "where cr.channelId = :channelId and p.userId is not null and cr.writeAccess = false ")
    List<Long> selectUserIdsByChannelIdOnlyReadAccess(Long channelId);
}
