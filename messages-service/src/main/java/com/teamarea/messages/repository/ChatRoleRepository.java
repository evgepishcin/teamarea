package com.teamarea.messages.repository;

import com.teamarea.messages.domain.entity.ChatRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ChatRoleRepository extends JpaRepository<ChatRole, Long> {
    @Query("DELETE FROM ChatRole cr where cr.roleId = :roleId")
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    void deleteAllByRoleId(Long roleId);

    @Query("DELETE FROM ChatRole cr where cr.chatId = :chatId")
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    void deleteAllByChatId(Long chatId);

    @Query("SELECT cr FROM ChatRole cr where cr.roleId = :roleId and cr.chatId = :chatId")
    Optional<ChatRole> findByRoleIdAndChatId(Long roleId, Long chatId);

    @Query("SELECT cr.roleId FROM ChatRole cr where cr.chatId = :chatId")
    List<Long> selectAllRolesIdsByChatId(Long chatId);

    @Query("SELECT p.userId FROM Participant p join ChatRole cr on p.roleId = cr.roleId " +
            "where p.userId is not null and cr.chatId = :chatId")
    List<Long> selectUserIdsByChatId(Long chatId);
}
