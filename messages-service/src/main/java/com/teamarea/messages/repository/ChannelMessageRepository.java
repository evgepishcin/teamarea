package com.teamarea.messages.repository;

import com.teamarea.messages.domain.entity.ChannelMessage;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ChannelMessageRepository extends JpaRepository<ChannelMessage, Long> {

    @Query("DELETE FROM ChannelMessage cm where cm.channelId = :channelId")
    @Modifying(flushAutomatically = true, clearAutomatically = true)
    void deleteAllByChannelId(Long channelId);

    @Query("select cm from ChannelMessage cm where cm.channelId = :channelId order by cm.id desc ")
    List<ChannelMessage> findLastMessages(Long channelId, Pageable pageable);

    @Query("select cm from ChannelMessage cm " +
            "where cm.channelId = :channelId and cm.id < :lastId order by cm.id desc ")
    List<ChannelMessage> findLastMessages(Long channelId, Long lastId, Pageable pageable);

    @Query("select cm from ChannelMessage cm " +
            "where cm.channelId = :channelId and cm.senderId = :participantId order by cm.id desc ")
    List<ChannelMessage> findLastParticipantMessages(Long channelId, Long participantId, Pageable pageable);

    @Query("select cm from ChannelMessage cm " +
            "where cm.channelId = :channelId and cm.senderId = :participantId and cm.id < :lastId order by cm.id desc ")
    List<ChannelMessage> findLastParticipantMessages(Long channelId, Long participantId, Long lastId, Pageable pageable);
}
