package com.teamarea.messages.repository;

import com.teamarea.messages.domain.entity.PersonalMessage;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PersonalMessageRepository extends JpaRepository<PersonalMessage, Long> {

    @Query("select pm from PersonalMessage pm where pm.senderId = :senderId and pm.recipientId = :recipientId " +
            "order by pm.id desc ")
    List<PersonalMessage> findAllBySenderIdAndRecipientId(Long senderId, Long recipientId, Pageable pageable);

    @Query("select pm from PersonalMessage pm where pm.senderId = :senderId and pm.recipientId = :recipientId " +
            "and pm.id < :lastId order by pm.id desc ")
    List<PersonalMessage> findAllBySenderIdAndRecipientId(Long senderId, Long recipientId, Long lastId, Pageable pageable);

    @Query(value = """
            select *
            from personal_message p
            where p.id in
                  (select max(p2.id)
                   from (
                            select p3.id, p3.sender_id as sender
                            from personal_message p3
                            where p3.recipient_id = :participantId
                            union all
                            select p4.id, p4.recipient_id
                            from personal_message p4
                            where p4.sender_id = :participantId
                        ) p2
                   group by sender) group by p.id order by p.id desc limit :limit
            """, nativeQuery = true)
    List<PersonalMessage> findLastPersonalMessages(Long participantId, Long limit);

    @Query(value = """
            select *
            from personal_message p
            where p.id in
                  (select max(p2.id)
                   from (
                            select p3.id, p3.sender_id as sender
                            from personal_message p3
                            where p3.recipient_id = :participantId
                            union all
                            select p4.id, p4.recipient_id
                            from personal_message p4
                            where p4.sender_id = :participantId
                        ) p2 where p2.id < :lastId
                   group by sender) group by p.id order by p.id desc limit :limit
            """, nativeQuery = true)
    List<PersonalMessage> findLastPersonalMessages(Long participantId, Long lastId, Long limit);
}
