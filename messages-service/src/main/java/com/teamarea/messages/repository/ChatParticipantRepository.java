package com.teamarea.messages.repository;

import com.teamarea.messages.domain.entity.ChatParticipant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ChatParticipantRepository extends JpaRepository<ChatParticipant, Long> {

    @Query("DELETE FROM ChatParticipant cp where cp.chatId = :chatId")
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    void deleteAllByChatId(Long chatId);

    @Query("SELECT cp FROM ChatParticipant cp where cp.participantId = :participantId and cp.chatId = :chatId")
    Optional<ChatParticipant> findByParticipantIdAndChatId(Long participantId, Long chatId);

    @Query("select cp.participantId FROM ChatParticipant cp where cp.chatId = :chatId")
    List<Long> selectAllParticipantsIdsByChatId(Long chatId);

    @Query("SELECT p.userId FROM Participant p join ChatParticipant cp on p.id = cp.participantId " +
            "where cp.chatId = :chatId and p.userId is not null ")
    List<Long> selectUserIdsByChatId(Long chatId);
}
