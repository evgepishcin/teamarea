package com.teamarea.messages.repository;

import com.teamarea.messages.domain.entity.ChatMessage;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ChatMessageRepository extends JpaRepository<ChatMessage, Long> {

    @Query("DELETE FROM ChatMessage cm where cm.chatId = :chatId")
    @Modifying(flushAutomatically = true, clearAutomatically = true)
    void deleteAllByChatId(Long chatId);

    @Query("SELECT cm FROM ChatMessage cm where cm.chatId = :chatId order by cm.id desc ")
    List<ChatMessage> findLastMessages(Long chatId, Pageable pageable);

    @Query("SELECT cm FROM ChatMessage cm where cm.chatId = :chatId and cm.id < :lastId order by cm.id desc ")
    List<ChatMessage> findLastMessagesBeforeId(Long chatId, Long lastId, Pageable pageable);
}
