package com.teamarea.messages.repository;

import com.teamarea.messages.domain.entity.ChatNotification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ChatNotificationRepository extends JpaRepository<ChatNotification, Long> {

    @Query("delete from ChatNotification cn where cn.chatId = :chatId")
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    void deleteAllByChatId(Long chatId);

    @Query("delete from ChatNotification cn where cn.participantId = :participantId and cn.chatId = :chatId")
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    void deleteAllByParticipantIdAndChatId(Long participantId, Long chatId);

    @Query("select cn from ChatNotification cn where cn.participantId = :participantId and cn.chatId = :chatId")
    Optional<ChatNotification> findByParticipantIdAndChatId(Long participantId, Long chatId);

    @Query("SELECT p.userId FROM Participant p join ChatNotification cn on p.id = cn.participantId " +
            "where cn.participantId in :chatUsers and p.userId is not null")
    List<Long> getUsersIdsByParticipantIdsIn(List<Long> chatUsers);
}
