package com.teamarea.messages.repository;

import com.teamarea.messages.domain.entity.Participant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ParticipantRepository extends JpaRepository<Participant, Long> {

    @Query("UPDATE Participant p set p.roleId = null where p.roleId = :roleId")
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    void removeRole(Long roleId);

    @Query("UPDATE Participant p set p.userId = null where p.groupId = :groupId")
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    void disableAllGroupParticipants(Long groupId);

    @Query("SELECT p FROM Participant p where p.userId = :userId and p.groupId = :groupId")
    Optional<Participant> findByUserIdAndGroupId(Long userId, Long groupId);

    @Query("SELECT p FROM Participant p where p.id = :id and p.groupId = :groupId and p.active = true")
    Optional<Participant> findByIdAndGroupIdAndActiveIsTrue(Long id, Long groupId);

    @Query("SELECT p FROM Participant p where p.id in :ids and p.groupId = :groupId and p.active = true")
    List<Participant> findAllByIdInAndGroupIdEqualsAndActive(List<Long> ids, Long groupId);

    @Query("update Participant p set p.userId = null where p.userId in: usersIds")
    @Modifying(flushAutomatically = true, clearAutomatically = true)
    void removeUsers(List<Long> usersIds);
}
