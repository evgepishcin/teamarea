package com.teamarea.messages.repository;

import com.teamarea.messages.domain.entity.PersonalMessageNotification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface PersonalMessageNotificationRepository extends JpaRepository<PersonalMessageNotification, Long> {

    @Query("SELECT pmn FROM PersonalMessageNotification pmn where pmn.participantId = :participantId and " +
            "pmn.senderId = :senderId")
    Optional<PersonalMessageNotification> findByParticipantIdAndAndSenderId(Long participantId, Long senderId);

    @Query("delete from PersonalMessageNotification pmn" +
            " where pmn.participantId = :participantId and pmn.senderId = :senderId")
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    void deleteAllByParticipantIdAndAndSenderId(Long participantId, Long senderId);
}
