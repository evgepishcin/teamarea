package com.teamarea.messages.repository;

import com.teamarea.messages.domain.entity.ChannelNotificationMail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface ChannelNotificationMailRepository extends JpaRepository<ChannelNotificationMail, Long> {

    @Query("DELETE FROM ChannelNotificationMail cnm where cnm.channelId = :channelId")
    @Modifying(flushAutomatically = true, clearAutomatically = true)
    void deleteAllByChannelId(Long channelId);

    @Query("SELECT cnm FROM ChannelNotificationMail cnm" +
            " where cnm.participantId = :participantId and cnm.channelId = :channelId")
    Optional<ChannelNotificationMail> findByParticipantIdAndChannelId(Long participantId, Long channelId);

    @Query("DELETE FROM ChannelNotificationMail cnm" +
            " where cnm.participantId = :participantId and cnm.channelId = :channelId")
    @Modifying(flushAutomatically = true, clearAutomatically = true)
    void deleteAllByParticipantIdAndChannelId(Long participantId, Long channelId);
}
