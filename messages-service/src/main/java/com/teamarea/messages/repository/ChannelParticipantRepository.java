package com.teamarea.messages.repository;

import com.teamarea.messages.domain.entity.ChannelParticipant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ChannelParticipantRepository extends JpaRepository<ChannelParticipant, Long> {

    @Query("DELETE FROM ChannelParticipant cp where cp.channelId = :channelId")
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    void deleteAllByChannelId(Long channelId);

    @Query("SELECT cp FROM ChannelParticipant cp where cp.participantId = :participantId and cp.channelId = :channelId")
    Optional<ChannelParticipant> findAllByParticipantIdAndChannelId(Long participantId, Long channelId);

    @Query("SELECT cp.participantId FROM ChannelParticipant cp where cp.channelId = :channelId and cp.writeAccess = :writeAccess")
    List<Long> selectAllParticipantsIdsByChannelIdAndWriteAccess(Long channelId, Boolean writeAccess);

    @Query("SELECT p.userId FROM Participant p join ChannelParticipant cp on p.id = cp.participantId " +
            "where cp.channelId = :channelId and p.userId is not null")
    List<Long> selectUserIdsByChannelId(Long channelId);

    @Query("SELECT p.userId FROM Participant p join ChannelParticipant cp on p.id = cp.participantId " +
            "where cp.channelId = :channelId and p.userId is not null and cp.writeAccess = false ")
    List<Long> selectUserIdsByChannelIdAndOnlyReadAccess(Long channelId);
}
