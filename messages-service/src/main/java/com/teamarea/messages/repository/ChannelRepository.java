package com.teamarea.messages.repository;

import com.teamarea.messages.domain.entity.Channel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ChannelRepository extends JpaRepository<Channel, Long> {

    @Query("SELECT c FROM Channel c join ChannelRole cr on c.id = cr.channelId where cr.roleId = :roleId")
    List<Channel> findAllByRoleId(Long roleId);

    @Query("SELECT c FROM Channel c join ChannelParticipant cp on c.id = cp.channelId" +
            " where cp.participantId = :participantId")
    List<Channel> findAllByParticipantId(Long participantId);

    @Query("SELECT c FROM Channel c where c.groupId = :groupId")
    List<Channel> findAllByGroupId(Long groupId);
}
