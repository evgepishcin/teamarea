package com.teamarea.messages.controller;

import com.teamarea.messages.domain.dto.*;
import com.teamarea.messages.service.ChatService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class ChatController {

    private final ChatService chatService;

    @PostMapping("/messages/{groupId}/chat")
    public ChatDto createChat(@PathVariable("groupId") Long groupId,
                              @Valid @RequestBody NewChatDto newChatDto,
                              @RequestHeader("User-Id") Long userId) {
        return chatService.createChat(newChatDto, groupId, userId);
    }

    @GetMapping("/messages/{groupId}/chat/{chatId}")
    public ChatInfoDto getChat(@PathVariable("groupId") Long groupId,
                               @PathVariable("chatId") Long chatId,
                               @RequestHeader("User-Id") Long userId) {
        return chatService.getChat(chatId, groupId, userId);
    }

    @PutMapping("/messages/{groupId}/chat/{chatId}")
    public ChatDto updateChat(@PathVariable("groupId") Long groupId,
                              @PathVariable("chatId") Long chatId,
                              @Valid @RequestBody NewChatDto newChatDto,
                              @RequestHeader("User-Id") Long userId) {
        return chatService.updateChat(newChatDto, chatId, groupId, userId);
    }

    @DeleteMapping("/messages/{groupId}/chat/{chatId}")
    public void deleteChat(@PathVariable("groupId") Long groupId,
                           @PathVariable("chatId") Long chatId,
                           @RequestHeader("User-Id") Long userId) {
        chatService.deleteChat(chatId, groupId, userId);
    }

    @GetMapping("/messages/{groupId}/chat/all")
    public List<ChatDto> getGroupChats(@PathVariable("groupId") Long groupId,
                                       @RequestHeader("User-Id") Long userId) {
        return chatService.getGroupChats(groupId, userId);
    }

    @GetMapping("/messages/{groupId}/chat/my")
    public List<ChatDto> getParticipantChats(@PathVariable("groupId") Long groupId,
                                             @RequestHeader("User-Id") Long userId) {
        return chatService.getParticipantChats(groupId, userId);
    }

    @PostMapping("/messages/{groupId}/chat/{chatId}/msg")
    public ChatMessageDto sendChatMessage(@PathVariable("groupId") Long groupId,
                                          @PathVariable("chatId") Long chatId,
                                          @Valid @RequestBody NewMessageDto newMessageDto,
                                          @RequestHeader("User-Id") Long userId) {
        return chatService.sendChatMessage(newMessageDto, chatId, groupId, userId);
    }

    @GetMapping("/messages/{groupId}/chat/{chatId}/msg")
    public List<ChatMessageDto> getChatMessages(@PathVariable("groupId") Long groupId,
                                                @PathVariable("chatId") Long chatId,
                                                @RequestParam(name = "lastId", required = false) Long lastId,
                                                @RequestHeader("User-Id") Long userId) {
        return chatService.getChatMessages(chatId, lastId, groupId, userId);
    }

    @PostMapping("/messages/{groupId}/chat/{chatId}/notification")
    public NotificationStatusDto notificationSwitch(@PathVariable("groupId") Long groupId,
                                                    @PathVariable("chatId") Long chatId,
                                                    @RequestParam("enabled") Boolean enabled,
                                                    @RequestHeader("User-Id") Long userId) {
        return chatService.notificationSwitch(chatId, groupId, userId, enabled);
    }

    @GetMapping("/messages/{groupId}/chat/{chatId}/notification")
    public NotificationStatusDto getNotificationStatus(@PathVariable("groupId") Long groupId,
                                                       @PathVariable("chatId") Long chatId,
                                                       @RequestHeader("User-Id") Long userId) {
        return chatService.getNotificationStatus(chatId, groupId, userId);
    }
}
