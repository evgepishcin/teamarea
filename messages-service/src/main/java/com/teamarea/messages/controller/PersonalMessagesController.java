package com.teamarea.messages.controller;

import com.teamarea.messages.domain.dto.NewMessageDto;
import com.teamarea.messages.domain.dto.NotificationStatusDto;
import com.teamarea.messages.domain.dto.PersonalMessageDto;
import com.teamarea.messages.domain.entity.PersonalMessage;
import com.teamarea.messages.service.PersonalMessageService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class PersonalMessagesController {

    private final PersonalMessageService personalMessageService;

    @PostMapping("/messages/{groupId}/personal/{participantId}")
    public PersonalMessageDto sendPersonalMessage(@PathVariable("groupId") Long groupId,
                                                  @PathVariable("participantId") Long participantId,
                                                  @Valid @RequestBody NewMessageDto newMessageDto,
                                                  @RequestHeader("User-Id") Long userId) {
        return personalMessageService.sendPersonalMessage(newMessageDto, participantId, groupId, userId);
    }

    @GetMapping("/messages/{groupId}/personal/{participantId}")
    public List<PersonalMessage> getPersonalMessages(@PathVariable("groupId") Long groupId,
                                                     @PathVariable("participantId") Long participantId,
                                                     @RequestParam(name = "lastId", required = false) Long lastId,
                                                     @RequestHeader("User-Id") Long userId) {
        return personalMessageService.getPersonalMessages(participantId, lastId, groupId, userId);
    }

    @GetMapping("/messages/{groupId}/personal")
    public List<PersonalMessageDto> getLastPersonalMessages(@PathVariable("groupId") Long groupId,
                                                            @RequestParam(name = "lastId", required = false) Long lastId,
                                                            @RequestHeader("User-Id") Long userId) {
        return personalMessageService.getLastPersonalMessages(lastId, groupId, userId);
    }

    @PostMapping("/messages/{groupId}/personal/{participantId}/notification")
    public NotificationStatusDto notificationSwitch(@PathVariable("groupId") Long groupId,
                                                    @PathVariable("participantId") Long participantId,
                                                    @RequestParam("enabled") Boolean enabled,
                                                    @RequestHeader("User-Id") Long userId) {
        return personalMessageService.notificationSwitch(participantId, groupId, userId, enabled);
    }

    @GetMapping("/messages/{groupId}/personal/{participantId}/notification")
    public NotificationStatusDto getNotificationStatus(@PathVariable("groupId") Long groupId,
                                                       @PathVariable("participantId") Long participantId,
                                                       @RequestHeader("User-Id") Long userId) {
        return personalMessageService.getNotificationStatus(participantId, groupId, userId);
    }
}
