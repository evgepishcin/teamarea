package com.teamarea.messages.controller;

import com.teamarea.messages.domain.dto.*;
import com.teamarea.messages.service.ChannelService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class ChannelController {

    private final ChannelService channelService;

    @PostMapping("/messages/{groupId}/channel")
    public ChannelDto createChannel(@PathVariable("groupId") Long groupId,
                                    @Valid @RequestBody NewChannelDto newChannelDto,
                                    @RequestHeader("User-Id") Long userId) {
        return channelService.createChannel(newChannelDto, groupId, userId);
    }

    @GetMapping("/messages/{groupId}/channel/{channelId}")
    public ChannelInfoDto getChannel(@PathVariable("groupId") Long groupId,
                                     @PathVariable("channelId") Long channelId,
                                     @RequestHeader("User-Id") Long userId) {
        return channelService.getChannel(channelId, groupId, userId);
    }

    @GetMapping("/messages/{groupId}/channel/all")
    public List<ChannelDto> getGroupChannels(@PathVariable("groupId") Long groupId,
                                             @RequestHeader("User-Id") Long userId) {
        return channelService.getGroupChannels(groupId, userId);
    }

    @GetMapping("/messages/{groupId}/channel/my")
    public List<ChannelDto> getParticipantChannels(@PathVariable("groupId") Long groupId,
                                                   @RequestHeader("User-Id") Long userId) {
        return channelService.getParticipantChannels(groupId, userId);
    }

    @PutMapping("/messages/{groupId}/channel/{channelId}")
    public ChannelDto updateChannel(@PathVariable("groupId") Long groupId,
                                    @PathVariable("channelId") Long channelId,
                                    @Valid @RequestBody NewChannelDto newChannelDto,
                                    @RequestHeader("User-Id") Long userId) {
        return channelService.updateChannel(newChannelDto, channelId, groupId, userId);
    }

    @DeleteMapping("/messages/{groupId}/channel/{channelId}")
    public void deleteChannel(@PathVariable("groupId") Long groupId,
                              @PathVariable("channelId") Long channelId,
                              @RequestHeader("User-Id") Long userId) {
        channelService.deleteChannel(channelId, groupId, userId);
    }

    @PostMapping("/messages/{groupId}/channel/{channelId}/msg")
    public ChannelMessageDto sendChannelMessage(@PathVariable("groupId") Long groupId,
                                                @PathVariable("channelId") Long channelId,
                                                @Valid @RequestBody NewMessageDto newMessageDto,
                                                @RequestHeader("User-Id") Long userId) {
        return channelService.sendChannelMessage(newMessageDto, channelId, groupId, userId);
    }

    @GetMapping("/messages/{groupId}/channel/{channelId}/msg")
    public List<ChannelMessageDto> getChannelMessages(@PathVariable("groupId") Long groupId,
                                                      @PathVariable("channelId") Long channelId,
                                                      @RequestParam(name = "lastId", required = false) Long lastId,
                                                      @RequestParam(name = "filterMy", required = false) Boolean filterMy,
                                                      @RequestHeader("User-Id") Long userId) {
        return channelService.getChannelMessages(channelId, groupId, userId, lastId, filterMy);
    }

    @PostMapping("/messages/{groupId}/channel/{channelId}/notification")
    public NotificationStatusDto notificationSwitch(@PathVariable("groupId") Long groupId,
                                                    @PathVariable("channelId") Long channelId,
                                                    @RequestParam("enabled") Boolean enabled,
                                                    @RequestHeader("User-Id") Long userId) {
        return channelService.notificationSwitch(channelId, groupId, userId, enabled);
    }

    @GetMapping("/messages/{groupId}/channel/{channelId}/notification")
    public NotificationStatusDto getNotificationStatus(@PathVariable("groupId") Long groupId,
                                                       @PathVariable("channelId") Long channelId,
                                                       @RequestHeader("User-Id") Long userId) {
        return channelService.getNotificationStatus(channelId, groupId, userId);
    }

    @GetMapping("/messages/{groupId}/channel/{channelId}/notification-mail")
    public NotificationStatusDto getNotificationMailStatus(@PathVariable("groupId") Long groupId,
                                                           @PathVariable("channelId") Long channelId,
                                                           @RequestHeader("User-Id") Long userId) {
        return channelService.getNotificationMailStatus(channelId, groupId, userId);
    }

    @PostMapping("/messages/{groupId}/channel/{channelId}/notification-mail")
    public NotificationStatusDto notificationMailSwitch(@PathVariable("groupId") Long groupId,
                                                        @PathVariable("channelId") Long channelId,
                                                        @RequestParam("enabled") Boolean enabled,
                                                        @RequestHeader("User-Id") Long userId) {
        return channelService.notificationMailSwitch(channelId, groupId, userId, enabled);
    }
}
