package com.teamarea.gateway.controller;

import com.teamarea.gateway.domain.dto.TokenInfoDto;
import com.teamarea.gateway.service.TokenService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class SecureController {

    private final TokenService tokenService;

    @DeleteMapping("/secure/gateway/jwt")
    public void deleteJwt(@RequestBody TokenInfoDto tokenInfoDto) {
        tokenService.removeToken(tokenInfoDto);
    }

    @DeleteMapping("/secure/gateway/user")
    public void deleteUser(@RequestParam("userId") Long userId) {
        tokenService.removeTokenForUser(userId);
    }
}
