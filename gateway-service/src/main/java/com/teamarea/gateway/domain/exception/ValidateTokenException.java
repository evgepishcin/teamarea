package com.teamarea.gateway.domain.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class ValidateTokenException extends RuntimeException {
    public ValidateTokenException() {
        super("Token is invalid");
    }
}
