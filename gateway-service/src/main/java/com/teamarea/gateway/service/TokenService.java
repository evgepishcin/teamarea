package com.teamarea.gateway.service;

import com.teamarea.gateway.configuration.GatewayProperties;
import com.teamarea.gateway.configuration.RedisCacheConfiguration;
import com.teamarea.gateway.domain.dto.TokenDeviceDto;
import com.teamarea.gateway.domain.dto.TokenDto;
import com.teamarea.gateway.domain.dto.TokenInfoDto;
import com.teamarea.gateway.domain.exception.ValidateTokenException;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class TokenService {

    private final CacheManager cacheManager;
    private final RestTemplate restTemplate;
    private final GatewayProperties gatewayProperties;

    private Cache tokenCache;
    private Cache logoutUsersCache;

    @PostConstruct
    public void init() {
        tokenCache = Objects.requireNonNull(cacheManager.getCache(RedisCacheConfiguration.TOKEN_CACHE));
        logoutUsersCache = Objects.requireNonNull(cacheManager.getCache(RedisCacheConfiguration.LOGOUT_USERS_CACHE));
    }

    public void removeToken(TokenInfoDto tokenInfoDto) {
        tokenCache.evict(tokenInfoDto.getToken());
    }

    public void removeTokenForUser(Long userId) {
        logoutUsersCache.put(userId, userId);
    }

    public TokenDeviceDto validateToken(String token) {
        TokenDeviceDto tokenDeviceDto = tokenCache.get(token, TokenDeviceDto.class);
        if (tokenDeviceDto != null && logoutUsersCache.get(tokenDeviceDto.getUserId(), Long.class) != null) {
            tokenDeviceDto = null;
        }
        if (tokenDeviceDto == null) {
            TokenDto tokenDto = new TokenDto(token);
            ResponseEntity<TokenDeviceDto> response = restTemplate
                    .postForEntity(gatewayProperties.getEndpoints().getAccountServiceUrl()
                            .concat("/secure/account/validate"), tokenDto, TokenDeviceDto.class);
            if (!response.getStatusCode().equals(HttpStatus.OK)) {
                throw new ValidateTokenException();
            }
            tokenDeviceDto = response.getBody();
            tokenCache.put(token, tokenDeviceDto);
        }
        return tokenDeviceDto;
    }

}
