package com.teamarea.gateway.filter;

import com.teamarea.gateway.domain.dto.TokenDeviceDto;
import com.teamarea.gateway.domain.exception.ValidateTokenException;
import com.teamarea.gateway.service.TokenService;
import lombok.RequiredArgsConstructor;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.List;

@Component
@RequiredArgsConstructor
public class AuthenticationFilter implements GatewayFilter {

    private final TokenService tokenService;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        String auth = getAuthHeader(exchange.getRequest());
        TokenDeviceDto tokenDeviceDto = tokenService.validateToken(auth);
        ServerHttpRequest request = exchange.getRequest();
        assert tokenDeviceDto != null;
        request.mutate().header("User-Id", tokenDeviceDto.getUserId().toString());
        request.mutate().header("Device-Uuid", tokenDeviceDto.getDeviceUuid().toString());
        return chain.filter(exchange);
    }

    private String getAuthHeader(ServerHttpRequest request) {
        List<String> data = request.getHeaders().getOrEmpty("Authorization");
        if (data.isEmpty()) {
            throw new ValidateTokenException();
        }
        return data.get(0);
    }
}
