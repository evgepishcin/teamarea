package com.teamarea.gateway.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

@ConfigurationProperties(prefix = "teamarea.gateway")
public class GatewayProperties {

    @NestedConfigurationProperty
    private Endpoints endpoints;

    public Endpoints getEndpoints() {
        return endpoints;
    }

    public void setEndpoints(Endpoints endpoints) {
        this.endpoints = endpoints;
    }

    public static class Endpoints {
        private String accountServiceUrl;
        private String groupServiceUrl;
        private String messagesServiceUrl;
        private String emailServiceUrl;
        private String notificationServiceUrl;
        private String cloudMessagingServiceUrl;

        public String getAccountServiceUrl() {
            return accountServiceUrl;
        }

        public void setAccountServiceUrl(String accountServiceUrl) {
            this.accountServiceUrl = accountServiceUrl;
        }

        public String getGroupServiceUrl() {
            return groupServiceUrl;
        }

        public void setGroupServiceUrl(String groupServiceUrl) {
            this.groupServiceUrl = groupServiceUrl;
        }

        public String getMessagesServiceUrl() {
            return messagesServiceUrl;
        }

        public void setMessagesServiceUrl(String messagesServiceUrl) {
            this.messagesServiceUrl = messagesServiceUrl;
        }

        public String getEmailServiceUrl() {
            return emailServiceUrl;
        }

        public void setEmailServiceUrl(String emailServiceUrl) {
            this.emailServiceUrl = emailServiceUrl;
        }

        public String getNotificationServiceUrl() {
            return notificationServiceUrl;
        }

        public void setNotificationServiceUrl(String notificationServiceUrl) {
            this.notificationServiceUrl = notificationServiceUrl;
        }

        public String getCloudMessagingServiceUrl() {
            return cloudMessagingServiceUrl;
        }

        public void setCloudMessagingServiceUrl(String cloudMessagingServiceUrl) {
            this.cloudMessagingServiceUrl = cloudMessagingServiceUrl;
        }
    }
}
