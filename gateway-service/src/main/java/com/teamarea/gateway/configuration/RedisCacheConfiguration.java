package com.teamarea.gateway.configuration;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.cache.RedisCacheManagerBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;

@Configuration
@RequiredArgsConstructor
public class RedisCacheConfiguration {

    public static final String TOKEN_CACHE = "tokens";
    public static final String LOGOUT_USERS_CACHE = "logout-users";
    private static final long TTL_MINUTES = 180;

    @Bean
    public RedisCacheManagerBuilderCustomizer redisCacheManagerBuilderCustomizer() {
        return builder -> builder
                .withCacheConfiguration(TOKEN_CACHE,
                        org.springframework.data.redis.cache.RedisCacheConfiguration
                                .defaultCacheConfig(Thread.currentThread().getContextClassLoader())
                                .prefixCacheNameWith(TOKEN_CACHE)
                                .entryTtl(Duration.ofMinutes(TTL_MINUTES)))
                .withCacheConfiguration(LOGOUT_USERS_CACHE,
                        org.springframework.data.redis.cache.RedisCacheConfiguration
                                .defaultCacheConfig(Thread.currentThread().getContextClassLoader())
                                .prefixCacheNameWith(LOGOUT_USERS_CACHE)
                                .entryTtl(Duration.ofMinutes(TTL_MINUTES)))
                .build();
    }

}
