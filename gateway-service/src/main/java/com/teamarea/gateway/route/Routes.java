package com.teamarea.gateway.route;

import com.teamarea.gateway.configuration.GatewayProperties;
import com.teamarea.gateway.filter.AuthenticationFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class Routes {

    private final GatewayProperties gatewayProperties;
    private final AuthenticationFilter authenticationFilter;

    @Bean
    public RouteLocator routeLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                .route(r -> r
                        .path("/account/**")
                        .uri(gatewayProperties.getEndpoints().getAccountServiceUrl()))
                .route(r -> r
                        .path("/group/**")
                        .filters(f -> f.filter(authenticationFilter))
                        .uri(gatewayProperties.getEndpoints().getGroupServiceUrl()))
                .route(r -> r
                        .path("/messages/**")
                        .filters(f -> f.filter(authenticationFilter))
                        .uri(gatewayProperties.getEndpoints().getMessagesServiceUrl()))
                .route(r -> r
                        .path("/email/**")
                        .filters(f -> f.filter(authenticationFilter))
                        .uri(gatewayProperties.getEndpoints().getEmailServiceUrl()))
                .route(r -> r
                        .path("/notification/**")
                        .filters(f -> f.filter(authenticationFilter))
                        .uri(gatewayProperties.getEndpoints().getNotificationServiceUrl()))
                .route(r -> r
                        .path("/cloudMessaging/**")
                        .filters(f -> f.filter(authenticationFilter))
                        .uri(gatewayProperties.getEndpoints().getCloudMessagingServiceUrl()))
                .build();
    }
}
