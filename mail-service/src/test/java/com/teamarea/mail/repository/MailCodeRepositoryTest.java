package com.teamarea.mail.repository;

import com.teamarea.mail.domain.entity.MailCode;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class MailCodeRepositoryTest {

    @Autowired
    private MailCodeRepository mailCodeRepository;

    @Autowired
    private TestEntityManager testEntityManager;


    @Test
    void deleteAllByUserIdIn() {
        List<MailCode> mailCodes = defMailCodeList();
        mailCodes.forEach(testEntityManager::persistAndFlush);
        mailCodeRepository.deleteAllByUserIdIn(Arrays.asList(1L, 2L));
        List<MailCode> checked = mailCodeRepository.findAll();
        assertEquals(1, checked.size());
        MailCode code = checked.get(0);
        assertEquals(3L, code.getUserId());
    }

    private List<MailCode> defMailCodeList() {
        MailCode c1 =  new MailCode();
        c1.setUserId(1L);
        c1.setCodeNewMail("codeNew1");
        c1.setCodeOldMail("codeOld1");
        c1.setCreateDate(new Date());
        c1.setTryCount(0);
        c1.setNewMail("newMail1");

        MailCode c2 =  new MailCode();
        c2.setUserId(2L);
        c2.setCodeNewMail("codeNew2");
        c2.setCodeOldMail("codeOld2");
        c2.setCreateDate(new Date());
        c2.setTryCount(0);
        c2.setNewMail("newMail2");

        MailCode c3 =  new MailCode();
        c3.setUserId(3L);
        c3.setCodeNewMail("codeNew3");
        c3.setCodeOldMail("codeOld3");
        c3.setCreateDate(new Date());
        c3.setTryCount(0);
        c3.setNewMail("newMail3");
        return Arrays.asList(c1, c2, c3);
    }
}