package com.teamarea.mail;

import com.teamarea.mail.context.TestContext;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

@SpringBootTest
@ActiveProfiles("test")
@ContextConfiguration(classes = TestContext.class)
class UserMailAppTest {

    @Test
    @SuppressWarnings("java:S2699")
    void contextLoads() {
    }

}