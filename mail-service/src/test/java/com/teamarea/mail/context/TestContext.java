package com.teamarea.mail.context;

import org.mockito.Mockito;
import org.springframework.boot.autoconfigure.mail.MailProperties;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.mail.javamail.JavaMailSender;

@TestConfiguration
@Profile("test")
public class TestContext {

    @Bean
    public JavaMailSender javaMailSender () {
        return Mockito.mock(JavaMailSender.class);
    }

    @Bean
    public MailProperties mailProperties() {
        return Mockito.mock(MailProperties.class);
    }
}
