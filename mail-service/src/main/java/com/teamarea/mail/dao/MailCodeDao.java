package com.teamarea.mail.dao;

import com.teamarea.mail.domain.entity.MailCode;
import com.teamarea.mail.repository.MailCodeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
public class MailCodeDao {

    private final MailCodeRepository mailCodeRepository;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void deleteByIdAndCommit(Long id) {
        mailCodeRepository.deleteById(id);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public MailCode saveAndCommit(MailCode mailCode) {
        return mailCodeRepository.save(mailCode);
    }
}
