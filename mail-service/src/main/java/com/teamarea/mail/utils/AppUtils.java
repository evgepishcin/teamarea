package com.teamarea.mail.utils;

import org.springframework.stereotype.Component;

@Component
public class AppUtils {

    private static final int MAIL_CODE_LENGTH = 8;

    public String generateMailCode() {
        RandomString randomString = new RandomString(MAIL_CODE_LENGTH);
        return randomString.nextString();
    }
}
