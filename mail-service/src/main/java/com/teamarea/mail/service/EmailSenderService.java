package com.teamarea.mail.service;

import com.teamarea.common.data.api.Operation;
import com.teamarea.mail.api.dto.ChannelMessageDto;
import com.teamarea.mail.api.dto.DeviceConfirmationDto;
import com.teamarea.mail.api.dto.EventDto;
import com.teamarea.mail.api.dto.PasswordRecoveryDto;
import com.teamarea.mail.domain.entity.MailCode;
import com.teamarea.mail.domain.entity.UserMail;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.boot.autoconfigure.mail.MailProperties;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.internet.MimeMessage;
import java.nio.charset.StandardCharsets;
import java.util.List;

@Service
@RequiredArgsConstructor
public class EmailSenderService {

    private final JavaMailSender javaMailSender;
    private final MailProperties mailProperties;
    private final SpringTemplateEngine springTemplateEngine;

    @SneakyThrows
    public void sendChangeMailEmail(MailCode newMailCode, UserMail oldMail) {
        MimeMessage messageOldMail = javaMailSender.createMimeMessage();
        MimeMessage messageNewMail = javaMailSender.createMimeMessage();
        MimeMessageHelper helperOldMail = new MimeMessageHelper(messageOldMail,
                MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                StandardCharsets.UTF_8.name());
        MimeMessageHelper helperNewMail = new MimeMessageHelper(messageNewMail,
                MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                StandardCharsets.UTF_8.name());
        Context contextOldMail = new Context();
        Context contextNewMail = new Context();
        contextOldMail.setVariable("code", newMailCode.getCodeOldMail());
        contextNewMail.setVariable("code", newMailCode.getCodeNewMail());
        String htmlOldCode = springTemplateEngine.process("change-email", contextOldMail);
        String htmlNewCode = springTemplateEngine.process("change-email", contextNewMail);
        helperOldMail.setFrom(mailProperties.getUsername());
        helperNewMail.setFrom(mailProperties.getUsername());
        helperOldMail.setText(htmlOldCode, true);
        helperNewMail.setText(htmlNewCode, true);
        helperOldMail.setSubject("email changing");
        helperNewMail.setSubject("email changing");
        helperOldMail.setTo(oldMail.getMail());
        helperNewMail.setTo(newMailCode.getNewMail());
        javaMailSender.send(messageOldMail);
        javaMailSender.send(messageNewMail);
    }

    @SneakyThrows
    public void sendEventMail(EventDto eventDto, String operation, List<String> mails) {
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper messageHelper = new MimeMessageHelper(message,
                MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());
        Context context = new Context();
        context.setVariable("groupName", eventDto.getGroupName());
        context.setVariable("name", eventDto.getName());
        context.setVariable("date", eventDto.getDate());
        context.setVariable("about", eventDto.getAbout());
        String operationValue;
        if (Operation.DELETE.equals(operation)) {
            operationValue = "Событие было удалено";
        } else if (Operation.UPDATE.equals(operation)) {
            operationValue = "Событие было изменено";
        } else if (Operation.READ.equals(operation)) {
            operationValue = "Напоминание о событии";
        } else {
            operationValue = "Было создано новое событие";
        }
        context.setVariable("operation", operationValue);
        String html = springTemplateEngine.process("event-email", context);
        messageHelper.setFrom(mailProperties.getUsername());
        messageHelper.setText(html, true);
        messageHelper.setSubject("event");
        messageHelper.setTo(mails.toArray(new String[0]));
        javaMailSender.send(message);
    }

    @SneakyThrows
    public void sendChannelMessageMail(ChannelMessageDto channelMessageDto, String operation, List<String> mails) {
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper messageHelper = new MimeMessageHelper(message,
                MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());
        Context context = new Context();
        context.setVariable("channelName", channelMessageDto.getChannelName());
        context.setVariable("createDate", channelMessageDto.getCreateDate());
        context.setVariable("text", channelMessageDto.getText());
        String operationValue;
        if (Operation.UPDATE.equals(operation)) {
            operationValue = "Сообщение было изменено";
        } else {
            operationValue = "Новое сообщение";
        }
        context.setVariable("operation", operationValue);
        String html = springTemplateEngine.process("channel-message-email", context);
        messageHelper.setFrom(mailProperties.getUsername());
        messageHelper.setText(html, true);
        messageHelper.setSubject("channel message");
        messageHelper.setTo(mails.toArray(new String[0]));
        javaMailSender.send(message);
    }

    @SneakyThrows
    public void sendDeviceConfirmationMail(DeviceConfirmationDto deviceConfirmationDto, String mail) {
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper messageHelper = new MimeMessageHelper(message,
                MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());
        Context context = new Context();
        context.setVariable("deviceInfo", deviceConfirmationDto.getDeviceInfo());
        context.setVariable("code", deviceConfirmationDto.getCode());
        String html = springTemplateEngine.process("device-confirmation-email", context);
        messageHelper.setFrom(mailProperties.getUsername());
        messageHelper.setText(html, true);
        messageHelper.setSubject("device confirmation");
        messageHelper.setTo(mail);
        javaMailSender.send(message);
    }

    @SneakyThrows
    public void sendPasswordRecoveryMail(PasswordRecoveryDto passwordRecoveryDto, String mail) {
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper messageHelper = new MimeMessageHelper(message,
                MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());
        Context context = new Context();
        context.setVariable("newPassword", passwordRecoveryDto.getNewPassword());
        String html = springTemplateEngine.process("password-recovery-email", context);
        messageHelper.setFrom(mailProperties.getUsername());
        messageHelper.setText(html, true);
        messageHelper.setSubject("password recovery");
        messageHelper.setTo(mail);
        javaMailSender.send(message);
    }
}
