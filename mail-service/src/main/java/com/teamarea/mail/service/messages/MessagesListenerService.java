package com.teamarea.mail.service.messages;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.teamarea.common.data.api.TransferData;
import lombok.RequiredArgsConstructor;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
@SuppressWarnings({"unchecked", "rawtypes"})
public class MessagesListenerService {

    private final ObjectMapper objectMapper;
    private final List<MessageWorker> messageWorkers;
    private final Map<String, MessageWorker> messageWorkerMap = new HashMap<>();

    @PostConstruct
    public void init() {
        messageWorkers.forEach(worker -> messageWorkerMap.put(worker.getMessageType(), worker));
    }

    @KafkaListener(topics = Channels.MAIL)
    public void listenGroup(String message) throws JsonProcessingException {
        TransferData transferData = objectMapper.readValue(message, TransferData.class);
        MessageWorker messageWorker = messageWorkerMap.get(transferData.getType());
        if (messageWorker == null) {
            throw new MessagesListenerServiceException("Implementation of messageWorker with " +
                    "this message type doesn't exist");
        }
        messageWorker.accept(objectMapper.readValue(transferData.getPayload(), messageWorker.getMessageClass()),
                transferData.getOperation());
    }
}
