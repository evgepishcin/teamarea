package com.teamarea.mail.service;

import com.teamarea.mail.api.dto.ChannelMessageDto;
import com.teamarea.mail.api.dto.DeviceConfirmationDto;
import com.teamarea.mail.api.dto.EventDto;
import com.teamarea.mail.api.dto.PasswordRecoveryDto;
import com.teamarea.mail.dao.MailCodeDao;
import com.teamarea.mail.domain.dto.ChangeMailDto;
import com.teamarea.mail.domain.dto.NewMailDto;
import com.teamarea.mail.domain.entity.MailCode;
import com.teamarea.mail.domain.entity.UserMail;
import com.teamarea.mail.domain.exception.*;
import com.teamarea.mail.domain.mapper.NewMailDtoMapper;
import com.teamarea.mail.repository.MailCodeRepository;
import com.teamarea.mail.repository.UserMailRepository;
import com.teamarea.mail.utils.AppUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class MailService {

    private static final int MAX_CODE_TRY_COUNT = 3;

    private final UserMailRepository userMailRepository;
    private final MailCodeRepository mailCodeRepository;
    private final NewMailDtoMapper newMailDtoMapper;
    private final AppUtils appUtils;
    private final EmailSenderService emailSenderService;
    private final MailCodeDao mailCodeDao;

    @Transactional
    public void registerUserMail(NewMailDto newMailDto) {
        Optional<UserMail> mail = userMailRepository.findByMail(newMailDto.getMail());
        Optional<MailCode> mailCode = mailCodeRepository.findByNewMail(newMailDto.getMail());
        if (mail.isPresent() || mailCode.isPresent()) {
            throw new MailAlreadyExistException();
        }
        UserMail userMail = newMailDtoMapper.newMailDtoToUserMail(newMailDto);
        userMailRepository.save(userMail);
    }

    @Transactional
    public void changeMail(ChangeMailDto changeMailDto, Long userId) {
        Optional<UserMail> mail = userMailRepository.findByMail(changeMailDto.getNewMail());
        Optional<MailCode> mailCode = mailCodeRepository.findByNewMail(changeMailDto.getNewMail());
        if (mail.isPresent() || mailCode.isPresent()) {
            throw new MailAlreadyExistException();
        }
        UserMail oldMail = userMailRepository.findByUserId(userId).orElseThrow(UserNotFoundException::new);
        MailCode newMailCode = new MailCode();
        newMailCode.setUserId(userId);
        newMailCode.setNewMail(changeMailDto.getNewMail());
        newMailCode.setCodeOldMail(appUtils.generateMailCode());
        newMailCode.setCodeNewMail(appUtils.generateMailCode());
        newMailCode.setTryCount(0);
        newMailCode.setCreateDate(new Date());
        mailCodeRepository.save(newMailCode);
        emailSenderService.sendChangeMailEmail(newMailCode, oldMail);
    }

    @Transactional
    public void confirmNewMail(String oldCode, String newCode, Long userId) {
        MailCode mailCode = mailCodeRepository.findByUserId(userId).orElseThrow(ChangeMailRequestNotFoundException::new);
        if (!mailCode.getCodeOldMail().equals(oldCode) || !mailCode.getCodeNewMail().equals(newCode)) {
            mailCode.setTryCount(mailCode.getTryCount() + 1);
            if (mailCode.getTryCount() >= MAX_CODE_TRY_COUNT) {
                mailCodeDao.deleteByIdAndCommit(mailCode.getUserId());
                throw new MaxTryCountException();
            }
            mailCodeDao.saveAndCommit(mailCode);
            throw new WrongCodeException();
        }
        UserMail userMail = userMailRepository.findByUserId(userId).orElseThrow(UserNotFoundException::new);
        userMail.setMail(mailCode.getNewMail());
        mailCodeRepository.deleteById(mailCode.getUserId());
    }

    @Transactional
    public void deleteOldMailCodes() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.HOUR_OF_DAY, -1);
        Date deleteTime = calendar.getTime();
        mailCodeRepository.deleteOldRecords(deleteTime);
    }

    public void sendEventMail(EventDto eventDto, String operation) {
        List<UserMail> userMails = userMailRepository.findByUserIdIn(eventDto.getUsersIds());
        List<String> mails = userMails.stream().map(UserMail::getMail).toList();
        if (!mails.isEmpty()) {
            emailSenderService.sendEventMail(eventDto, operation, mails);
        }
    }

    public void sendChannelMessageMail(ChannelMessageDto channelMessageDto, String operation) {
        List<UserMail> userMails = userMailRepository.findByUserIdIn(channelMessageDto.getUsersIds());
        List<String> mails = userMails.stream().map(UserMail::getMail).toList();
        if (!mails.isEmpty()) {
            emailSenderService.sendChannelMessageMail(channelMessageDto, operation, mails);
        }
    }

    public void sendDeviceConfirmationMail(DeviceConfirmationDto deviceConfirmationDto) {
        Optional<UserMail> userMail = userMailRepository.findByUserId(deviceConfirmationDto.getUserId());
        userMail.ifPresent(mail -> emailSenderService.sendDeviceConfirmationMail(deviceConfirmationDto, mail.getMail()));
    }

    public void sendPasswordRecoveryMail(PasswordRecoveryDto passwordRecoveryDto) {
        Optional<UserMail> userMail = userMailRepository.findByUserId(passwordRecoveryDto.getUserId());
        userMail.ifPresent(mail -> emailSenderService.sendPasswordRecoveryMail(passwordRecoveryDto, mail.getMail()));
    }

    @Transactional
    public void deleteUsers(List<Long> usersIds) {
        mailCodeRepository.deleteAllByUserIdIn(usersIds);
        userMailRepository.deleteAllByUserIdIn(usersIds);
    }
}
