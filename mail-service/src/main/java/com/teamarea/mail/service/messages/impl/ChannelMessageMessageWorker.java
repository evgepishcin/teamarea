package com.teamarea.mail.service.messages.impl;

import com.teamarea.mail.api.dto.ChannelMessageDto;
import com.teamarea.mail.api.dto.MailMessageType;
import com.teamarea.mail.service.MailService;
import com.teamarea.mail.service.messages.MessageWorker;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ChannelMessageMessageWorker implements MessageWorker<ChannelMessageDto> {

    private final MailService mailService;

    @Override
    public String getMessageType() {
        return MailMessageType.CHANNEL_MESSAGE;
    }

    @Override
    public void accept(ChannelMessageDto channelMessageDto, String operation) {
        mailService.sendChannelMessageMail(channelMessageDto, operation);
    }

    @Override
    public Class<?> getMessageClass() {
        return ChannelMessageDto.class;
    }
}
