package com.teamarea.mail.service.messages.impl;

import com.teamarea.common.data.api.Operation;
import com.teamarea.mail.api.dto.DeviceConfirmationDto;
import com.teamarea.mail.api.dto.MailMessageType;
import com.teamarea.mail.service.MailService;
import com.teamarea.mail.service.messages.MessageWorker;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class DeviceConfirmationMessageWorker implements MessageWorker<DeviceConfirmationDto> {

    private final MailService mailService;

    @Override
    public String getMessageType() {
        return MailMessageType.DEVICE_CONFIRMATION;
    }

    @Override
    public void accept(DeviceConfirmationDto deviceConfirmationDto, String operation) {
        if (Operation.CREATE.equals(operation)) {
            mailService.sendDeviceConfirmationMail(deviceConfirmationDto);
        }
    }

    @Override
    public Class<?> getMessageClass() {
        return DeviceConfirmationDto.class;
    }
}
