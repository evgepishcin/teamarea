package com.teamarea.mail.service.messages.impl;

import com.teamarea.mail.api.dto.EventDto;
import com.teamarea.mail.api.dto.MailMessageType;
import com.teamarea.mail.service.MailService;
import com.teamarea.mail.service.messages.MessageWorker;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class EventMessageWorker implements MessageWorker<EventDto> {

    private final MailService mailService;

    @Override
    public String getMessageType() {
        return MailMessageType.EVENT;
    }

    @Override
    public void accept(EventDto eventDto, String operation) {
        mailService.sendEventMail(eventDto, operation);
    }

    @Override
    public Class<?> getMessageClass() {
        return EventDto.class;
    }
}
