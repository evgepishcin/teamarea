package com.teamarea.mail.service.messages.impl;

import com.teamarea.common.data.api.Operation;
import com.teamarea.mail.api.dto.MailMessageType;
import com.teamarea.mail.api.dto.PasswordRecoveryDto;
import com.teamarea.mail.service.MailService;
import com.teamarea.mail.service.messages.MessageWorker;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class PasswordRecoveryMessageWorker implements MessageWorker<PasswordRecoveryDto> {

    private final MailService mailService;

    @Override
    public String getMessageType() {
        return MailMessageType.PASSWORD_RECOVERY;
    }

    @Override
    public void accept(PasswordRecoveryDto passwordRecoveryDto, String operation) {
        if (Operation.CREATE.equals(operation)) {
            mailService.sendPasswordRecoveryMail(passwordRecoveryDto);
        }
    }

    @Override
    public Class<?> getMessageClass() {
        return PasswordRecoveryDto.class;
    }
}
