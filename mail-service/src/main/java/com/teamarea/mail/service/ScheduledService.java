package com.teamarea.mail.service;

import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ScheduledService {

    private final MailService mailService;

    @Scheduled(fixedDelay = 300000)
    public void deleteOldMailCodes() {
        mailService.deleteOldMailCodes();
    }
}
