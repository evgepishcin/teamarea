package com.teamarea.mail.service.messages;

import lombok.experimental.UtilityClass;

/**
 * Канал для отправки сообщений в брокер сообщений
 */
@UtilityClass
public class Channels {
    public final String MAIL = "mail";
}
