package com.teamarea.mail.service.messages.impl;

import com.teamarea.account.api.dto.AccountMessageType;
import com.teamarea.account.api.dto.UsersIdsList;
import com.teamarea.common.data.api.Operation;
import com.teamarea.mail.service.MailService;
import com.teamarea.mail.service.messages.MessageWorker;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UsersIdsListMessageWorker implements MessageWorker<UsersIdsList> {

    private final MailService mailService;

    @Override
    public String getMessageType() {
        return AccountMessageType.USERS_ID_LIST;
    }

    @Override
    public void accept(UsersIdsList usersIdsList, String operation) {
        if (Operation.DELETE.equals(operation)) {
            mailService.deleteUsers(usersIdsList.getUsersIds());
        }
    }

    @Override
    public Class<?> getMessageClass() {
        return UsersIdsList.class;
    }
}
