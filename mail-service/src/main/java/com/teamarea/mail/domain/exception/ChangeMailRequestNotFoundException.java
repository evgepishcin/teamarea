package com.teamarea.mail.domain.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ChangeMailRequestNotFoundException extends RuntimeException {
    public ChangeMailRequestNotFoundException() {
        super("Change mail request not found!");
    }
}
