package com.teamarea.mail.domain.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "mail_code")
@Getter
@Setter
@NoArgsConstructor
public class MailCode {
    @Id
    private Long userId;
    private String newMail;
    private String codeOldMail;
    private String codeNewMail;
    private Integer tryCount;
    private Date createDate;
}
