package com.teamarea.mail.domain.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class MailAlreadyExistException extends RuntimeException {
    public MailAlreadyExistException() {
        super("This mail already exist!");
    }
}
