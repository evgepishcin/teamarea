package com.teamarea.mail.domain.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mail")
@Getter
@Setter
@NoArgsConstructor
public class UserMail {
    @Id
    private Long userId;
    private String mail;
}
