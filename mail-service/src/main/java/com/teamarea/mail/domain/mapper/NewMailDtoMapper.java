package com.teamarea.mail.domain.mapper;

import com.teamarea.mail.domain.dto.NewMailDto;
import com.teamarea.mail.domain.entity.UserMail;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface NewMailDtoMapper {
    UserMail newMailDtoToUserMail(NewMailDto source);
}
