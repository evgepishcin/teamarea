package com.teamarea.mail.repository;

import com.teamarea.mail.domain.entity.MailCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface MailCodeRepository extends JpaRepository<MailCode, Long> {

    Optional<MailCode> findByNewMail(String mail);

    Optional<MailCode> findByUserId(Long userId);

    @Query("delete from MailCode mc where mc.createDate < :date")
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    void deleteOldRecords(Date date);

    void deleteAllByUserIdIn(List<Long> usersIds);
}
