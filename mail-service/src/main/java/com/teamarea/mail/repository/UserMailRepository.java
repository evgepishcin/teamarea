package com.teamarea.mail.repository;

import com.teamarea.mail.domain.entity.UserMail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserMailRepository extends JpaRepository<UserMail, Long> {

    Optional<UserMail> findByMail(String mail);

    Optional<UserMail> findByUserId(Long userId);

    List<UserMail> findByUserIdIn(List<Long> usersIds);

    void deleteAllByUserIdIn(List<Long> usersIds);
}
