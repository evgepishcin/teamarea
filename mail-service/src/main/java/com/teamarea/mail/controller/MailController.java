package com.teamarea.mail.controller;

import com.teamarea.mail.domain.dto.ChangeMailDto;
import com.teamarea.mail.service.MailService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
public class MailController {

    private final MailService mailService;

    @PostMapping("/email/change")
    public void changeMail(
            @RequestBody ChangeMailDto changeMailDto,
            @RequestHeader("User-Id") Long userId) {
        mailService.changeMail(changeMailDto, userId);
    }

    @PostMapping("/email/change/confirm")
    public void confirmNewMail(
            @RequestParam("oldCode") String oldCode,
            @RequestParam("newCode") String newCode,
            @RequestHeader("User-Id") Long userId
    ) {
        mailService.confirmNewMail(oldCode, newCode, userId);
    }
}
