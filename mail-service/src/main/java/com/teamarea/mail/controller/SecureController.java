package com.teamarea.mail.controller;

import com.teamarea.mail.domain.dto.NewMailDto;
import com.teamarea.mail.service.MailService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * Даный контроллер должен быть доступен только внутри кластера
 * Используется только внутренними сервисами
 */
@RestController
@RequiredArgsConstructor
public class SecureController {

    private final MailService mailService;

    @PostMapping("/secure/mail/user")
    @ResponseStatus(value = HttpStatus.CREATED)
    public void registerUserMail(@Valid @RequestBody NewMailDto newMailDto) {
        mailService.registerUserMail(newMailDto);
    }
}
