package com.teamarea.mail.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Данные для восстановления пароля
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PasswordRecoveryDto {

    /**
     * Идентификатор пользователя
     */
    private Long userId;

    /**
     * Новый пароль
     */
    private String newPassword;
}
