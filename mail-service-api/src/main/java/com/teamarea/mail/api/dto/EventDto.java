package com.teamarea.mail.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

/**
 * Событие группы
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EventDto {

    /**
     * Идентификаторы пользователей
     */
    private List<Long> usersIds;

    /**
     * Имя группы
     */
    private String groupName;

    /**
     * Название события
     */
    private String name;

    /**
     * Дата события
     */
    private Date date;

    /**
     * Информация о событии
     */
    private String about;
}
