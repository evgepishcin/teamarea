package com.teamarea.mail.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

/**
 * Информация о сообщении в канале
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ChannelMessageDto {

    /**
     * Идентификаторы пользователей
     */
    private List<Long> usersIds;

    /**
     * Имя канала
     */
    private String channelName;

    /**
     * Текст сообщения
     */
    private String text;

    /**
     * Дата отправки сообщения
     */
    private Date createDate;
}
