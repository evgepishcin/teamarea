package com.teamarea.mail.api.dto;

import lombok.experimental.UtilityClass;

@UtilityClass
public class MailMessageType {
    public final String DEVICE_CONFIRMATION = "DEVICE_CONFIRMATION";
    public final String PASSWORD_RECOVERY = "PASSWORD_RECOVERY";
    public final String CHANNEL_MESSAGE = "CHANNEL_MESSAGE";
    public final String EVENT = "EVENT";
}
