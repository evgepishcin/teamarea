package com.teamarea.mail.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Данные о подтверждении входа с нового устройства
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DeviceConfirmationDto {

    /**
     * Идентификатор пользователя
     */
    private Long userId;

    /**
     * Краткая информация об устройстве
     */
    private String deviceInfo;

    /**
     * Код подтверждения входа с нового устройства
     */
    private String code;
}
